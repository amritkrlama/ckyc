# CKYC #

This README documents the steps necessary to get the application up and running.

### What is this repository for? ###

* Central KYC Registry is a centralized repository of KYC records of customers in the financial sector with uniform KYC norms and inter-usability of the KYC records across the sector with an objective to reduce the burden of producing KYC documents and getting those verified every time when the customer creates a new relationship with a financial entity.

 
### How do I get set up? ###

* Need to have "input" and "response" folders in "/Downloads/CKYC/" path.
  
* APIs can be accessed at:
	Upload: http://localhost:8090/api/bulkUpload
	Update: http://localhost:8090/api/bulkUpdate
	Search: http://localhost:8090/api/searchCkyc


### Who do I talk to for suggestions or doubt? ###

* Amrit Kumar Lama (amrit.kumar@capiot.com)