package com.capiot.ckyc.bulkfile.client;

import org.json.simple.JSONArray;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.env.Environment;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestClientException;
import org.springframework.web.client.RestTemplate;

@Component
public class ODPClient {
	
	final static Logger logger = LoggerFactory.getLogger(ODPClient.class);
	@Autowired
	RestTemplate template;
	@Autowired
	Environment environment;
	@Value("${business_center}")
	private String businessCenter;
	
	public void postFileToODP(String commonPath, String fileName) throws Exception {
		HttpHeaders header = new HttpHeaders();
		header.setContentType(MediaType.APPLICATION_JSON);
		HttpEntity<String> request = new HttpEntity<>(header);
		String url = businessCenter+"file/upload";
		try {
			template.exchange(url, HttpMethod.GET, request, String.class);
		} catch (RestClientException exception) {
			throw new Exception("Invalid request for invoking bulk file generation", exception);
		}
	}
	
	
	public void postSuccessRecordsToODP(JSONArray records) throws Exception {
		HttpHeaders header = new HttpHeaders();
		header.setContentType(MediaType.APPLICATION_JSON);
		HttpEntity<String> request = new HttpEntity<String>(records.toJSONString(), header);
		String url = businessCenter+"ckychub";
		try {
			template.exchange(url, HttpMethod.POST, request, String.class);
		} catch (RestClientException exception) {
			throw new Exception("Invalid request for invoking bulk file generation", exception);
		}
	}
}
