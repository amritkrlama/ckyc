package com.capiot.ckyc.bulkfile.exception;

import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;
import javax.ws.rs.ext.ExceptionMapper;

public class InvalidValueExceptionHandler implements ExceptionMapper<InvalidValueException> {

	@Override
	public Response toResponse(InvalidValueException exception) {
		return Response.status(Status.BAD_REQUEST).entity(exception.getMessage()).build();
	}

}
