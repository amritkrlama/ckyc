package com.capiot.ckyc.bulkfile.exception;

public class InvalidSignatureException extends Exception{

	private static final long serialVersionUID = 1L;
	
	public InvalidSignatureException(String exception) {
		super(exception);
	}
	
	public InvalidSignatureException(String exception, Throwable cause) {
		super(exception, cause);
	}
}
