package com.capiot.ckyc.bulkfile.exception;

import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;
import javax.ws.rs.ext.ExceptionMapper;

public class PSFFileExceptionHandler implements ExceptionMapper<PSFFileException>{

	@Override
	public Response toResponse(PSFFileException exception) {
		return Response.status(Status.PRECONDITION_FAILED).entity(exception.getMessage()).build();
	}
	
	

}
