package com.capiot.ckyc.bulkfile.exception;

public class PSFFileException extends Exception{

	private static final long serialVersionUID = 1L;
	
	public PSFFileException(String message) {
		super(message);
	}
	
	public PSFFileException(String message, Throwable cause) {
		super(message, cause);
	}

}
