package com.capiot.ckyc.bulkfile.exception;

public class FieldMissingException extends Exception {

	private static final long serialVersionUID = 1L;
	
	public FieldMissingException(String message){
		super(message);
	}
	
	public FieldMissingException(String message, Throwable cause) {
		super(message, cause);
	}

}
