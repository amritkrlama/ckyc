package com.capiot.ckyc.bulkfile.exception;

import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;
import javax.ws.rs.ext.ExceptionMapper;

public class InvalidSignatureExceptionHandler implements ExceptionMapper<InvalidSignatureException>{

	@Override
	public Response toResponse(InvalidSignatureException exception) {
		return Response.status(Status.NOT_ACCEPTABLE).entity(exception.getMessage()).build();
	}

}
