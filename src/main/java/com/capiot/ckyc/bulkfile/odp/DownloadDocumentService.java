package com.capiot.ckyc.bulkfile.odp;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;
import java.util.Arrays;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.ParseException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.env.Environment;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.HttpServerErrorException;
import org.springframework.web.client.RestTemplate;

import com.capiot.ckyc.bulkfile.constant.ImageDetailsFields;
import com.capiot.ckyc.bulkfile.exception.InvalidRequestException;
import com.capiot.ckyc.bulkfile.support.CreateFileService;

/** This class Downloads the files/attachments for a record from ODP
 *  and moves them to the Generated folder.
 * 
 * @author Amrit
 *	
 */
@Component
public class DownloadDocumentService {
	
	@Autowired
	private Environment env;
	@Autowired
	private RestTemplate template;
	@Autowired
	TokenGenerateService tokenGenerateService;
	@Value("${business_center}")
	private String businessCenter;

	
	private final static Logger logger = LoggerFactory.getLogger(DownloadDocumentService.class);
	private String actualFileName = null;
	private String home = System.getProperty("user.home");
	String fileName = null;
	String fileSeparator = System.getProperty("file.separator");
	
	public void downloadDocument(JSONObject singleRecord) throws InvalidRequestException, IOException, ParseException {	
		JSONArray documents = (JSONArray) singleRecord.get(ImageDetailsFields.DOCUMENTS);
		if(documents!=null && documents.size()>0) {
			for(int index=0;index<documents.size();index++) {
				fetchDocumentFromODP((JSONObject)documents.get(index));
				moveDocument();
			}
		}
	}
	
	
	
	/*Method to fetch documents from ODP */
	public void fetchDocumentFromODP(JSONObject documents) throws InvalidRequestException, IOException, ParseException {
		
		String BUSINESS_CENTER =businessCenter;
		String DOMAIN = env.getProperty("domain");
		String ENTITY = env.getProperty("entity");
		
		if(documents!=null) {
			JSONObject document = (JSONObject) documents.get(ImageDetailsFields.ATTACHMENT);
			if(document!=null) {
				fileName = (String) document.get(ImageDetailsFields.FILE_NAME);
				JSONObject metadata = (JSONObject) document.get(ImageDetailsFields.METADATA);
				actualFileName = (String) metadata.get(ImageDetailsFields.FILE_NAME);
			}
		}
		
		ResponseEntity<byte[]> response = null;
		String url = BUSINESS_CENTER+DOMAIN+"/"+ENTITY+"/file/download/"+fileName;
		logger.info("URL for downloading the documents: " + url);
		
		

		HttpHeaders header = new HttpHeaders();
		header.setContentType(MediaType.APPLICATION_JSON);
		header.setAccept(Arrays.asList(MediaType.APPLICATION_OCTET_STREAM));
//		header.set("Authorization", "JWT " + RetrieveRecordService.token);
		header.add("Cookie", "Authorization="+ "JWT " + RetrieveRecordService.token);
		
		//To count the number of login hit
		int counter=0; 

		START: while (response == null) {
			try {
				logger.info("Downloading the files/attachments from ODP");
				HttpEntity<String> entity = new HttpEntity<>(header);
				long startTime = System.currentTimeMillis();
				response = template.exchange(url, HttpMethod.GET, entity, byte[].class);
				long elapsedTime = (System.currentTimeMillis() - startTime);
				if(response.getStatusCode() == HttpStatus.OK) {
					File downloadFolder = new File(home+env.getProperty("downloadFolder"));
					if(!downloadFolder.exists()) {
						downloadFolder.mkdirs();
					}
					File downloadedFile = new File(home+env.getProperty("downloadFolder")+fileSeparator+actualFileName);
					if(!downloadedFile.exists()) {
						downloadedFile.createNewFile();
					}
					Files.write(Paths.get(home+env.getProperty("downloadFolder")+fileSeparator+actualFileName), response.getBody());
				}
				logger.info("Documents have been successfully downloaded from ODP (to Downloads folder) in {} ms",elapsedTime);;
			} catch (HttpClientErrorException exception) {
				if (exception.getStatusText().contains("Unauthorized")) {
					counter++;
					logger.info("Invalid JWT token. Generating a new token from Login.");
					RetrieveRecordService.token = tokenGenerateService.generateToken();
				//	header.set("Authorization", "JWT " + token);
					header.add("Cookie", "Authorization="+ "JWT " + RetrieveRecordService.token);
					if(counter==5) {
						throw new InvalidRequestException("Unable to fetch documents/files from ODP in 5 attempts");
					}
					continue START;
				} else {
					throw new InvalidRequestException("Invalid request for downloading documents from ODP", exception);
				}
			}catch(HttpServerErrorException exception) {
				throw new InvalidRequestException("Invalid request for downloading documents from ODP",exception);
			}
		}
	
	}
	
	
	
	
	/*Method to move the document from "Downloads" folder to our generated "CKYC" folder*/
	public void moveDocument() {
		String fromPath = home+env.getProperty("downloadFolder")+fileSeparator+actualFileName;
		String toPath = CreateFileService.innerFolderPath;
		Path toCheckForSuccess = null;
		
		logger.info("Moving the documents from Downloads to Required folder");
		try {
			toCheckForSuccess = Files.move(Paths.get(fromPath), Paths.get(toPath).resolve(Paths.get(fromPath).getFileName()),
													StandardCopyOption.REPLACE_EXISTING);
		} catch (IOException e) {
			logger.info("Error in moving the files");
			e.printStackTrace();
		}
		
		if(toCheckForSuccess!=null) {
			logger.info("File has been successfully moved to the required folder");
		}else {
			logger.info("Failed to move the files");
		}
	}
	
}
