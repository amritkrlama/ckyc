package com.capiot.ckyc.bulkfile.odp;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import org.springframework.core.env.Environment;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.HttpServerErrorException;
import org.springframework.web.client.RestTemplate;

import com.capiot.ckyc.bulkfile.constant.DetailRecordFields;
import com.capiot.ckyc.bulkfile.exception.InvalidRequestException;
import com.fasterxml.jackson.core.JsonProcessingException;



/**
 * @author amrit
 *	To retrieve the latest audit
 */
@Component
@Configuration
@PropertySource("classpath:ckyc.properties")
public class RetrieveAuditService {
	
	@Autowired
	private Environment environment;
	
	@Autowired
	private RestTemplate template;
	@Autowired
	private TokenGenerateService tokenGenerateService;
	@Value("${business_center}")
	private String businessCenter;
	
	private static final Logger logger = LoggerFactory.getLogger(RetrieveAuditService.class);

	
	/**
	 * 
	 * @param Single record of a person in ODP(DCH)
	 * @return All Audits of the record
	 * @throws JsonProcessingException
	 * @throws ParseException
	 * @throws InvalidRequestException
	 */
	@SuppressWarnings("unchecked")
	public JSONArray fetchAllAudits(JSONObject record) throws JsonProcessingException, ParseException, InvalidRequestException{
		
		final String BUSINESS_CENTER = businessCenter;
		final String DOMAIN = environment.getProperty("domain");
		final String ENTITY = environment.getProperty("entity");
		
		ResponseEntity<String> response = null;
		JSONParser parser = new JSONParser();
			
		/*Fetching FIReference Number from the record*/
		String referenceNumber = (String) record.get(DetailRecordFields.CKYC_FI_REFERENCE_NUMBER);
		
		JSONObject filterForAudit = new JSONObject();
		filterForAudit.put("data._id", referenceNumber);
		logger.info("Filter for Audit: {}", String.valueOf(filterForAudit));
		
		String url = BUSINESS_CENTER+DOMAIN+"/"+ENTITY+"/audit?filter={filterForAudit}";
		
		HttpHeaders header = new HttpHeaders();
		header.setContentType(MediaType.APPLICATION_JSON);
		header.set("Authorization", "JWT " + RetrieveRecordService.token);
		logger.info("URL: " + url);

		START: while (response == null) {
			try {
				HttpEntity<String> entity = new HttpEntity<>(header);	
				long startTime = System.currentTimeMillis();
				
				response = template.exchange(url, HttpMethod.GET, entity, String.class, filterForAudit.toJSONString());
				
				long elapsedTime = (System.currentTimeMillis() - startTime);
				logger.info("Time elapsed (in milliseconds) for getting all the audits for the current record from ODP: "+elapsedTime);;
			} catch (HttpClientErrorException exception) {
				if (exception.getStatusText().contains("Unauthorized")) {
					logger.debug("Invalid JWT token. Generating a new token from Login.");
					RetrieveRecordService.token = tokenGenerateService.generateToken();
					logger.debug("JWT generated: {}", RetrieveRecordService.token);
					header.set("Authorization", "JWT " + RetrieveRecordService.token);
					continue START;
				} else {
					throw new InvalidRequestException("Invalid request for fetching records", exception);
				}
			}catch(HttpServerErrorException exception) {
				throw new InvalidRequestException("Invalid request for fetching records",exception);
			}
		}
		
		
		
		
		JSONArray audits = (JSONArray) parser.parse(response.getBody().toString());
		logger.info("Number of Audits fetched {}", audits.size());
		
		//Removing the last audit (as it comes when we add the record)
		if(audits.size()>=1) {
			audits.remove(audits.size()-1);
		}
		
		return fetchAuditsBetweenInterval(audits);
		
	}
	
	
	
	/**
	 * 
	 * @param All audits of a record
	 * @return Array of audits between the required interval
	 */
	@SuppressWarnings("unchecked")
	public JSONArray fetchAuditsBetweenInterval(JSONArray audits) {
		
		final String INTERVAL_IN_HOURS = environment.getProperty("interval");
		
		JSONArray updatedRecordsBetweenInterval = new JSONArray();
		
		for(int index=0; index<audits.size();index++) {
			JSONObject audit = (JSONObject) audits.get(index);
			if(audit!=null) {
				JSONObject metaData = (JSONObject) audit.get("_metadata");
				if(metaData!=null) {
					String lastUpdated = (String) metaData.get("lastUpdated");
					if(lastUpdated!=null) {
						try {
							Date lastUpdatedDate = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.sss").parse(lastUpdated);
							if(lastUpdatedDate.after(getPastDateTime(INTERVAL_IN_HOURS)) ||
															lastUpdatedDate.equals(getPastDateTime(INTERVAL_IN_HOURS))) {
								updatedRecordsBetweenInterval.add(audit);
							}
						} catch (java.text.ParseException e) {
							logger.debug("Error while parsing the last updated date for fetching the audits");
							e.printStackTrace();
						}
						
					}else {
						logger.info("No Last Updated date found in Audits while fetching the last updated record");
					}
				}else {
					logger.info("No metadata found in Audit while fetching last updated record");
				}
			}else {
				logger.info("No Audits found while fetching last updated records");
			}
		}
		
		return updatedRecordsBetweenInterval;
		
	}
	

	public String getCurrentDateTime(){
		DateFormat sdf = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.sss");
	    String currentDate= sdf.format(new Date()); 
	    return currentDate;
	}

	
	public Date getPastDateTime(String interval){
	    Calendar calendar = Calendar.getInstance();
	    calendar.add(Calendar.HOUR, -(Integer.parseInt(interval)));
	    Date pastDateTime = calendar.getTime();
	    return pastDateTime;
	}
	
	
	

}
