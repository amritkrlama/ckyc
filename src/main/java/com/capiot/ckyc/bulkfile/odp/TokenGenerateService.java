package com.capiot.ckyc.bulkfile.odp;

import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import org.springframework.core.env.Environment;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestClientException;
import org.springframework.web.client.RestTemplate;

import com.capiot.ckyc.bulkfile.exception.InvalidRequestException;
import com.fasterxml.jackson.core.JsonProcessingException;

@Component
@Configuration
@PropertySource("classpath:ckyc.properties")
public class TokenGenerateService{
	
	@Autowired
	private Environment env;
	@Autowired
	RestTemplate template;
	@Value("${login}")
	private String login;

	private static final Logger logger = LoggerFactory.getLogger(TokenGenerateService.class);
	
	/**
	 * 
	 * @return Generated JWT from login
	 * @throws JsonProcessingException
	 * @throws ParseException
	 * @throws InvalidRequestException
	 */

	@SuppressWarnings("unchecked")
	public String generateToken() throws JsonProcessingException, ParseException, InvalidRequestException {
		String loginURL = login;  //login url
		String username = env.getProperty("uname");  //username
		String password = env.getProperty("pword");  //password
		String loginDetails = "";
		
		JSONObject userAsJSON = new JSONObject();
		userAsJSON.put("username", username);
		userAsJSON.put("password", password);
		String userAsJSONString = userAsJSON.toJSONString();
		HttpHeaders header = new HttpHeaders();
		header.setContentType(MediaType.APPLICATION_JSON);
		HttpEntity<String> request = new HttpEntity<>(userAsJSONString, header);
		logger.info("Logging in to generate JWT token. URL {}, Body {}", loginURL, userAsJSONString);
		
		try {
			loginDetails = template.postForObject(loginURL, request, String.class);
		}catch(RestClientException exception) {
			throw new InvalidRequestException("Invalid login request", exception);
		}
		logger.info("JWT has been captured successfully");
		JSONParser parser = new JSONParser();
		JSONObject loginDetailsAsJSON = (JSONObject) parser.parse(loginDetails);
		return (String) loginDetailsAsJSON.get("token");
	}


}