package com.capiot.ckyc.bulkfile.odp;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Arrays;
import java.util.HashMap;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.ParseException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import org.springframework.core.env.Environment;
import org.springframework.core.io.FileSystemResource;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.HttpServerErrorException;
import org.springframework.web.client.RestTemplate;

import com.capiot.ckyc.bulkfile.constant.DetailRecordFields;
import com.capiot.ckyc.bulkfile.exception.InvalidRequestException;
import com.capiot.ckyc.bulkfile.support.CommonUtility;
import com.fasterxml.jackson.core.JsonProcessingException;

@RestController
@Configuration
@PropertySource("classpath:ckyc.properties")
public class PostToODPService {

	public static final Logger logger = LoggerFactory.getLogger(PostToODPService.class);
	@Autowired
	private Environment environment;
	@Autowired
	private RestTemplate template;
	@Autowired
	private TokenGenerateService tokenGenerateService;
	@Value("${business_center}")
	private String businessCenter;
	
	long dummyCkycNumber = 31828712837912L;
	private final String SUCCESS = "Success";
	private final String home = System.getProperty("user.home");

	@SuppressWarnings("unchecked")
	@PostMapping("/ckychub")
	public JSONArray postSuccessRecordsToCKYCHub(JSONArray records)
			throws JsonProcessingException, ParseException, InvalidRequestException {
		JSONArray tempRecords = new JSONArray();
		final String appCenter =businessCenter;
		final String appName = environment.getProperty("domain");
		final String downloadFolder = home+environment.getProperty("downloadFolder");
		final String dataServiceName = environment.getProperty("ckycHub");

		ResponseEntity<JSONArray> response = null;
		final String urlForPostToCkycHub = appCenter + appName + "/" + dataServiceName;
		HttpHeaders header = new HttpHeaders();
		header.setContentType(MediaType.APPLICATION_JSON);
		header.set("Authorization", "JWT " + RetrieveRecordService.token);
		logger.info("URL for posting successful records to ODP(CKYC Hub): {}", urlForPostToCkycHub);

		for (int index = 0; index < records.size(); index++) {
			HashMap<String, Object> tmpMap = (HashMap<String, Object>) records.get(index);
			JSONObject tmp = new JSONObject();
			tmpMap.forEach((k, v) -> {
				if(k.equalsIgnoreCase(DetailRecordFields.CKYC_FI_REFERENCE_NUMBER)) {
					++dummyCkycNumber;
					tmp.put(k, dummyCkycNumber);
				}else if (k.equalsIgnoreCase(DetailRecordFields.CURRENT_STATE)) {
					tmp.put(k, SUCCESS);
				} else if (k.equalsIgnoreCase(DetailRecordFields.DOCUMENTS)) {
					JSONArray tempDocuments = new JSONArray();
					JSONArray documents = (JSONArray) v;
					for (int j = 0; j < documents.size(); j++) {
						JSONObject tempDoc = new JSONObject();
						HashMap<String, Object> doc = (HashMap<String, Object>) documents.get(j);
						doc.forEach((key, value) -> {
							if (key.equalsIgnoreCase(DetailRecordFields.DOCUMENT)) {
								JSONObject docObject = (JSONObject) value;
								String fileName = (String) docObject.get(DetailRecordFields.FILE_NAME);
								JSONObject metadata = (JSONObject) docObject.get("metadata");
								String actualFileName = (String) metadata.get(DetailRecordFields.FILE_NAME);
								try {
									fetchDocumentFromODP(fileName, actualFileName);
									JSONObject uploadAttachmentToODP = uploadAttachmentToODP(downloadFolder,
											actualFileName);
									tempDoc.put(DetailRecordFields.DOCUMENT, uploadAttachmentToODP);
								} catch (JsonProcessingException | ParseException | InvalidRequestException e) {
									e.printStackTrace();
								} catch (IOException e) {
									e.printStackTrace();
								}
							} else {
								tempDoc.put(key, value);
							}
						});
						tempDocuments.add(tempDoc);
					}
					tmp.put(DetailRecordFields.DOCUMENTS, tempDocuments);
				} else {
					tmp.put(k, v);
				}

			});
			tempRecords.add(tmp);
		}

		logger.info("Request to Post to CKYC Hub (Successful records): {}", tempRecords.toJSONString());
		START: while (response == null) {
			try {
				HttpEntity<JSONArray> entity = new HttpEntity<JSONArray>(tempRecords, header);
				logger.info("Posting successful records to CKYCHub DataService.");
				long startTime = System.currentTimeMillis();
				response = template.exchange(urlForPostToCkycHub, HttpMethod.POST, entity, JSONArray.class);
				long elapsedTime = (System.currentTimeMillis() - startTime);
				logger.info(
						"Time elapsed (in milliseconds) for Posting records to CKYCHub DataService: " + elapsedTime);
			} catch (HttpClientErrorException exception) {
				if (exception.getStatusText().contains("Unauthorized")) {
					logger.info("Invalid JWT token. Generating a new token from Login.");
					RetrieveRecordService.token = tokenGenerateService.generateToken();
					header.set("Authorization", "JWT " + RetrieveRecordService.token);
					continue START;
				} else {
					throw new InvalidRequestException("Invalid request for adding records to ODP", exception);
				}
			} catch (HttpServerErrorException exception) {
				throw new InvalidRequestException("Invalid request for adding records to ODP", exception);
			}
		}
		return tempRecords;
	}

	@SuppressWarnings("unchecked")
	@PutMapping("/ckycTransaction")
	public JSONArray updateRecordsToSuccessful(JSONArray records)
			throws InvalidRequestException, JsonProcessingException, ParseException {
		JSONArray finalArrayOfJSON = new JSONArray();
		final String appCenter = businessCenter;
		final String appName = environment.getProperty("domain");
		final String dataServiceName = environment.getProperty("entity");

		String urlToCKYCTransaction = appCenter + appName + "/" + dataServiceName;
		String[] id = new String[1];
		HttpHeaders header = new HttpHeaders();
		header.setContentType(MediaType.APPLICATION_JSON);
		header.set("Authorization", "JWT " + RetrieveRecordService.token);

		for (int index = 0; index < records.size(); index++) {
			ResponseEntity<JSONObject> response = null;
			HashMap<String, Object> tmpMap = (HashMap<String, Object>) records.get(index);
			JSONObject tmp = new JSONObject();
			tmpMap.forEach((k, v) -> {
				if (k.equalsIgnoreCase(DetailRecordFields.CKYC_FI_REFERENCE_NUMBER))
					id[0] = (String) v;

				if (k.equalsIgnoreCase(DetailRecordFields.CURRENT_STATE)) {
					tmp.put(k, SUCCESS);
				} else if (k.equalsIgnoreCase(DetailRecordFields.TRANSACTION_STATE)) {
					JSONArray transactionStates = (JSONArray) v;
					JSONArray newTransactionStates = new JSONArray();
					for (int j = 0; j < transactionStates.size(); j++) {
						HashMap<String, Object> tempTransactionState = (HashMap<String, Object>) transactionStates
								.get(j);
						JSONObject tmpTransactionState = new JSONObject();
						tempTransactionState.forEach((key, value) -> {
							tmpTransactionState.put(key, value);
						});
						newTransactionStates.add(tmpTransactionState);
					}
					JSONObject newTmpTransactionState = new JSONObject();
					newTmpTransactionState.put(DetailRecordFields.TRANSACTION_STATE_STATUS, "Success");
					newTmpTransactionState.put(DetailRecordFields.STATE_TIMESTAMP,
							CommonUtility.getCurrentDateTimeStamp());
					newTransactionStates.add(newTmpTransactionState);

					tmp.put(k, newTransactionStates);
				} else {
					tmp.put(k, v);
				}
			});

			String urlForUpdatingTransactionToSuccess = urlToCKYCTransaction + "/" + id[0];
			logger.info("URL for updating the records to successful in ODP(CKYC Transaction): {}",
					urlForUpdatingTransactionToSuccess);
			logger.info("Request to Update CKYC Transaction for successful records: {}", tmp.toJSONString());

			START: while (response == null) {
				try {
					HttpEntity<JSONObject> entity = new HttpEntity<JSONObject>(tmp, header);
					logger.info("Updating successful records to CKYC Transaction DataService.");
					long startTime = System.currentTimeMillis();
					response = template.exchange(urlForUpdatingTransactionToSuccess, HttpMethod.PUT, entity,
							JSONObject.class);
					long elapsedTime = (System.currentTimeMillis() - startTime);
					logger.info(
							"Time elapsed (in milliseconds) for Updating successful records to CKYC Transaction	 DataService: "
									+ elapsedTime);
				} catch (HttpClientErrorException exception) {
					if (exception.getStatusText().contains("Unauthorized")) {
						logger.info("Invalid JWT token. Generating a new token from Login.");
						RetrieveRecordService.token = tokenGenerateService.generateToken();
						header.set("Authorization", "JWT " + RetrieveRecordService.token);
						continue START;
					} else {
						throw new InvalidRequestException("Invalid request for adding records to ODP", exception);
					}
				} catch (HttpServerErrorException exception) {
					throw new InvalidRequestException("Invalid request for adding records to ODP", exception);
				}
			}
		}

		return finalArrayOfJSON;
	}

	@SuppressWarnings("unchecked")
	@GetMapping("/file/upload")
	public void postBulkFileToODP(String filePath, String fileName)
			throws JsonProcessingException, ParseException, InvalidRequestException {
		final String appCenter = businessCenter;
		final String appName = environment.getProperty("domain");
		final String dataServiceName = environment.getProperty("bulk_file_data_service");
		JSONObject responseFromUpload = uploadBulkFileToODP(filePath, fileName);

		ResponseEntity<JSONObject> response = null;
		final String urlForPostFile = appCenter + appName + "/" + dataServiceName;
		HttpHeaders header = new HttpHeaders();
		header.setContentType(MediaType.APPLICATION_JSON);
		header.set("Authorization", "JWT " + RetrieveRecordService.token);
		logger.info("URL for posting bulk file to ODP: {}", urlForPostFile);
		JSONObject request = new JSONObject();
		request.put("timestamp", CommonUtility.getCurrentDateTimeStamp());
		request.put("file", responseFromUpload);

		START: while (response == null) {
			try {
				HttpEntity<JSONObject> entity = new HttpEntity<JSONObject>(request, header);
				logger.info("Posting uploaded bulk file to ODP(BulkFile) DataService.");
				long startTime = System.currentTimeMillis();
				response = template.exchange(urlForPostFile, HttpMethod.POST, entity, JSONObject.class);
				long elapsedTime = (System.currentTimeMillis() - startTime);
				logger.info(
						"Time elapsed (in milliseconds) for Posting uploaded bulk file to ODP(BulkFile) DataService: "
								+ elapsedTime);
				;
			} catch (HttpClientErrorException exception) {
				if (exception.getStatusText().contains("Unauthorized")) {
					logger.info("Invalid JWT token. Generating a new token from Login.");
					RetrieveRecordService.token = tokenGenerateService.generateToken();
					header.set("Authorization", "JWT " + RetrieveRecordService.token);
					continue START;
				} else {
					throw new InvalidRequestException("Invalid request for posting uploaded bulk file to ODP",
							exception);
				}
			} catch (HttpServerErrorException exception) {
				throw new InvalidRequestException("Invalid request for posting uploaded bulk file to ODP", exception);
			}
		}

	}

	public JSONObject uploadBulkFileToODP(String filePath, String fileName)
			throws JsonProcessingException, ParseException, InvalidRequestException {
		final String appCenter = businessCenter;
		final String appName = environment.getProperty("domain");
		final String dataServiceName = environment.getProperty("bulk_file_data_service");
		ResponseEntity<JSONObject> response = null;
		final String urlForFileUpload = appCenter + appName + "/" + dataServiceName + "/file/upload";
		HttpHeaders header = new HttpHeaders();
		header.setContentType(MediaType.MULTIPART_FORM_DATA);
		header.set("Authorization", "JWT " + RetrieveRecordService.token);
		logger.info("URL for bulk file upload: " + urlForFileUpload);
		MultiValueMap<String, Object> body = new LinkedMultiValueMap<>();
		body.add("file", new FileSystemResource(filePath + fileName + ".zip"));

		START: while (response == null) {
			try {
				HttpEntity<MultiValueMap<String, Object>> entity = new HttpEntity<MultiValueMap<String, Object>>(body,
						header);
				logger.info("Uploading bulk file {} to ODP.", filePath + fileName + ".zip");
				long startTime = System.currentTimeMillis();
				response = template.exchange(urlForFileUpload, HttpMethod.POST, entity, JSONObject.class);
				long elapsedTime = (System.currentTimeMillis() - startTime);
				logger.info("Time elapsed (in milliseconds) for uploading bulk file to ODP: " + elapsedTime);
				;
			} catch (HttpClientErrorException exception) {
				if (exception.getStatusText().contains("Unauthorized")) {
					logger.info("Invalid JWT token. Generating a new token from Login.");
					RetrieveRecordService.token = tokenGenerateService.generateToken();
					header.set("Authorization", "JWT " + RetrieveRecordService.token);
					continue START;
				} else {
					throw new InvalidRequestException("Invalid request for uploading the bulk file to ODP", exception);
				}
			} catch (HttpServerErrorException exception) {
				throw new InvalidRequestException("Invalid request for uploading the bulk file to ODP", exception);
			}
		}
		return response.getBody();
	}

	public JSONObject uploadAttachmentToODP(String filePath, String fileName)
			throws JsonProcessingException, ParseException, InvalidRequestException {
		final String appCenter = businessCenter;
		final String appName = environment.getProperty("domain");
		final String dataServiceName = environment.getProperty("ckycHub");
		ResponseEntity<JSONObject> response = null;
		final String urlForFileUpload = appCenter + appName + "/" + dataServiceName + "/file/upload";
		HttpHeaders header = new HttpHeaders();
		header.setContentType(MediaType.MULTIPART_FORM_DATA);
		header.set("Authorization", "JWT " + RetrieveRecordService.token);
		logger.info("URL for attachment upload: " + urlForFileUpload);
		MultiValueMap<String, Object> body = new LinkedMultiValueMap<>();
		body.add("file", new FileSystemResource(filePath + File.separator + fileName));

		START: while (response == null) {
			try {
				HttpEntity<MultiValueMap<String, Object>> entity = new HttpEntity<MultiValueMap<String, Object>>(body,
						header);
				logger.info("Uploading attachment {} to ODP.", filePath + File.separator + fileName);
				long startTime = System.currentTimeMillis();
				response = template.exchange(urlForFileUpload, HttpMethod.POST, entity, JSONObject.class);
				long elapsedTime = (System.currentTimeMillis() - startTime);
				logger.info("Time elapsed (in milliseconds) for uploading attachment to ODP: " + elapsedTime);
				;
			} catch (HttpClientErrorException exception) {
				if (exception.getStatusText().contains("Unauthorized")) {
					logger.info("Invalid JWT token. Generating a new token from Login.");
					RetrieveRecordService.token = tokenGenerateService.generateToken();
					header.set("Authorization", "JWT " + RetrieveRecordService.token);
					continue START;
				} else {
					throw new InvalidRequestException("Invalid request for uploading the attachment to ODP", exception);
				}
			} catch (HttpServerErrorException exception) {
				throw new InvalidRequestException("Invalid request for uploading the attachment to ODP", exception);
			}
		}
		return response.getBody();
	}

	public void fetchDocumentFromODP(String fileName, String actualFileName)
			throws InvalidRequestException, IOException, ParseException {
		String BUSINESS_CENTER = businessCenter;
		String DOMAIN = environment.getProperty("domain");
		String ENTITY = environment.getProperty("entity");

		ResponseEntity<byte[]> response = null;
		String url = BUSINESS_CENTER + DOMAIN + "/" + ENTITY + "/file/download/" + fileName;
		logger.info("URL for downloading the documents: " + url);

		HttpHeaders header = new HttpHeaders();
		header.setContentType(MediaType.APPLICATION_JSON);
		header.setAccept(Arrays.asList(MediaType.APPLICATION_OCTET_STREAM));
		header.add("Cookie", "Authorization=" + "JWT " + RetrieveRecordService.token);

		START: while (response == null) {
			try {
				logger.info("Downloading the files/attachments from ODP");
				HttpEntity<String> entity = new HttpEntity<>(header);
				long startTime = System.currentTimeMillis();
				response = template.exchange(url, HttpMethod.GET, entity, byte[].class);
				long elapsedTime = (System.currentTimeMillis() - startTime);
				if (response.getStatusCode() == HttpStatus.OK) {
					File downloadFolder = new File(home+environment.getProperty("downloadFolder"));
					if (!downloadFolder.exists()) {
						downloadFolder.mkdirs();
					}
					File downloadedFile = new File(home+environment.getProperty("downloadFolder") + File.separator + actualFileName);
					if (!downloadedFile.exists()) {
						downloadedFile.createNewFile();
					}
					Files.write(Paths.get(home+environment.getProperty("downloadFolder") + File.separator + actualFileName),
							response.getBody());
				}
				logger.info("Documents have been successfully downloaded from ODP (to Downloads folder) in {} ms",
						elapsedTime);
				;
			} catch (HttpClientErrorException exception) {
				if (exception.getStatusText().contains("Unauthorized")) {
					logger.info("Invalid JWT token. Generating a new token from Login.");
					RetrieveRecordService.token = tokenGenerateService.generateToken();
					header.add("Cookie", "Authorization=" + "JWT " + RetrieveRecordService.token);
					continue START;
				} else {
					throw new InvalidRequestException("Invalid request for downloading documents from ODP", exception);
				}
			} catch (HttpServerErrorException exception) {
				throw new InvalidRequestException("Invalid request for downloading documents from ODP", exception);
			}
		}

	}

}
