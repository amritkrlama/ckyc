package com.capiot.ckyc.bulkfile.odp;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import org.springframework.core.env.Environment;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.HttpServerErrorException;
import org.springframework.web.client.RestTemplate;

import com.capiot.ckyc.bulkfile.constant.DetailRecordFields;
import com.capiot.ckyc.bulkfile.constant.DetailRecordValues;
import com.capiot.ckyc.bulkfile.exception.InvalidRequestException;
import com.capiot.ckyc.bulkfile.support.CommonUtility;
import com.fasterxml.jackson.core.JsonProcessingException;


/**
 * @author Amrit
 * To fetch records from ODP
 */

@Configuration
@PropertySource("classpath:ckyc.properties")
public class RetrieveRecordService{

	@Autowired
	private Environment environment;
	
	@Autowired
	RestTemplate template;
	@Autowired
	TokenGenerateService tokenGenerateService;
	@Value("${business_center}")
	private String businessCenter;

	/* JWT Token */
	public static String token = "someinvalidtokentostartwith";  
	
	private final static Logger logger = LoggerFactory.getLogger(RetrieveRecordService.class);
	JSONObject filter = new JSONObject();
	JSONObject rangeDateFilter = new JSONObject();
	JSONParser parser = new JSONParser();
	

	
	/* Method to fetch all the documents from an entity */
	@SuppressWarnings("unchecked")
	public ResponseEntity<?> fetchPendingRecords() throws InvalidRequestException, JsonProcessingException, ParseException{
		
		final String BUSINESS_CENTER = businessCenter;
		final String DOMAIN = environment.getProperty("domain");
		final String ENTITY = environment.getProperty("entity");
		
		ResponseEntity<String> response = null;

		filter.put(DetailRecordFields.CURRENT_STATE, DetailRecordValues.TRANSACTION_STATUS_PENDING);
		String filterAsString = filter.toJSONString();
		logger.info("Filter for fetching new records: {}",filterAsString);
		
		String url = BUSINESS_CENTER+DOMAIN+"/"+ENTITY+"?count=-1&filter={filterAsString}&expand=true";
		HttpHeaders header = new HttpHeaders();
		header.setContentType(MediaType.APPLICATION_JSON);
		header.set("Authorization", "JWT " + token);
		logger.info("URL: " + url);

		START: while (response == null) {
			try {
				HttpEntity<String> entity = new HttpEntity<>(header);
				logger.info("Fetching Pending records from ODP.");
				long startTime = System.currentTimeMillis();
				response = template.exchange(url, HttpMethod.GET, entity, String.class, filterAsString);
				long elapsedTime = (System.currentTimeMillis() - startTime);
				logger.info("Time elapsed (in milliseconds) for getting all the Pending records from ODP: "+elapsedTime);;
			} catch (HttpClientErrorException exception) {
				if (exception.getStatusText().contains("Unauthorized")) {
					logger.info("Invalid JWT token. Generating a new token from Login.");
					long startTime = System.currentTimeMillis();
					token = tokenGenerateService.generateToken();
					long elapsedTime = (System.currentTimeMillis() - startTime);
					header.set("Authorization", "JWT " + token);
					logger.info("Time elapsed (in milliseconds) for generating the JWT: "+elapsedTime);;
					continue START;
				} else {
					throw new InvalidRequestException("Invalid request for fetching records", exception);
				}
			}catch(HttpServerErrorException exception) {
				throw new InvalidRequestException("Invalid request for fetching records",exception);
			}
		}
		return response;
	}
	
	
	
	/* Method to fetch all the documents from an entity */
	@SuppressWarnings("unchecked")
	public ResponseEntity<?> fetchUpdatedRecords() throws InvalidRequestException, JsonProcessingException, ParseException {
		
		
		final String BUSINESS_CENTER =businessCenter;
		final String DOMAIN = environment.getProperty("domain");
		final String ENTITY = environment.getProperty("entity");
		final String INTERVAL_IN_HOURS = environment.getProperty("interval");
		
		
		ResponseEntity<String> response = null;
		
		JSONObject notEqualsDateFilter = new JSONObject();
		JSONObject notInRangeDateFilter = new JSONObject();
		
		
		//creating the filter
		rangeDateFilter.put("$gte",""+getPastDateTime(INTERVAL_IN_HOURS)+"");
		rangeDateFilter.put("$lte",""+CommonUtility.getCurrentDateTime()+"");
		
		notEqualsDateFilter.put("$gte",""+getPastDateTime(INTERVAL_IN_HOURS)+"");
		notEqualsDateFilter.put("$lte",""+CommonUtility.getCurrentDateTime()+"");
		notInRangeDateFilter.put("$not", notEqualsDateFilter);
		
		filter.put("_metadata.lastUpdated", rangeDateFilter);
		filter.put("_metadata.createdAt", notInRangeDateFilter);
		
		
		String filterAsString = filter.toJSONString();
		logger.info("Filter for fetching updated records: {}",filterAsString);
		
		String url = BUSINESS_CENTER+DOMAIN+"/"+ENTITY+"/?count=-1&filter={filterAsString}&expand=true";
		//+"/?filter={filterAsString}&expand=true"
		HttpHeaders header = new HttpHeaders();
		header.setContentType(MediaType.APPLICATION_JSON);
		header.set("Authorization", "JWT " + token);
		logger.info("URL: " + url);

		START: while (response == null) {
			try {
				HttpEntity<String> entity = new HttpEntity<>(header);
				logger.info("Fetching updated records from ODP.");
				long startTime = System.currentTimeMillis();
				response = template.exchange(url, HttpMethod.GET, entity, String.class, filterAsString);
				long elapsedTime = (System.currentTimeMillis() - startTime);
				logger.info("Time elapsed (in milliseconds) for getting all the Updated records from ODP: "+elapsedTime);;
			} catch (HttpClientErrorException exception) {
				if (exception.getStatusText().contains("Unauthorized")) {
					logger.debug("Invalid JWT token. Generating a new token from Login.");
					token = tokenGenerateService.generateToken();
					header.set("Authorization", "JWT " + token);
					continue START;
				} else {
					throw new InvalidRequestException("Invalid request for fetching records", exception);
				}
			}catch(HttpServerErrorException exception) {
				throw new InvalidRequestException("Invalid request for fetching records",exception);
			}
		}
		return response;
	}
	
	


	
	public static String getPastDateTime(String interval){
		DateFormat sdf = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.sss");
	    Calendar calendar = Calendar.getInstance();
	    calendar.add(Calendar.HOUR, -(Integer.parseInt(interval)));
	    Date pastDateTime = calendar.getTime();
	    String past = sdf.format(pastDateTime);
	    return past;
	}


}
