package com.capiot.ckyc.bulkfile.constant;

public final class RelatedPersonValues {
	
	private RelatedPersonValues() {
		super();
	}
	
	public static final String RECORD_TYPE = "40";
	public static final String ADDITION = "Addition of related person";
	public static final String ADDITION_VALUE = "01";
	public static final String DELETION = "Deletion of related person";
	public static final String DELETION_VALUE = "02";
	public static final String CERTIFIED_COPIES = "Certified Copies";
	public static final String CERTIFIED_COPIES_VALUE = "01";
}
