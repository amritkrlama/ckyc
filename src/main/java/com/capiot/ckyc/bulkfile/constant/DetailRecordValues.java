package com.capiot.ckyc.bulkfile.constant;

public final class DetailRecordValues {
	
	private DetailRecordValues() {
		super();
	}
	
	public static final String RECORD_TYPE = "20";
	public static final String APPLICATION_TYPE_NEW = "New";
	public static final String APPLICATION_TYPE_NEW_VALUE ="01";
	public static final String APPLICATION_TYPE_UPDATE = "Update";
	public static final String APPLICATION_TYPE_UPDATE_VALUE = "03";
	public static final String APPLICATION_TYPE_DOWNLOAD = "Download";
	public static final String APPLICATION_TYPE_DOWNLOAD_VALUE = "04";
	public static final String ACCOUNT_TYPE_NORMAL = "01";
	public static final String ACCOUNT_TYPE_SMALL = "02";
	public static final String ACCOUNT_TYPE_SIMPLIFIED = "03";
	public static final String ACCOUNT_TYPE_OTP_BASED = "04";
	public static final String ACCOUNT_TYPE_NORMAL_VALUE = "Normal";
	public static final String ACCOUNT_TYPE_SMALL_VALUE = "Small";
	public static final String ACCOUNT_TYPE_SIMPLIFIED_VALUE = "Simplified";
	public static final String ACCOUNT_TYPE_OTP_BASED_VALUE = "OTP Based E-KYC";
	public static final String CONSTITUTION_TYPE_INDIVIDUAL = "01";
	public static final String CONSTITUTION_TYPE_LEGAL = "02";
	public static final String FLAG_FATHER = "01";
	public static final String FLAG_SPOUSE = "02";
	public static final String GENDER_MALE = "M";
	public static final String GENDER_FEMALE = "F";
	public static final String GENDER_TRANSGENDER = "T";
	public static final String MARITAL_STATUS_MARRIED = "01";
	public static final String MARITAL_STATUS_UNMARRIED = "02";
	public static final String MARITAL_STATUS_OTHERS = "03";
	public static final String RESIDENTIAL_STATUS_VALUE_RESIDENTIAL_INDIVIDUAL = "Resident Individual";
	public static final String RESIDENTIAL_STATUS_VALUE_NRI = "Non Resident Indian";
	public static final String RESIDENTIAL_STATUS_VALUE_FOREIGN_NATIONAL = "Foreign National";
	public static final String RESIDENTIAL_STATUS_VALUE_PERSON_OF_INDIAN_ORIGIN = "Person of Indian Origin";
	public static final String RESIDENTIAL_STATUS_INDIVIDUAL = "01";
	public static final String RESIDENTIAL_STATUS_NRI = "02";
	public static final String RESIDENTIAL_STATUS_FOREIGN_NATIONAL = "03";
	public static final String RESIDENTIAL_STATUS_PERSON_OF_INDIAN_ORIGIN = "04";
	public static final String FLAG_INDICATING_APPLICANT_RESIDENT_OUTSIDE_INDIA_YES = "01";
	public static final String FLAG_INDICATING_APPLICANT_RESIDENT_OUTSIDE_INDIA_NO = "02";
	public static final String CURRENT_PERMANENT_OVERSEAS_ADDRESS_TYPE_RESIDENTBUSINESS = "01";
	public static final String CURRENT_PERMANENT_OVERSEAS_ADDRESS_TYPE_RESIDENTIAL = "02";
	public static final String CURRENT_PERMANENT_OVERSEAS_ADDRESS_TYPE_BUSINESS = "03";
	public static final String CURRENT_PERMANENT_OVERSEAS_ADDRESS_TYPE_REGISTEREDOFFICE = "04";
	public static final String CURRENT_PERMANENT_OVERSEAS_ADDRESS_TYPE_UNSPECIFIED = "05";
	public static final String FLAG_INDICATING_CURRENT_ADDRESS_ISSAMEAS_CORRESPONDENCE_ADDRESS_YES = "Y";
	public static final String FLAG_INDICATING_CURRENT_ADDRESS_ISSAMEAS_CORRESPONDENCE_ADDRESS_NO = "N";
	public static final String JURISDICTION_ADDRESS_TYPE_SAME_AS_CURRENT_ADDRESS = "01";
	public static final String JURISDICTION_ADDRESS_TYPE_SAME_AS_CORRESPONDENCE_ADDRESS = "02";
	public static final String JURISDICTION_ADDRESS_TYPE_DIFFERENT = "03";
	public static final String TYPES_OF_DOCUMENTS_SUBMITTED_CERTIFIED_COPIES = "01";
	public static final String ACCOUNT_TYPE_HOLDER_FLAG_US_REPORTABLE = "01";
	public static final String ACCOUNT_TYPE_HOLDER_FLAG_OTHER_REPORTABLE = "02"; 
	public static final String UPDATED_FLAG_YES = "01";
	public static final String UPDATED_FLAG_NO = "02";
	public static final String ADDRESS_TYPE_RESIDENT_BUSINESS = "Resident/Business";
	public static final String ADDRESS_TYPE_RESIDENT = "resident";
	public static final String ADDRESS_TYPE_BUSINESS = "business";
	public static final String ADDRESS_TYPE_REGISTERED_OFFICE = "registeredOffice";
	public static final String ADDRESS_TYPE_UNSPECIFIED = "unspecified";
	public static final String TRANSACTION_STATUS_PENDING = "Pending";
	
}
