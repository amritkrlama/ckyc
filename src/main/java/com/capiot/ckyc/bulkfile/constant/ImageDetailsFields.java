package com.capiot.ckyc.bulkfile.constant;

public final class ImageDetailsFields {
	
	private ImageDetailsFields() {
		super();
	}
	
	public static final String DOCUMENTS = "documents";
	public static final String ATTACHMENT = "document";
	public static final String METADATA = "metadata";
	public static final String FILE_NAME = "filename";
	public static final String FILE_TYPE = "documentType";
	public static final String FILE_TYPE_CODE = "code";
	public static final String FILE_TYPE_NAME = "name";
	public static final String GLOBAL_OR_LOCAL = "globalLocal";
	public static final String BRANCH_CODE = "branchCode";
	public static final String FILLER_ONE = "filler1";
	public static final String FILLER_TWO = "filler2";
	public static final String FILLER_THREE = "filler3";
}
