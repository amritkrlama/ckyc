package com.capiot.ckyc.bulkfile.constant;

public final class IdentityDetailsValues {
	
	private IdentityDetailsValues() {
		super();
	}
	
	public static final String RECORD_TYPE = "30";
	public static final String PROOF_SUBMITTED_YES = "01";
	public static final String PROOF_SUBMITTED_NO = "02";
	public static final String VERIFICATION_STATUS_YES = "01";
	public static final String VERIFICATION_STATUS_NO = "02";
}
