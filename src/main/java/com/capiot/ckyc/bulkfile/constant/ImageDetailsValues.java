package com.capiot.ckyc.bulkfile.constant;

public final class ImageDetailsValues {
	
	private ImageDetailsValues() {
		super();
	}
	
	public static final String RECORD_TYPE = "70";
	public static final String GLOBAL = "Global";
	public static final String LOCAL = "local";
	public static final String GLOBAL_VALUE = "01";
	public static final String LOCAL_VALUE = "02";
}
