package com.capiot.ckyc.bulkfile.constant;

import org.springframework.stereotype.Component;

@Component
public class SearchIndividualCKYCValues {
	
	public static final String SEARCH_TYPE = "searchType";
	public static final String CKYC_REGISTRY = "CKYC Registry";
	public static final String EXISTING_ENTITY = "Existing Entity";
	public static final String IDENTITY = "identity";
	public static final String IDENTITY_TYPE = "type";
	public static final String CATEGORY_NAME = "categoryName";
	public static final String NUMBER = "number";
	public static final String ID = "_id";
}
