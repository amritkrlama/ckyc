package com.capiot.ckyc.bulkfile.constant;

public final class RelatedPersonFields {
	
	private RelatedPersonFields() {
		super();
	}
	
	public static final String RELATED_PERSON = "relatedPerson";
	public static final String TYPE_OF_RELATIONSHIP = "typeOfRelationship";
	public static final String TYPE_OF_RELATIONSHIP_CODE = "code";
	public static final String ADDITION_DELETION = "additionDeletion";
	public static final String KYC_VERIFICATION = "kycVerification";
	public static final String APPLICANT_DECLARATION = "applicantDeclaration";
	public static final String KYC_NUMBER = "kycNumber";
	public static final String NAME = "name";
	public static final String MAIDEN_NAME = "maidenName";
	public static final String NAME_PREFIX = "prefix";
	public static final String FIRST_NAME = "firstName";
	public static final String MIDDLE_NAME = "middleName";
	public static final String LAST_NAME = "lastName";
	public static final String FATHER_OR_SPOUSE_FLAG = "fatherOrSpouseFlag";
	public static final String FATHER_OR_SPOUSE_NAME = "fatherOrSpouseName";
	public static final String MOTHER_NAME = "motherName";
	public static final String PAN_CARD = "panCard";
	public static final String UID_AADHAR = "uidAadhar";
	public static final String VOTER_ID_CARD = "voterIdCard";
	public static final String NREGA_JOB_CARD = "nregaJobCard";
	public static final String PASSPORT_NUMBER = "passportNo";
	public static final String PASSPORT_EXPIRY_DATE = "passportExpiryDate";
	public static final String DRIVING_LICENSE_NUMBER = "drivingLicenseNo";
	public static final String DRIVING_LICENSE_EXPIRY_DATE = "drivingLicenceExpiryDate";
	public static final String ID_NAME_OTHER = "idNameOther";
	public static final String ID_NUMBER_OTHER = "idNumberOther";
	public static final String DOCUMENT_TYPE_CODE = "documentTypeCode";
	public static final String DOCUMENT_IDENTIFICATION_NUMBER = "documentIdentificationNumber";
	public static final String DATE_OF_BIRTH = "dateOfBirth";
	public static final String GENDER = "gender";
	public static final String MARITAL_STATUS = "maritalStatus";
	public static final String NATIONALITY = "nationality";
	public static final String COUNTRY_CODE = "countryCode";
	public static final String RESIDENTIAL_STATUS = "residentialStatus";
	public static final String OCCUPATION_TYPE = "occupationType";
	public static final String OCCUPATION_CODE = "code";
	public static final String DATE_OF_DECLARATION = "dateOfDeclaration";
	public static final String PLACE_OF_DECLARATION = "placeOfDeclaration";
	public static final String TYPE_OF_DOCUMENTS = "typeOfDocumentsSubmitted";
	public static final String KYC_VERIFICATION_DATE = "kycVerificationDate";
	public static final String KYC_VERIFICATION_CARRIED_OUT_BY = "kycVerificationCarriedOutBy";
	public static final String KYC_VERIFICATION_EMPLOYEE_NAME = "empName";
	public static final String KYC_VERIFICATION_EMPLOYEE_CODE = "empCode";
	public static final String KYC_VERIFICATION_EMPLOYEE_DESIGNATION = "empDesignation";
	public static final String KYC_VERIFICATION_EMPLOYEE_BRANCH = "empBranch";
	public static final String INSTITUTION_DETAILS = "institutionDetails";
	public static final String FLAG_INDICATING_RELATED_PERSON_RESIDENT_FOR_TAX_PURPOSE = "overseasResidentTaxPurposesFlag";
	public static final String JURISDICTION_OF_RESIDENCE = "jurisdictionOfResidence";
	public static final String JURISDICTION_CITY_OF_BIRTH = "cityOfBirth";
	public static final String JURISDICTION_TAXID = "taxId";
	public static final String COUNTRY_OF_BIRTH = "countryOfBirth";
	public static final String JURISDICTION_COUNTRY = "country";
	public static final String JURISDICTION_COUNTRYCODE = "countryCode";
	public static final String INSTITUTION_DETAILS_CODE = "code";
	public static final String INSTITUTION_DETAILS_NAME = "name";
	public static final String LOCAL_ADDRESS = "localAddress";
	public static final String ADDRESS_COUNTRY = "country";
	public static final String ADDRESS_TYPE = "addressType";
	public static final String ADDRESS_PINCODE = "pinCode";
	public static final String ADDRESS_CITY_TOWN_VILLAGE = "cityTownVillage";
	public static final String ADDRESS_STATE = "state";
	public static final String STATE_CODE = "stateCode";
	public static final String LINE1 = "line1";
	public static final String LINE2 = "line2";
	public static final String LINE3 = "line3";
	public static final String PROOF_OF_ADDRESS = "addressProofType";
	public static final String PROOF_OF_ADDRESS_OTHERS = "addressProofOthers";
}
