package com.capiot.ckyc.bulkfile.constant;

public final class HeaderValues {
	
	private HeaderValues() {
		super();
	}
	
	public static final int RECORD_TYPE = 10;
	public static final String VERSION_NUMBER = "V1.1";

}
