package com.capiot.ckyc.bulkfile.constant;

public final class IdentityDetailsFields {
	
	private IdentityDetailsFields() {
		super();
	}
	
	public static final String RECORD_TYPE = "30";
	public static final String IDENTITY = "identity";
	public static final String IDENTITY_TYPE = "type";
	public static final String TYPE_CATEGORY_CODE = "categoryCode";
	public static final String IDENTITY_NUMBER = "number";
	public static final String EXPIRY_DATE = "expiryDate";
	public static final String PROOF_SUBMITTED = "proofSubmitted";
	public static final String VERIFICATION_STATUS = "verificationStatus";
	public static final String FILLER_ONE = "filler1";
	public static final String FILLER_TWO = "filler2";
	public static final String FILLER_THREE = "filler3";
	public static final String FILLER_FOUR = "filler4";
}
