package com.capiot.ckyc.bulkfile.service;

import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

import org.json.simple.JSONArray;
import org.springframework.stereotype.Component;

import com.capiot.ckyc.bulkfile.constant.HeaderValues;

@Component
public class HeaderService {
	
	private int recordType;
	private String batchNumber;
	private String FIcode;
	private String regionCode;
	private int totalNumberOfRecords;
	private Date createdDate;
	
	public String writeRecordType() {
		recordType = HeaderValues.RECORD_TYPE;
		return String.valueOf(recordType).concat("|");
	}
	
	public String writeBatchNumber(int batchNumber) {
		this.batchNumber= new DecimalFormat("00000").format(batchNumber);
		return this.batchNumber.concat("|");
	}
	
	public String writeFICode(String FIcode) {
		this.FIcode = FIcode;
		return this.FIcode.concat("|");
	}
	
	public String writeRegionCode(String regionCode) {
		this.regionCode = regionCode;
		return this.regionCode.concat("|");
	}
	
	public String writeTotalNumberOfRecord(JSONArray records) {
		totalNumberOfRecords = records.size();
		return String.valueOf(totalNumberOfRecords).concat("|");
	}
	
	public String writeCreatedDate() {
		createdDate = new Date();
		SimpleDateFormat formatter = new SimpleDateFormat("dd-MM-yyyy");
		return formatter.format(createdDate).concat("|");
	}
	
	public String writeVersionNumber() {
		return HeaderValues.VERSION_NUMBER.concat("|");
	}
	
	
	public String writeFiller1() {
		return "|";
	}
	
	
	public String writeFiller2() {
		return "|";
	}
	
	
	public String writeFiller3() {
		return "|";
	}
	
	
	public String writeFiller4() {
		return "|";
	}
	
	public String writeNewLine() {
		return System.getProperty("line.separator");
	}
	
}
