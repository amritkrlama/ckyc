package com.capiot.ckyc.bulkfile.service;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import org.json.simple.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import com.capiot.ckyc.bulkfile.constant.IdentityDetailsFields;
import com.capiot.ckyc.bulkfile.constant.IdentityDetailsValues;
import com.capiot.ckyc.bulkfile.exception.InvalidValueException;


@Component
public class IdentityDetailsService {
	
	private String identificationType;
	private String identityNumber;
	private Date expiryDate;
	private Boolean identityProofSubmitted;
	private Boolean identityVerificationStatus;
	private String filler1;
	private String filler2;
	private String filler3;
	private String filler4;
	
	private final Logger logger = LoggerFactory.getLogger(IdentityDetailsService.class);
	
	
	public String writeIdentityDetailRecordType() {
		return IdentityDetailsValues.RECORD_TYPE.concat("|");
	}
	
	
	public String writeLineNumber(int lineNumber) {
		return String.valueOf(lineNumber).concat("|");
	}
	
	
	
	public String writeIdentificationType(JSONObject record) throws InvalidValueException {
		JSONObject type = (JSONObject) record.get(IdentityDetailsFields.IDENTITY_TYPE);
		if(type!=null) {
			identificationType = (String) type.get(IdentityDetailsFields.TYPE_CATEGORY_CODE);
			if(identificationType!=null) {
				return identificationType.concat("|");
			}else {
				return "|";
			}
		}else {
//			logger.info("Identification Type field is empty");
//			throw new InvalidValueException("Identification Type field is empty");
			return "|";
		}
	}
	
	
	public String writeIdentificationNumber(JSONObject record) throws InvalidValueException {
		identityNumber = (String) record.get(IdentityDetailsFields.IDENTITY_NUMBER);
		if(identityNumber!=null) {
			return identityNumber.concat("|");
		}else {
//			logger.info("Identity Number field is empty");
//			throw new InvalidValueException("Identity Number field is empty");
			return "|";
		}
	}

	
	public String writeExpiryDate(JSONObject record) throws InvalidValueException {
		String expiryDateAsString = (String) record.get(IdentityDetailsFields.EXPIRY_DATE);
		if(expiryDateAsString!=null) {
			try {
				expiryDate = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.sssX").parse(expiryDateAsString);
				SimpleDateFormat formatter = new SimpleDateFormat("dd-MM-yyyy");
				return formatter.format(expiryDate).concat("|");
			}catch(ParseException exception) {
				logger.debug("Error in parsing the Expiry date of Identity detail");
				throw new InvalidValueException("Error in parsing the Expiry date of Identity detail");
			}		
		}else {
			return "|";
		}		
	}
	
	
	public String writeProofSubmitted(JSONObject record) throws InvalidValueException {
		identityProofSubmitted = (Boolean) record.get(IdentityDetailsFields.PROOF_SUBMITTED);
		if(identityProofSubmitted!=null) {
			if(identityProofSubmitted) {
				return IdentityDetailsValues.PROOF_SUBMITTED_YES.concat("|");
			}else {
				return IdentityDetailsValues.PROOF_SUBMITTED_NO.concat("|");
			}
		}else {
//			logger.info("Identity Proof submitted field is empty");
//			throw new InvalidValueException("Identity Proof submitted field is empty");
			return "|";
		}
	}
	
	public String writeVerificationStatus(JSONObject record) throws InvalidValueException {
		identityVerificationStatus = (Boolean) record.get(IdentityDetailsFields.VERIFICATION_STATUS);
		if(identityVerificationStatus!=null) {
			if(identityVerificationStatus) {
				return IdentityDetailsValues.VERIFICATION_STATUS_YES.concat("|");
			}else {
				return IdentityDetailsValues.VERIFICATION_STATUS_NO.concat("|");
			}
		}else {
//			logger.info("Identity Verification Status field is empty");
//			throw new InvalidValueException("Identity Verification Status field is empty");
			return "|";
		}
	}
	
	
	public String writeFiller1(JSONObject record) {
		filler1 = (String) record.get(IdentityDetailsFields.FILLER_ONE);
		if(filler1!=null) {
			return filler1.concat("|");
		}else {
			return "|";
		}
	}
	
	
	public String writeFiller2(JSONObject record) {
		filler2 = (String) record.get(IdentityDetailsFields.FILLER_TWO);
		if(filler2!=null) {
			return filler2.concat("|");
		}else {
			return "|";
		}
	}
	
	
	public String writeFiller3(JSONObject record) {
		filler3 = (String) record.get(IdentityDetailsFields.FILLER_THREE);
		if(filler3!=null) {
			return filler3.concat("|");
		}else {
			return "|";
		}
	}
	
	
	public String writeFiller4(JSONObject record) {
		filler4 = (String) record.get(IdentityDetailsFields.FILLER_FOUR);
		if(filler4!=null) {
			return filler4.concat("|");
		}else {
			return "|";
		}
	}
	
	public String writeNewLine() {
		return "\r\n";
	}
	
}
