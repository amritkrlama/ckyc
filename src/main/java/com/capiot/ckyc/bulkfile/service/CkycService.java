package com.capiot.ckyc.bulkfile.service;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;
import com.capiot.ckyc.bulkfile.constant.IdentityDetailsFields;
import com.capiot.ckyc.bulkfile.constant.ImageDetailsFields;
import com.capiot.ckyc.bulkfile.constant.RelatedPersonFields;
import com.capiot.ckyc.bulkfile.exception.FieldMissingException;
import com.capiot.ckyc.bulkfile.exception.InvalidRequestException;
import com.capiot.ckyc.bulkfile.exception.InvalidValueException;
import com.capiot.ckyc.bulkfile.exception.PSFFileException;
import com.capiot.ckyc.bulkfile.exception.RequestParseException;
import com.capiot.ckyc.bulkfile.odp.DownloadDocumentService;
import com.capiot.ckyc.bulkfile.odp.PostToODPService;
import com.capiot.ckyc.bulkfile.odp.RetrieveRecordService;
import com.capiot.ckyc.bulkfile.support.CreateFileService;
import com.capiot.ckyc.bulkfile.support.MailService;
import com.capiot.ckyc.bulkfile.support.ResponseFileListener;
import com.capiot.ckyc.bulkfile.support.ZipFolderService;
import com.fasterxml.jackson.core.JsonProcessingException;


@RestController
public class CkycService {

	@Autowired
	RetrieveRecordService retrieveRecordService;
	@Autowired
	CreateFileService createFileService;
	@Autowired
	HeaderService headerService;
	@Autowired
	IdentityDetailsService identityDetailsService;
	@Autowired
	RelatedPersonService relatedPersonService;
	@Autowired
	ImageDetailsService imageDetailsService;
	@Autowired
	DownloadDocumentService downloadDocumentService;
	@Autowired
	ZipFolderService zipFolderService;
	@Autowired
	ResponseFileListener responseFileListenerService;
	@Autowired
	DetailRecordService detailRecordService;
	@Autowired
	PostToODPService postToODPService;
	@Autowired
	MailService mailService;
	@Autowired
	Environment environment;
	
	
	public File file = null; // The file
	public String result; //The actual String to write
	public int batchNumber = 1; //For batch number generation
	public int index = 0;  //For counter - indicates the index of record '20'
	private String home=System.getProperty("user.home");
	private static final Logger logger = LoggerFactory.getLogger(CkycService.class);
	JSONParser parser = new JSONParser();
	JSONArray responseAsJSONArray = new JSONArray();


	
	
	
	
	@GetMapping("/bulkFile")
	public String ckycServiceBulkUpload() throws InvalidRequestException, ParseException, RequestParseException, PSFFileException, 
										InvalidValueException, IOException, FieldMissingException, java.text.ParseException{
			
		JSONArray responseRecords = getResponseAsJSONArrayForPendingRecords();
		String commonPath =home+environment.getProperty("commonPath");
		if(responseRecords.size()>0) {
			result="";
			createFoldersAndFiles(responseRecords);
			writeHeader(responseRecords);
			writeDetailRecords(responseRecords);
			writeToFile(result);		
			zipOuterFolderAndFiles();
			createTriggerFile();
			postToODPService.postBulkFileToODP(commonPath,CreateFileService.outerFolderName);
			//mailService.mailBulkFile(commonPath, CreateFileService.outerFolderName);
			postToODPService.updateRecordsToSuccessful(responseRecords);
			postToODPService.postSuccessRecordsToCKYCHub(responseRecords);
		}
		//Incrementing the batch number
		batchNumber++;  
		
		return "Done";
	}


	public JSONArray getResponseAsJSONArrayForPendingRecords() throws JsonProcessingException, InvalidRequestException, ParseException, 
																	RequestParseException {	
		ResponseEntity<?> response = retrieveRecordService.fetchPendingRecords();
		try {
				responseAsJSONArray = (JSONArray) parser.parse(response.getBody().toString());
				if(responseAsJSONArray.isEmpty()) {
					logger.info("No Pending records are found");
					throw new InvalidRequestException("No Pending records are found");
				}else {
					logger.info("Retrieved all the pending records");
				}			
			} catch (ParseException exception) {
				logger.error("Error in parsing Response from RetrieveRecordService");
			}	
		return responseAsJSONArray;
	}
	

	public void createFoldersAndFiles(JSONArray responseAsJSONArray) throws PSFFileException, InvalidRequestException, IOException, ParseException {
		/*
		 * FI Code, Branch Code, User ID, Batch Number
		 */
		createFileService.createOuterFolder("IN0467", "RG", "IA000595", batchNumber);
		file = createFileService.createPSF("IN0467", "RG", "IA000595", batchNumber);
		for(int index=0;index<responseAsJSONArray.size();index++) {
			createFileService.createInnerFolders((JSONObject)responseAsJSONArray.get(index));
			downloadDocumentService.downloadDocument((JSONObject)responseAsJSONArray.get(index));
			zipFolderService.zipFolder(CreateFileService.innerFolderPath, CreateFileService.innerFolderPath);
			logger.info("Inner folder has been zipped successfully");
		}
	}
	
	public void createTriggerFile() throws PSFFileException {
		createFileService.createTriggerFile("IN0467", "RG", "IA000595", batchNumber);
	}
	
	
	public void zipOuterFolderAndFiles() {
		//To compress the outer folder
		zipFolderService.zipFolder(CreateFileService.outerFolderPath, CreateFileService.outerFolderPath);
		logger.info("Outer folder has been zipped successfully");
	}
	

	private void writeHeader(JSONArray responseAsJSONArray) {
		result=result.concat(headerService.writeRecordType());
		result=result.concat(headerService.writeBatchNumber(batchNumber));
		result=result.concat(headerService.writeFICode("IN0467"));
		result=result.concat(headerService.writeRegionCode("RG"));
		result=result.concat(headerService.writeTotalNumberOfRecord(responseAsJSONArray));
		result=result.concat(headerService.writeCreatedDate());
		result=result.concat(headerService.writeVersionNumber());
		result=result.concat(headerService.writeFiller1());
		result=result.concat(headerService.writeFiller2());
		result=result.concat(headerService.writeFiller3());
		result=result.concat(headerService.writeFiller4());
		result=result.concat(headerService.writeNewLine());
	}




	/* Method to iterate over each record in response (from ODP) */
	public void writeDetailRecords(JSONArray responseAsJSONArray) throws InvalidValueException, IOException,
															FieldMissingException, InvalidRequestException, ParseException{

		for (index = 0; index < responseAsJSONArray.size(); index++) {
			JSONObject singleRecord = (JSONObject) responseAsJSONArray.get(index);
			iterateOverEachRecord(singleRecord, index);
		}
	}
	
	
	private void iterateOverEachRecord(JSONObject singleRecord, int index) throws InvalidValueException,
																					InvalidRequestException, ParseException, IOException {
		result=result.concat(detailRecordService.writeRecordType(singleRecord));
		result=result.concat(detailRecordService.writeLineNumber(singleRecord, index+1));
		result=result.concat(detailRecordService.writeApplicationType(singleRecord));
		result=result.concat(detailRecordService.writeBranchCode(singleRecord));
		result=result.concat(detailRecordService.writeApplicantNameUpdateFlag(singleRecord));
		result=result.concat(detailRecordService.writePersonalEntityDetailsUpdateFlag(singleRecord));
		result=result.concat(detailRecordService.writeAddressDetailsUpdateFlag(singleRecord));
		result=result.concat(detailRecordService.writeContactDetailsUpdateFlag(singleRecord));
		result=result.concat(detailRecordService.writeRemarksUpdateFlag(singleRecord));
		result=result.concat(detailRecordService.writeKycVerificationUpdateFlag(singleRecord));
		result=result.concat(detailRecordService.writeIdentityDetailsUpdateFlag(singleRecord));
		result=result.concat(detailRecordService.writeRelatedPersonDetailsFlag(singleRecord));
		result=result.concat(detailRecordService.writeControllingPersonDetailsFlag(singleRecord));
		result=result.concat(detailRecordService.writeImageDetailsUpdateFlag(singleRecord));
		result=result.concat(detailRecordService.writeConstitutionType(singleRecord));
		result=result.concat(detailRecordService.writeAccountHolderTypeFlag(singleRecord));
		result=result.concat(detailRecordService.writeAccountHolderType(singleRecord));
		result=result.concat(detailRecordService.writeAccountType(singleRecord));
		result=result.concat(detailRecordService.writeCkycFIReferenceNumber(singleRecord));
		result=result.concat(detailRecordService.writeApplicantName(singleRecord));
		result=result.concat(detailRecordService.writeNameOfTheApplicantEntity(singleRecord));
		result=result.concat(detailRecordService.writeApplicantMaidenName(singleRecord));
		result=result.concat(detailRecordService.writeApplicantMaidenFullName(singleRecord));
		result=result.concat(detailRecordService.writeFlagIndicatingFatherOrSpouseName(singleRecord));
		result=result.concat(detailRecordService.writeApplicantFatherSpouseName(singleRecord));
		result=result.concat(detailRecordService.writeFatherSpouseFullName(singleRecord));
		result=result.concat(detailRecordService.writeApplicantMotherName(singleRecord));
		result=result.concat(detailRecordService.writeMotherFullName(singleRecord));
		result=result.concat(detailRecordService.writeGender(singleRecord));
		result=result.concat(detailRecordService.writeMaritalStatus(singleRecord));
		result=result.concat(detailRecordService.writeNationality(singleRecord));
		result=result.concat(detailRecordService.writeOccupationType(singleRecord));
		result=result.concat(detailRecordService.writeDateOfBirth(singleRecord));
		result=result.concat(detailRecordService.writePlaceOfInCorporation(singleRecord));
		result=result.concat(detailRecordService.writeDateOfCommencementOfBusiness(singleRecord));
		result=result.concat(detailRecordService.writeCountryOfIncorporation(singleRecord));
		result=result.concat(detailRecordService.writeCountryOfResidenceAsPerTaxLaws(singleRecord));
		result=result.concat(detailRecordService.writeIdentificationType(singleRecord));
		result=result.concat(detailRecordService.writeTIN(singleRecord));
		result=result.concat(detailRecordService.writeTINIssuingCountry(singleRecord));
		result=result.concat(detailRecordService.writePAN(singleRecord));
		result=result.concat(detailRecordService.writeResidentialStatus(singleRecord));
		result=result.concat(detailRecordService.writeFlagIndicatingApplicantResidentForTaxPurposesInJurisdictionOutsideIndia(singleRecord));
		result=result.concat(detailRecordService.writeJurisdictionOfResidence(singleRecord));
		result=result.concat(detailRecordService.writeTaxIdentificationNumber(singleRecord));
		result=result.concat(detailRecordService.writeCountryOfBirth(singleRecord));
		result=result.concat(detailRecordService.writeCityPlaceOfBirth(singleRecord));
		result=result.concat(detailRecordService.writeCurrentPermanentOverseasAddressType(singleRecord));
		result=result.concat(detailRecordService.writeCurrentPermanentOverseasAddress(singleRecord));
		result=result.concat(detailRecordService.writeProofOfAddressSubmittedForCurrentPermanentOverseas(singleRecord));
		result=result.concat(detailRecordService.writeProofOfAddressSubmittedForCurrentPermanentOverseasOthers(singleRecord));
		result=result.concat(detailRecordService.writeFlagIndicatingIfCurrentPermanentAddressIsSameAsCorrespondanceLocalAddress(singleRecord));
		result=result.concat(detailRecordService.writeCorrespondenceAddressType(singleRecord));
		result=result.concat(detailRecordService.writeCorrespondanceAddress(singleRecord));
		result=result.concat(detailRecordService.writePOASubmittedForCorrespondenceAddress(singleRecord));
		result=result.concat(detailRecordService.writeJurisdictionAddressType(singleRecord));
		result=result.concat(detailRecordService.writeAddressInJurisdictionType(singleRecord));
		result=result.concat(detailRecordService.writeJurisdictionAddress(singleRecord));
		result=result.concat(detailRecordService.writePOASubmittedForAddressInJuriscition(singleRecord));
		result=result.concat(detailRecordService.writeContactDetails(singleRecord));
		result=result.concat(detailRecordService.writeRemarks(singleRecord));
		result=result.concat(detailRecordService.writeKYCVerification(singleRecord));
		result=result.concat(detailRecordService.writeNumberOfIdentityDetails(singleRecord));
		result=result.concat(detailRecordService.writeNumberOfRelatedPerson(singleRecord));
		result=result.concat(detailRecordService.writeNumberOfControllingPersonResidentOutsideIndia(singleRecord));
		result=result.concat(detailRecordService.writeNumberOfLocalAddress(singleRecord));
		result=result.concat(detailRecordService.writeNumberOfDocumentDetails(singleRecord));
		result=result.concat(detailRecordService.writeErrorCode(singleRecord));
		result=result.concat(detailRecordService.writeFiller1(singleRecord));
		result=result.concat(detailRecordService.writeFiller2(singleRecord));
		result=result.concat(detailRecordService.writeFiller3(singleRecord));
		result=result.concat(detailRecordService.writeFiller4(singleRecord));
		result=result.concat(detailRecordService.writeNewLine());
		
	
		writeIdentityDetails(singleRecord, index+1);
		writeRelatedPersonDetails(singleRecord,index+1);
		writeImageDetails(singleRecord, index+1);
	}




	/*Method to write Identity details*/
	public void writeIdentityDetails(JSONObject singleRecord, int lineNumber) throws InvalidValueException {
		JSONArray identity = (JSONArray) singleRecord.get(IdentityDetailsFields.IDENTITY);
		if(identity!=null) {
			for(int index=0; index<identity.size();index++) {
				JSONObject singleIdentityRecord = (JSONObject) identity.get(index);
				result=result.concat(identityDetailsService.writeIdentityDetailRecordType());
				result=result.concat(identityDetailsService.writeLineNumber(lineNumber));
				result=result.concat(identityDetailsService.writeIdentificationType(singleIdentityRecord));
				result=result.concat(identityDetailsService.writeIdentificationNumber(singleIdentityRecord));
				result=result.concat(identityDetailsService.writeExpiryDate(singleIdentityRecord));
				result=result.concat(identityDetailsService.writeProofSubmitted(singleIdentityRecord));
				result=result.concat(identityDetailsService.writeVerificationStatus(singleIdentityRecord));
				result=result.concat(identityDetailsService.writeFiller1(singleIdentityRecord));
				result=result.concat(identityDetailsService.writeFiller2(singleIdentityRecord));
				result=result.concat(identityDetailsService.writeFiller3(singleIdentityRecord));
				result=result.concat(identityDetailsService.writeFiller4(singleIdentityRecord));
				result=result.concat(identityDetailsService.writeNewLine());
			}
		}
	}
	
	
	
	
	private void writeRelatedPersonDetails(JSONObject singleRecord, int lineNumber) throws InvalidValueException {
		JSONArray relatedPerson = (JSONArray) singleRecord.get(RelatedPersonFields.RELATED_PERSON);
		if(relatedPerson!=null) {
			for(int index=0; index<relatedPerson.size(); index++) {
				JSONObject singleRelatedPersonDetailRecord = (JSONObject) relatedPerson.get(index);
				result=result.concat(relatedPersonService.writeRecordType(singleRelatedPersonDetailRecord));
				result=result.concat(relatedPersonService.writeLineNumber(lineNumber));
				result=result.concat(relatedPersonService.writeTypeOfRelationship(singleRelatedPersonDetailRecord));
				result=result.concat(relatedPersonService.writeAdditionDeletionOfRelatedPerson(singleRelatedPersonDetailRecord));
				result=result.concat(relatedPersonService.writeKycNumber(singleRelatedPersonDetailRecord));
				result=result.concat(relatedPersonService.writeName(singleRelatedPersonDetailRecord));
				result=result.concat(relatedPersonService.writeRelatedPersonMaidenName(singleRelatedPersonDetailRecord));
				result=result.concat(relatedPersonService.writeFlagIndicationRelatedPersonFatherOrSpouse(singleRelatedPersonDetailRecord));
				result=result.concat(relatedPersonService.writeRelatedPersonFatherOrSpouseName(singleRelatedPersonDetailRecord));
				result=result.concat(relatedPersonService.writeRelatedPersonMotherName(singleRelatedPersonDetailRecord));
				result=result.concat(relatedPersonService.writeRelatedPersonDateOfBirth(singleRelatedPersonDetailRecord));
				result=result.concat(relatedPersonService.writeGender(singleRelatedPersonDetailRecord));
				result=result.concat(relatedPersonService.writeMaritalStatus(singleRelatedPersonDetailRecord));
				result=result.concat(relatedPersonService.writeNationality(singleRelatedPersonDetailRecord));
				result=result.concat(relatedPersonService.writeResidentialStatus(singleRelatedPersonDetailRecord));
				result=result.concat(relatedPersonService.writeOccupationType(singleRelatedPersonDetailRecord));
				result=result.concat(relatedPersonService.writeFlagIndicatingResidentForTax(singleRelatedPersonDetailRecord));
				result=result.concat(relatedPersonService.writeJurisdictionOfResidence(singleRelatedPersonDetailRecord));
				result=result.concat(relatedPersonService.writePANCard(singleRelatedPersonDetailRecord));
				result=result.concat(relatedPersonService.writeUidAadhar(singleRelatedPersonDetailRecord));
				result=result.concat(relatedPersonService.writeVoterIDCard(singleRelatedPersonDetailRecord));
				result=result.concat(relatedPersonService.writeNREGAJobCard(singleRelatedPersonDetailRecord));
				result=result.concat(relatedPersonService.writePassportNumber(singleRelatedPersonDetailRecord));
				result=result.concat(relatedPersonService.writePassportExpiryDate(singleRelatedPersonDetailRecord));
				result=result.concat(relatedPersonService.writeDrivingLicenseNumber(singleRelatedPersonDetailRecord));
				result=result.concat(relatedPersonService.writeDrivingLicenseExpiryDate(singleRelatedPersonDetailRecord));
				result=result.concat(relatedPersonService.writeIDNameOther(singleRelatedPersonDetailRecord));
				result=result.concat(relatedPersonService.writeIDNumberOther(singleRelatedPersonDetailRecord));
				result=result.concat(relatedPersonService.writeDocumentTypeCode(singleRelatedPersonDetailRecord));
				result=result.concat(relatedPersonService.writeDocumentIdentificationNumber(singleRelatedPersonDetailRecord));
				result=result.concat(relatedPersonService.writeAddress(singleRelatedPersonDetailRecord));
				result=result.concat(relatedPersonService.writeKycDetails(singleRelatedPersonDetailRecord));
				result=result.concat(relatedPersonService.writeFiller1(singleRelatedPersonDetailRecord));
				result=result.concat(relatedPersonService.writeFiller2(singleRelatedPersonDetailRecord));
				result=result.concat(relatedPersonService.writeFiller3(singleRelatedPersonDetailRecord));
				result=result.concat(relatedPersonService.writeFiller4(singleRelatedPersonDetailRecord));
				result=result.concat(relatedPersonService.writeNewLine());
				
			}
		}
		
	}
	

	public void writeImageDetails(JSONObject singleRecord, int lineNumber) throws InvalidValueException, InvalidRequestException {
		JSONArray image = (JSONArray) singleRecord.get(ImageDetailsFields.DOCUMENTS);
		if(image!=null) {
			for(int index=0; index<image.size();index++) {
				JSONObject singleImageDetailRecord = (JSONObject) image.get(index);
				result=result.concat(imageDetailsService.writeRecordType());
				result=result.concat(imageDetailsService.writeLineNumber(lineNumber));
				result=result.concat(imageDetailsService.writeFileName(singleImageDetailRecord));
				result=result.concat(imageDetailsService.writeImageType(singleImageDetailRecord));
				result=result.concat(imageDetailsService.writeGlobalOrLocal(singleImageDetailRecord));
				result=result.concat(imageDetailsService.writeBranchCode(singleImageDetailRecord));
				result=result.concat(imageDetailsService.writeFiller1(singleImageDetailRecord));
				result=result.concat(imageDetailsService.writeFiller2(singleImageDetailRecord));
				result=result.concat(imageDetailsService.writeFiller3(singleImageDetailRecord));
				result=result.concat(imageDetailsService.writeNewLine());
			}
		}
	}

	public void writeToFile(String result) {	
		try(BufferedWriter writer = new BufferedWriter(new FileWriter(file, true))){
			writer.write(result);
		}catch(IOException exception) {
			exception.printStackTrace();
		}
	}

}
