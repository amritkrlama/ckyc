package com.capiot.ckyc.bulkfile.service;

import java.io.IOException;
import java.text.DecimalFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.capiot.ckyc.bulkfile.constant.*;
import com.capiot.ckyc.bulkfile.exception.InvalidRequestException;
import com.capiot.ckyc.bulkfile.exception.InvalidValueException;
import com.capiot.ckyc.bulkfile.odp.RetrieveAuditService;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

/**
 * 
 * @author amrit To write Detail Record (20)
 */
@Component
public class DetailRecordService {

	@Autowired
	RetrieveAuditService retrieveAuditService;
	final ObjectMapper mapper = new ObjectMapper();

	private final Logger logger = LoggerFactory.getLogger(DetailRecordService.class);

	private String branchCode;
	private String constitutionType;
	private String accountType;
	private String ckycFIreferenceNumber;
	private String applicantNamePrefix;
	private String applicantFirstName;
	private String applicantMiddleName;
	private String applicantLastName;
	private String applicantMaidenNamePrefix;
	private String applicantMaidenFirstName;
	private String applicantMaidenMiddleName;
	private String applicantMaidenLastName;
	private String applicantFatherSpouseNamePrefix;
	private String applicantFatherSpouseFirstName;
	private String applicantFatherSpouseMiddleName;
	private String applicantFatherSpouseLastName;
	private String motherNamePrefix;
	private String motherFirstName;
	private String motherMiddleName;
	private String motherLastName;
	private String gender;
	private String maritalStatus;
	private String nationality;
	private String occupationType;
	private String dateOfBirth;
	private String residentialStatus;
	private Boolean flagIndicatingApplicantResidentForTaxPurposesInJurisdictionOutsideIndia;
	private String jurisdictionOfResidence;
	private String taxIdentificationNumber;
	private String countryOfBirth;
	private String cityPlaceOfBirth;
	private String currentPermanentOverseasAddressType;
	private String currentPermanentOverseasAddressLine1;
	private String currentPermanentOverseasAddressLine2;
	private String currentPermanentOverseasAddressLine3;
	private String currentPermanentOverseasCityTownVillage;
	private String currentPermanentOverseasDistrict;
	private String currentPermanentOverseasState;
	private String currentPermanentOverseasCountry;
	private String currentPermanentOverseasPincode;
	private String proofOfAddressSubmittedForCurrentPermanentOverseas;
	private String proofOfAddressSubmittedForCurrentPermanentOverseasOthers;
	private Boolean flagIndicatingIfCurrentPermanentAddressIsSameAsCorrespondanceLocalAddress;
	private String correspondenceLocalAddressLine1;
	private String correspondenceLocalAddressLine2;
	private String correspondenceLocalAddressLine3;
	private String correspondenceLocalCityTownVillage;
	private String correspondenceLocalDistrict;
	private String correspondenceLocalPINCODE;
	private String jurisdictionAddressType;
	private String jurisdictionAddressLine1;
	private String jurisdictionAddressLine2;
	private String jurisdictionAddressLine3;
	private String jurisdictionAddressCityTownVillage;
	private String jurisdictionStateCode;
	private String jurisdictionCountryCode;
	private String jurisdictionPINCODE;
	private String residenceStdCode;
	private String residenceNumber;
	private String officeStdCode;
	private String officeNumber;
	private String mobileIsdCode;
	private String mobileNumber;
	private String faxStdCode;
	private String faxNumber;
	private String email;
	private String remarks;
	private String dateOfDeclaration;
	private String placeOfDeclaration;
	private String typeOfDocumentsSubmitted;
	private String dateWhenKYCVerificationWasCarriedOut;
	private String employeeName;
	private String employeeCode;
	private String employeeDesignation;
	private String employeeBranch;
	private String institutionDetailName;
	private String institutionDetailCode;

//	private JSONArray auditsBetweenTheInterval;

	public DetailRecordService() {
		super();
	}

	/**
	 * 
	 * @param Single record of a person in ODP(DCH)
	 * @return Record Type (20)
	 * @throws InvalidValueException
	 */
	public String writeRecordType(JSONObject record) throws InvalidValueException {
		return DetailRecordValues.RECORD_TYPE.concat("|");
	}

	/**
	 * 
	 * @param Single  record of a person in ODP(DCH)
	 * @param counter for incrementing the Line Number
	 * @return Line number
	 * @throws InvalidValueException
	 */
	public String writeLineNumber(JSONObject record, int counter) throws InvalidValueException {
		String result = "";
		result = result.concat(String.valueOf(counter));
		result = result.concat("|");
		return result;
	}

	/**
	 * 
	 * @param applicationType
	 * @return Application Type
	 * @throws InvalidValueException
	 */
	public String writeApplicationType(JSONObject record) throws InvalidValueException {
		String applicationType = (String) record.get(DetailRecordFields.APPLICATION_TYPE);
		String result = "";
		if (applicationType != null) {
			if (applicationType.equalsIgnoreCase(DetailRecordValues.APPLICATION_TYPE_NEW)) {
				result = result.concat(DetailRecordValues.APPLICATION_TYPE_NEW_VALUE).concat("|");
			} else if (applicationType.equalsIgnoreCase(DetailRecordValues.APPLICATION_TYPE_UPDATE)) {
				result = result.concat(DetailRecordValues.APPLICATION_TYPE_UPDATE_VALUE).concat("|");
			} else if (applicationType.equalsIgnoreCase(DetailRecordValues.APPLICATION_TYPE_DOWNLOAD)) {
				result = result.concat(DetailRecordValues.APPLICATION_TYPE_DOWNLOAD_VALUE).concat("|");
			}
		}
		return result;
	}

	/**
	 * 
	 * @param Single record of a person in ODP(DCH)
	 * @return Branch Code
	 * @throws InvalidValueException
	 */
	public String writeBranchCode(JSONObject record) throws InvalidValueException {

		branchCode = (String) record.get(DetailRecordFields.BRANCH_CODE);
		String result = "";
		if (branchCode == null) {
//			logger.info("Branch Code field is empty");
//			throw new InvalidValueException("Branch Code field is empty");
			logger.warn("Branch code is missing in Detail record");
			result = result.concat("|");
			return result;
		} else {
			branchCode = branchCode.trim();
			result = result.concat(branchCode);
			result = result.concat("|");
			return result;
		}
	}

	/**
	 * 
	 * @param Single record of a person in ODP(DCH)
	 * @return Constitution Type
	 * @throws InvalidValueException
	 */
	public String writeConstitutionType(JSONObject record) throws InvalidValueException {

		JSONObject accountDetails = (JSONObject) record.get(DetailRecordFields.ACCOUNT_DETAILS);
		String result = "";
		if (accountDetails != null) {
			constitutionType = (String) accountDetails.get(DetailRecordFields.CONSTITUTION_TYPE);
//			if (constitutionType == null) {
//				logger.info("Constitution Type Field is empty");
//				throw new InvalidValueException("Constitution Type Field is empty");
//			}
			if (constitutionType != null) {
				if (constitutionType.equalsIgnoreCase(DetailRecordFields.CONSTITUTION_TYPE_INDIVIDUAL)) {
					result = result.concat(DetailRecordValues.CONSTITUTION_TYPE_INDIVIDUAL);
					result = result.concat("|");
					return result;
				} else if (constitutionType.equalsIgnoreCase(DetailRecordFields.CONSTITUTION_TYPE_LEGAL)) {
					result = result.concat(DetailRecordValues.CONSTITUTION_TYPE_LEGAL);
					result = result.concat("|");
					return result;
				}
			} else {
				logger.warn("Constitution type is missing in Account details under Detail record");
				result = result.concat("|");
				return result;
			}
		} else {
			logger.warn("Account detail is missing in Detail record");
			result = result.concat("||||");
		}

		return result;
	}

	/**
	 * 
	 * @param Single record of a person in ODP(DCH)
	 * @return Account Type
	 * @throws InvalidValueException
	 */
	public String writeAccountType(JSONObject record) throws InvalidValueException {

		JSONObject accountDetails = (JSONObject) record.get(DetailRecordFields.ACCOUNT_DETAILS);
		if (accountDetails != null) {
			accountType = (String) accountDetails.get(DetailRecordFields.ACCOUNT_TYPE);
			String result = "";
//			if (accountType == null) {
//				logger.info("Account Type Field is empty");
//				throw new InvalidValueException("Account Type Field is empty");
//			} 
			if (accountType != null) {
				if (accountType.equalsIgnoreCase(DetailRecordValues.ACCOUNT_TYPE_NORMAL_VALUE)) {
					result = result.concat(DetailRecordValues.ACCOUNT_TYPE_NORMAL);
					result = result.concat("|");
					return result;
				} else if (accountType.equalsIgnoreCase(DetailRecordValues.ACCOUNT_TYPE_SMALL_VALUE)) {
					result = result.concat(DetailRecordValues.ACCOUNT_TYPE_SMALL);
					result = result.concat("|");
					return result;
				} else if (accountType.equalsIgnoreCase(DetailRecordValues.ACCOUNT_TYPE_SIMPLIFIED_VALUE)) {
					result = result.concat(DetailRecordValues.ACCOUNT_TYPE_SIMPLIFIED);
					result = result.concat("|");
					return result;
				} else if (accountType.equalsIgnoreCase(DetailRecordValues.ACCOUNT_TYPE_OTP_BASED_VALUE)) {
					result = result.concat(DetailRecordValues.ACCOUNT_TYPE_OTP_BASED);
					result = result.concat("|");
					return result;
				} else {
					logger.info("Invalid value in Account Type Field");
					throw new InvalidValueException("Invalid value in Account Type Field");
				}
			} else {
				logger.warn("Account type is missing in Detail record");
				result = result.concat("|");
				return result;
			}
		} else {
			return "";
		}
	}

	/**
	 * 
	 * @param Single record of a person in ODP(DCH)
	 * @return CKYC/FI Reference Number
	 * @throws InvalidValueException
	 */
	public String writeCkycFIReferenceNumber(JSONObject record) throws InvalidValueException {

		ckycFIreferenceNumber = (String) record.get(DetailRecordFields.CKYC_FI_REFERENCE_NUMBER);
		String result = "";
		if (ckycFIreferenceNumber == null) {
			logger.info("FI reference number is empty");
			throw new InvalidValueException("FI reference number is empty");
		} else {
			result = result.concat(ckycFIreferenceNumber);
			result = result.concat("|");
			return result;
		}
	}

	/**
	 * 
	 * @param Single record of a person in ODP(DCH)
	 * @return Applicant Name
	 * @throws InvalidValueException
	 */
	public String writeApplicantName(JSONObject record) throws InvalidValueException {

		JSONObject personalEntityDetails = (JSONObject) record.get(DetailRecordFields.PERSONAL_DETAILS);
		String result = "";

		if (personalEntityDetails != null) {
			JSONObject applicantNameAsObject = (JSONObject) personalEntityDetails
					.get(DetailRecordFields.APPLICANT_NAME);
			if (applicantNameAsObject != null) {
				applicantNamePrefix = (String) applicantNameAsObject.get(DetailRecordFields.APPLICANT_NAME_PREFIX);
				applicantFirstName = (String) applicantNameAsObject.get(DetailRecordFields.APPLICANT_NAME_FIRSTNAME);
				applicantMiddleName = (String) applicantNameAsObject.get(DetailRecordFields.APPLICANT_NAME_MIDDLENAME);
				applicantLastName = (String) applicantNameAsObject.get(DetailRecordFields.APPLICANT_NAME_LASTNAME);

				String regex = "^[a-zA-Z']+$";
				Pattern pattern = Pattern.compile(regex);
				Matcher matcher = null;

				// Prefix
				if (applicantNamePrefix != null) {
					applicantNamePrefix = applicantNamePrefix.trim();
					result = result.concat(applicantNamePrefix);
					result = result.concat("|");
				} else {
//					logger.info("Applicant Name Prefix field is empty");
//					throw new InvalidValueException("Applicant Name Prefix field is empty");
					logger.warn("Prefix is missing in Applicant Name under Detail record");
					result = result.concat("|");
				}

				// First name
				if (applicantFirstName != null) {
					applicantFirstName = applicantFirstName.trim();
					matcher = pattern.matcher(applicantFirstName);
					if (matcher.matches()) {
						result = result.concat(applicantFirstName);
						result = result.concat("|");
					} else {
						logger.info("Invalid value in First Name field");
//						throw new InvalidValueException("Invalid value in First Name field");
					}
				} else {
					logger.warn("First Name field is empty");
//					throw new InvalidValueException("First Name field is empty");
					result = result.concat("|");
				}

				// Middle name
				if (applicantMiddleName != null) {
					applicantMiddleName = applicantMiddleName.trim();
					matcher = pattern.matcher(applicantMiddleName);
					if (matcher.matches()) {
						result = result.concat(applicantMiddleName);
						result = result.concat("|");
					} else {
						logger.info("Invalid value in Middle Name field");
//						throw new InvalidValueException("Invalid value in Middle Name field");
					}
				} else {
					result = result.concat("|");
				}

				// Last name
				if (applicantLastName != null) {
					applicantLastName = applicantLastName.trim();
					matcher = pattern.matcher(applicantLastName);
					if (matcher.matches()) {
						result = result.concat(applicantLastName);
						result = result.concat("|");
					} else {
						logger.info("Invalid value in Last Name field");
//						throw new InvalidValueException("Invalid value in Last Name field");
					}
				} else {
					result = result.concat("|");
				}
			} else {
				logger.warn("Applicant name is missing under Detail record");
				return "|".concat("|").concat("|").concat("|");
			}
		}

		return result;
	}

	/**
	 * 
	 * @param Single record of a person in ODP(DCH)
	 * @return Name of the Applicant/Entity
	 */
	public String writeNameOfTheApplicantEntity(JSONObject record) {
		JSONObject personalEntityDetails = (JSONObject) record.get(DetailRecordFields.PERSONAL_DETAILS);
		String result = "";
		if (personalEntityDetails != null) {
			String nameOfTheApplicantEntity = (String) personalEntityDetails
					.get(DetailRecordFields.NAME_OF_THE_APPLICANT_ENTITY);
			if (nameOfTheApplicantEntity != null) {
				result = result.concat(nameOfTheApplicantEntity).concat("|");
			} else {
				result = result.concat("|");
			}
		}
		return result;
	}

	/**
	 * 
	 * @param Single record of a person in ODP(DCH)
	 * @return Applicant Maiden Name
	 * @throws InvalidValueException
	 */
	public String writeApplicantMaidenName(JSONObject record) throws InvalidValueException {
		JSONObject personalEntityDetails = (JSONObject) record.get(DetailRecordFields.PERSONAL_DETAILS);
		String result = "";

		if (personalEntityDetails != null) {
			JSONObject applicantMaidenNameAsObject = (JSONObject) personalEntityDetails
					.get(DetailRecordFields.APPLICANT_MAIDEN_NAME);
			if (applicantMaidenNameAsObject != null) {
				applicantMaidenNamePrefix = (String) applicantMaidenNameAsObject
						.get(DetailRecordFields.APPLICANT_MAIDEN_NAME_PREFIX);
				applicantMaidenFirstName = (String) applicantMaidenNameAsObject
						.get(DetailRecordFields.APPLICANT_MAIDEN_NAME_FIRSTNAME);
				applicantMaidenMiddleName = (String) applicantMaidenNameAsObject
						.get(DetailRecordFields.APPLICANT_MAIDEN_NAME_MIDDLENAME);
				applicantMaidenLastName = (String) applicantMaidenNameAsObject
						.get(DetailRecordFields.APPLICANT_MAIDEN_NAME_LASTNAME);

				Matcher matcher = null;

				String regex = "^[a-zA-Z']+$";
				Pattern pattern = Pattern.compile(regex);

				// Prefix
				if (applicantMaidenNamePrefix != null) {
					applicantMaidenNamePrefix = applicantMaidenNamePrefix.trim();
					result = result.concat(applicantMaidenNamePrefix);
					result = result.concat("|");
				} else {
					result = result.concat("|");
				}

				// First Name
				if (applicantMaidenFirstName != null) {
					applicantMaidenFirstName = applicantMaidenFirstName.trim();
					matcher = pattern.matcher(applicantMaidenFirstName);
					if (matcher.matches()) {
						result = result.concat(applicantMaidenFirstName);
						result = result.concat("|");
					} else {
						logger.info("Invalid value in Maiden First Name");
						throw new InvalidValueException("Invalid value in Maiden First Name");
					}
				} else {
					result = result.concat("|");
				}

				// Middle Name
				if (applicantMaidenMiddleName != null) {
					applicantMaidenMiddleName = applicantMaidenMiddleName.trim();
					matcher = pattern.matcher(applicantMaidenMiddleName);
					if (matcher.matches()) {
						result = result.concat(applicantMaidenMiddleName);
						result = result.concat("|");
					} else {
						logger.info("Invalid value in Maiden Middle name field");
						throw new InvalidValueException("Invalid value in Maiden Middle Name field");
					}
				} else {
					result = result.concat("|");
				}

				// Last Name
				if (applicantMaidenLastName != null) {
					applicantMaidenLastName = applicantMaidenLastName.trim();
					matcher = pattern.matcher(applicantMaidenLastName);
					if (matcher.matches()) {
						result = result.concat(applicantMaidenLastName);
						result = result.concat("|");
					} else {
						logger.info("Invalid value in Maiden Last name field");
						throw new InvalidValueException("Invalid value in Maiden Last Name field");
					}
				} else {
					result = result.concat("|");
				}
			} else {
				result = result.concat("|").concat("|").concat("|").concat("|");
			}
		}

		return result;
	}

	/**
	 * 
	 * @param Single record of a person in ODP(DCH)
	 * @return Applicant Maiden Full Name
	 */
	public String writeApplicantMaidenFullName(JSONObject record) {
		JSONObject personalEntityDetails = (JSONObject) record.get(DetailRecordFields.PERSONAL_DETAILS);
		String result = "";
		if (personalEntityDetails != null) {
			String maidenFullName = (String) personalEntityDetails.get(DetailRecordFields.APPLICANT_MAIDEN_FULLNAME);
			if (maidenFullName != null) {
				maidenFullName = maidenFullName.trim();
				result = result.concat(maidenFullName).concat("|");
			} else {
				result = result.concat("|");
			}
		}
		return result;
	}

	/**
	 * 
	 * @param Single record of a person in ODP(DCH)
	 * @return Flat Indication Father or Spouse
	 */
	public String writeFlagIndicatingFatherOrSpouseName(JSONObject record) {
		String result = "";
		JSONObject personalEntityDetails = (JSONObject) record.get(DetailRecordFields.PERSONAL_DETAILS);
		if (personalEntityDetails != null) {
			String flagIndicatingFatherSpouseName = (String) personalEntityDetails
					.get(DetailRecordFields.FLAG_INDICATING_FATHER_OR_SPOUSE_NAME);
			if (flagIndicatingFatherSpouseName != null) {
				if (flagIndicatingFatherSpouseName.equalsIgnoreCase("Father")) {
					result = result.concat(DetailRecordValues.FLAG_FATHER);
					result = result.concat("|");
					return result;
				} else {
					result = result.concat(DetailRecordValues.FLAG_SPOUSE);
					result = result.concat("|");
				}
			} else {
				logger.warn("Flag indicating father or spouse is missing under detail record");
				return "|";
			}
		}
		return result;
	}

	/**
	 * 
	 * @param Single record of a person in ODP(DCH)
	 * @return Applicant Father/Spouse Name
	 * @throws InvalidValueException
	 */
	public String writeApplicantFatherSpouseName(JSONObject record) throws InvalidValueException {

		JSONObject personalEntityDetails = (JSONObject) record.get(DetailRecordFields.PERSONAL_DETAILS);
		String result = "";

		if (personalEntityDetails != null) {
			JSONObject applicantFatherSpouseNameAsObject = (JSONObject) personalEntityDetails
					.get(DetailRecordFields.APPLICANT_FATHER_OR_SPOUSE_NAME);
			if (applicantFatherSpouseNameAsObject != null) {
				applicantFatherSpouseNamePrefix = (String) applicantFatherSpouseNameAsObject
						.get(DetailRecordFields.APPLICANT_FATHER_OR_SPOUSE_NAME_PREFIX);
				applicantFatherSpouseFirstName = (String) applicantFatherSpouseNameAsObject
						.get(DetailRecordFields.APPLICANT_FATHER_OR_SPOUSE_NAME__FIRSTNAME);
				applicantFatherSpouseMiddleName = (String) applicantFatherSpouseNameAsObject
						.get(DetailRecordFields.APPLICANT_FATHER_OR_SPOUSE_NAME_MIDDLENAME);
				applicantFatherSpouseLastName = (String) applicantFatherSpouseNameAsObject
						.get(DetailRecordFields.APPLICANT_FATHER_OR_SPOUSE_NAME_LASTNAME);

				String regex = "^[a-zA-Z']+$";
				Pattern pattern = Pattern.compile(regex);
				Matcher matcher = null;

				// Prefix
				if (applicantFatherSpouseNamePrefix != null) {
					applicantFatherSpouseNamePrefix = applicantFatherSpouseNamePrefix.trim();
					result = result.concat(applicantFatherSpouseNamePrefix);
					result = result.concat("|");
				} else {
					logger.warn("Applicant Father/Spouse name Prefix field is empty");
//					throw new InvalidValueException("Applicant Father/Spouse prefix field is empty");
					result = result.concat("|");
				}

				// First Name
				if (applicantFatherSpouseFirstName != null) {
					applicantFatherSpouseFirstName = applicantFatherSpouseFirstName.trim();
					matcher = pattern.matcher(applicantFatherSpouseFirstName);
					if (matcher.matches()) {
						result = result.concat(applicantFatherSpouseFirstName);
						result = result.concat("|");
					} else {
						logger.info("Invalid value in Father/Spouse First Name field");
//						throw new InvalidValueException("Invalid value in Father/Spouse First Name field");
					}
				} else {
					logger.warn("Applicant Father/Spouse First name field is empty");
//					throw new InvalidValueException("Applicant Father/Spouse First name field is empty");
					result = result.concat("|");
				}

				// Middle Name
				if (applicantFatherSpouseMiddleName != null) {
					applicantFatherSpouseMiddleName = applicantFatherSpouseMiddleName.trim();
					matcher = pattern.matcher(applicantFatherSpouseMiddleName);
					if (matcher.matches()) {
						result = result.concat(applicantFatherSpouseMiddleName);
						result = result.concat("|");
					} else {
						logger.info("Invalid value in Father/Spouse Middle name field");
//						throw new InvalidValueException("Invalid value in Father/Spouse Middle Name field");
					}
				} else {
					result = result.concat("|");
				}

				// Last name
				if (applicantFatherSpouseLastName != null) {
					applicantFatherSpouseLastName = applicantFatherSpouseLastName.trim();
					matcher = pattern.matcher(applicantFatherSpouseLastName);
					if (matcher.matches()) {
						result = result.concat(applicantFatherSpouseLastName);
						result = result.concat("|");
					} else {
						logger.info("Invalid value in Applicant Father/Spouse Last name field");
//						throw new InvalidValueException("Invalid value in Father/Spouse Last Name field");
					}
				} else {
					result = result.concat("|");
				}
			} else {
				logger.warn("Applicant father/spouse name is missing in detail record");
				return "|".concat("|").concat("|").concat("|");
			}
		}

		return result;
	}

	/**
	 * 
	 * @param Single record of a person in ODP(DCH)
	 * @return Father/Spouse full name
	 */
	public String writeFatherSpouseFullName(JSONObject record) {
		JSONObject personalEntityDetails = (JSONObject) record.get(DetailRecordFields.PERSONAL_DETAILS);
		if (personalEntityDetails != null) {
			String fatherSpouseFullName = (String) personalEntityDetails
					.get(DetailRecordFields.APPLICANT_FATHER_OR_SPOUSE_FULL_NAME);
			if (fatherSpouseFullName != null) {
				fatherSpouseFullName = fatherSpouseFullName.trim();
				return fatherSpouseFullName.concat("|");
			} else {
				return "|";
			}
		}
		return "";
	}

	/**
	 * 
	 * @param Single record of a person in ODP(DCH)
	 * @return Applicant Mother Name
	 * @throws InvalidValueException
	 */
	public String writeApplicantMotherName(JSONObject record) throws InvalidValueException {

		JSONObject personalEntityDetails = (JSONObject) record.get(DetailRecordFields.PERSONAL_DETAILS);
		String result = "";

		if (personalEntityDetails != null) {
			JSONObject applicantMotherNameAsObject = (JSONObject) personalEntityDetails
					.get(DetailRecordFields.APPLICANT_MOTHER_NAME);
			if (applicantMotherNameAsObject != null) {
				motherNamePrefix = (String) applicantMotherNameAsObject
						.get(DetailRecordFields.APPLICANT_MOTHER_NAME_PREFIX);
				motherFirstName = (String) applicantMotherNameAsObject
						.get(DetailRecordFields.APPLICANT_MOTHER_NAME_FIRSTNAME);
				motherMiddleName = (String) applicantMotherNameAsObject
						.get(DetailRecordFields.APPLICANT_MOTHER_NAME_MIDDLENAME);
				motherLastName = (String) applicantMotherNameAsObject
						.get(DetailRecordFields.APPLICANT_MOTHER_NAME_LASTNAME);

				String regex = "^[a-zA-Z']+$";
				Pattern pattern = Pattern.compile(regex);
				Matcher matcher = null;

				// Prefix
				if (motherNamePrefix != null) {
					motherNamePrefix = motherNamePrefix.trim();
					result = result.concat(motherNamePrefix);
					result = result.concat("|");
				} else {
					result = result.concat("|");
				}

				// First Name
				if (motherFirstName != null) {
					motherFirstName = motherFirstName.trim();
					matcher = pattern.matcher(motherFirstName);
					if (matcher.matches()) {
						result = result.concat(motherFirstName);
						result = result.concat("|");
					} else {
						logger.warn("Invalid value in Applicant Mother first name field");
						result = result.concat("|");
					}
				} else {
					logger.warn("Applicant Mother first name field is empty");
//					throw new InvalidValueException("Applicant Mother first name field is empty");
					result = result.concat("|");
				}

				// Middle Name
				if (motherMiddleName != null) {
					motherMiddleName = motherMiddleName.trim();
					matcher = pattern.matcher(motherMiddleName);
					if (matcher.matches()) {
						result = result.concat(motherMiddleName);
						result = result.concat("|");
					} else {
						logger.warn("Invalid value in Applicant Mother middle name field");
						result = result.concat("|");
					}
				} else {
					result = result.concat("|");
				}

				// Last name
				if (motherLastName != null) {
					motherLastName = motherLastName.trim();
					matcher = pattern.matcher(motherLastName);
					if (matcher.matches()) {
						result = result.concat(motherLastName);
						result = result.concat("|");
					} else {
						logger.warn("Invalid value in Applicant Mother last name field");
						result = result.concat("|");
					}
				} else {
					result = result.concat("|");
				}
			} else {
				logger.warn("Applicant Mother name is missing in Detail record");
				return "|".concat("|").concat("|").concat("|");
			}
		}

		return result;
	}

	/**
	 * 
	 * @param Single record of a person in ODP(DCH)
	 * @return Applicant Mother Full Name
	 */
	public String writeMotherFullName(JSONObject record) {
		JSONObject personalEntityDetails = (JSONObject) record.get(DetailRecordFields.PERSONAL_DETAILS);
		if (personalEntityDetails != null) {
			String motherFullName = (String) personalEntityDetails.get(DetailRecordFields.APPLICANT_MOTHER_FULLNAME);
			if (motherFullName != null) {
				motherFullName = motherFullName.trim();
				return motherFullName.concat("|");
			} else {
				return "|";
			}
		}
		return "";
	}

	/**
	 * 
	 * @param Single record of a person in ODP(DCH)
	 * @return Applicant Gender
	 * @throws InvalidValueException
	 */
	public String writeGender(JSONObject record) throws InvalidValueException {
		JSONObject personalEntityDetails = (JSONObject) record.get(DetailRecordFields.PERSONAL_DETAILS);
		if (personalEntityDetails != null) {
			gender = (String) personalEntityDetails.get(DetailRecordFields.GENDER);
			String result = "";

			if (gender != null) {
				if (gender.equalsIgnoreCase("Male")) {
					result = result.concat(DetailRecordValues.GENDER_MALE);
					result = result.concat("|");
					return result;
				} else if (gender.equalsIgnoreCase("Female")) {
					result = result.concat(DetailRecordValues.GENDER_FEMALE);
					result = result.concat("|");
					return result;
				} else if (gender.equalsIgnoreCase("Transgender")) {
					result = result.concat(DetailRecordValues.GENDER_TRANSGENDER);
					result = result.concat("|");
					return result;
				} else {
					logger.warn("Invalid value in Gender field");
//					throw new InvalidValueException("Invalid value in Gender field");
					result = result.concat("|");
				}
			} else {
				return "|";
			}
		}

		return "";
	}

	/**
	 * 
	 * @param Single record of a person in ODP(DCH)
	 * @return Applicant Marital Status
	 * @throws InvalidValueException
	 */
	public String writeMaritalStatus(JSONObject record) throws InvalidValueException {

		JSONObject personalEntityDetails = (JSONObject) record.get(DetailRecordFields.PERSONAL_DETAILS);
		String result = "";
		if (personalEntityDetails != null) {
			maritalStatus = (String) personalEntityDetails.get(DetailRecordFields.MARITAL_STATUS);
			if (maritalStatus != null) {
				if (maritalStatus.equalsIgnoreCase("Married")) {
					result = result.concat(DetailRecordValues.MARITAL_STATUS_MARRIED);
					result = result.concat("|");
				} else if (maritalStatus.equalsIgnoreCase("Unmarried")) {
					result = result.concat(DetailRecordValues.MARITAL_STATUS_UNMARRIED);
					result = result.concat("|");
				} else if (maritalStatus.equalsIgnoreCase("Others")) {
					result = result.concat(DetailRecordValues.MARITAL_STATUS_OTHERS);
					result = result.concat("|");
				} else {
					logger.warn("Invalid value in Marital Status");
//					throw new InvalidValueException("Invalid value in Marital Status");
					result = result.concat("|");
				}
			} else {
				return "|";
			}
		}
		return result;
	}

	/**
	 * 
	 * @param Single record of a person in ODP(DCH)
	 * @return Applicant Nationality
	 * @throws InvalidValueException
	 */
	public String writeNationality(JSONObject record) throws InvalidValueException {
		JSONObject personalEntityDetails = (JSONObject) record.get(DetailRecordFields.PERSONAL_DETAILS);
		String result = "";

		if (personalEntityDetails != null) {
			JSONObject nationalityAsObject = (JSONObject) personalEntityDetails.get(DetailRecordFields.NATIONALITY);
			if (nationalityAsObject != null) {
				nationality = (String) nationalityAsObject.get(DetailRecordFields.NATIONALITY_CODE);
				if (nationality != null) {
					result = result.concat(nationality);
					result = result.concat("|");
				} else {
					logger.warn("Nationality is not provided");
//					throw new InvalidValueException("Nationality is not provided");
					result = result.concat("|");
				}
			} else {
				return "|";
			}
		}
		return result;
	}

	/**
	 * 
	 * @param Single record of a person in ODP(DCH)
	 * @return Applicant Occupation Type
	 * @throws InvalidValueException
	 */
	public String writeOccupationType(JSONObject record) throws InvalidValueException {
		JSONObject personalEntityDetails = (JSONObject) record.get(DetailRecordFields.PERSONAL_DETAILS);
		String result = "";

		if (personalEntityDetails != null) {
			JSONObject occupationTypeAsObject = (JSONObject) personalEntityDetails
					.get(DetailRecordFields.OCCUPATION_TYPE);
			if (occupationTypeAsObject != null) {
				occupationType = (String) occupationTypeAsObject.get(DetailRecordFields.OCCUPATION_TYPE_CODE);
				if (occupationType != null) {
					result = result.concat(occupationType);
					result = result.concat("|");
				} else {
					logger.warn("Occupation field not provided");
//					throw new InvalidValueException("Occupation field not provided");
					result = result.concat("|");
				}
			} else {
				return "|";
			}
		}
		return result;
	}

	/**
	 * 
	 * @param Single record of a person in ODP(DCH)
	 * @return Applicant Date Of Birth
	 * @throws InvalidValueException
	 */
	public String writeDateOfBirth(JSONObject record) throws InvalidValueException {
		JSONObject personalEntityDetails = (JSONObject) record.get(DetailRecordFields.PERSONAL_DETAILS);
		String result = "";

		if (personalEntityDetails != null) {
			dateOfBirth = (String) personalEntityDetails.get(DetailRecordFields.DATE_OF_BIRTH);
			if (dateOfBirth != null) {
				Date dob = null;
				try {
					dob = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.sssX").parse(dateOfBirth);
					SimpleDateFormat formatter = new SimpleDateFormat("dd-MM-yyyy");
					String strDob = formatter.format(dob);
					result = result.concat(strDob);
					result = result.concat("|");
				} catch (ParseException exception) {
					logger.debug("Invalid value in Date of Birth Field");
					throw new InvalidValueException("Invalid value in Date of Birth Field");
				}
			} else {
				logger.warn("Date of Birth/Date of Incorporation field is empty");
//				throw new InvalidValueException("Date of Birth/Date of Incorporation field is empty");
				result = result.concat("|");
			}
		}
		return result;
	}

	/**
	 * 
	 * @param Single record of a person in ODP(DCH)
	 * @return Applicant Residential Status
	 * @throws InvalidValueException
	 */
	public String writeResidentialStatus(JSONObject record) throws InvalidValueException {
		JSONObject personalEntityDetails = (JSONObject) record.get(DetailRecordFields.PERSONAL_DETAILS);
		String result = "";
		if (personalEntityDetails != null) {
			residentialStatus = (String) personalEntityDetails.get(DetailRecordFields.RESIDENTIAL_STATUS);
			if (residentialStatus != null) {
				if (residentialStatus
						.equalsIgnoreCase(DetailRecordValues.RESIDENTIAL_STATUS_VALUE_RESIDENTIAL_INDIVIDUAL)) {
					result = result.concat(DetailRecordValues.RESIDENTIAL_STATUS_INDIVIDUAL);
					result = result.concat("|");
					return result;
				} else if (residentialStatus.equalsIgnoreCase(DetailRecordValues.RESIDENTIAL_STATUS_VALUE_NRI)) {
					result = result.concat(DetailRecordValues.RESIDENTIAL_STATUS_NRI);
					result = result.concat("|");
					return result;
				} else if (residentialStatus
						.equalsIgnoreCase(DetailRecordValues.RESIDENTIAL_STATUS_VALUE_FOREIGN_NATIONAL)) {
					result = result.concat(DetailRecordValues.RESIDENTIAL_STATUS_FOREIGN_NATIONAL);
					result = result.concat("|");
					return result;
				} else if (residentialStatus
						.equalsIgnoreCase(DetailRecordValues.RESIDENTIAL_STATUS_VALUE_PERSON_OF_INDIAN_ORIGIN)) {
					result = result.concat(DetailRecordValues.RESIDENTIAL_STATUS_PERSON_OF_INDIAN_ORIGIN);
					result = result.concat("|");
					return result;
				} else {
					logger.info("Invalid value in Residential Status field");
					throw new InvalidValueException("Invalid value in Residential Status field");
				}
			} else {
				logger.warn("Residential Status field is empty");
//				throw new InvalidValueException("Residential Status field is empty");
				result = result.concat("|");
				return result;
			}
		}
		return result;
	}

	/*
	 * Method to write Flag Indicating Applicant Resident for tax purposes in
	 * Jurisdiction outside India
	 */
	public String writeFlagIndicatingApplicantResidentForTaxPurposesInJurisdictionOutsideIndia(JSONObject record)
			throws InvalidValueException {
		String result = "";
		JSONObject personalEntityDetails = (JSONObject) record.get(DetailRecordFields.PERSONAL_DETAILS);
		if (personalEntityDetails != null) {
			flagIndicatingApplicantResidentForTaxPurposesInJurisdictionOutsideIndia = (Boolean) personalEntityDetails
					.get(DetailRecordFields.OVERSEAS_RESIDENT_FOR_TAX_PURPOSE);
			if (flagIndicatingApplicantResidentForTaxPurposesInJurisdictionOutsideIndia != null) {
				if (flagIndicatingApplicantResidentForTaxPurposesInJurisdictionOutsideIndia) {
					result = result.concat(DetailRecordValues.FLAG_INDICATING_APPLICANT_RESIDENT_OUTSIDE_INDIA_YES);
					result = result.concat("|");
					return result;
				} else {
					result = result.concat(DetailRecordValues.FLAG_INDICATING_APPLICANT_RESIDENT_OUTSIDE_INDIA_NO);
					result = result.concat("|");
					return result;
				}
			} else {
				logger.warn("Flag indicating applicant resident for tax purposes in jurisdiction outside India is missing in Detail record");
				result = result.concat("|");
			}
		}
		return result;
	}

	/* Method to write Jurisdiction Of residence */
	public String writeJurisdictionOfResidence(JSONObject record) throws InvalidValueException {
		JSONObject personalEntityDetails = (JSONObject) record.get(DetailRecordFields.PERSONAL_DETAILS);
		String result = "";

		if (personalEntityDetails != null) {
			flagIndicatingApplicantResidentForTaxPurposesInJurisdictionOutsideIndia = (Boolean) personalEntityDetails
					.get(DetailRecordFields.OVERSEAS_RESIDENT_FOR_TAX_PURPOSE);
			if (flagIndicatingApplicantResidentForTaxPurposesInJurisdictionOutsideIndia != null) {
				if (flagIndicatingApplicantResidentForTaxPurposesInJurisdictionOutsideIndia) {
					JSONObject jurisdictionOfResidenceAsObject = (JSONObject) record
							.get(DetailRecordFields.JURISDICTION_OF_RESIDENCE);
					if (jurisdictionOfResidenceAsObject != null) {
						jurisdictionOfResidence = (String) jurisdictionOfResidenceAsObject
								.get(DetailRecordFields.JURISDICTION_OF_RESIDENCE_COUNTRYCODE);
						result = result.concat(jurisdictionOfResidence);
						result = result.concat("|");
					} else {
							logger.warn("Country of residence field is empty under Jurisdiction of Residence");
//							throw new InvalidValueException("Country of residence field is empty");
						result = result.concat("|");
					}
				} else {
					return "|";
				}
			} else {
				return "|";
			}
		}
		return result;
	}

	/* Method to write TIN */
	public String writeTaxIdentificationNumber(JSONObject record) throws InvalidValueException {
		JSONObject personalEntityDetails = (JSONObject) record.get(DetailRecordFields.PERSONAL_DETAILS);
		String result = "";

		if (personalEntityDetails != null) {
			flagIndicatingApplicantResidentForTaxPurposesInJurisdictionOutsideIndia = (Boolean) personalEntityDetails
					.get(DetailRecordFields.OVERSEAS_RESIDENT_FOR_TAX_PURPOSE);
			if (flagIndicatingApplicantResidentForTaxPurposesInJurisdictionOutsideIndia != null) {
				if (flagIndicatingApplicantResidentForTaxPurposesInJurisdictionOutsideIndia) {
					taxIdentificationNumber = (String) record.get(DetailRecordFields.TIN_OR_EQUIVALENT);
					if (taxIdentificationNumber != null) {
						result = result.concat(taxIdentificationNumber);
						result = result.concat("|");
						return result;
					} else {
							logger.warn("Tax identification number or equivalent not provided");
//							throw new InvalidValueException("Tax identification number or equivalent not provided");
						result = result.concat("|");

					}
				} else {
					return "|";
				}
			} else {
				return "|";
			}
		}
		return result;
	}

	/* Method to write Country of Birth */
	public String writeCountryOfBirth(JSONObject record) throws InvalidValueException {
		JSONObject personalEntityDetails = (JSONObject) record.get(DetailRecordFields.PERSONAL_DETAILS);
		String result = "";

		if (personalEntityDetails != null) {
			flagIndicatingApplicantResidentForTaxPurposesInJurisdictionOutsideIndia = (Boolean) personalEntityDetails
					.get(DetailRecordFields.OVERSEAS_RESIDENT_FOR_TAX_PURPOSE);
			if (flagIndicatingApplicantResidentForTaxPurposesInJurisdictionOutsideIndia != null) {
				if (flagIndicatingApplicantResidentForTaxPurposesInJurisdictionOutsideIndia) {
					JSONObject countryOfBirthAsObject = (JSONObject) record.get(DetailRecordFields.COUNTRY_OF_BIRTH);
					if (countryOfBirthAsObject != null) {
						countryOfBirth = (String) countryOfBirthAsObject
								.get(DetailRecordFields.COUNTRY_OF_BIRTH_COUNTRYCODE);
						result = result.concat(countryOfBirth);
						result = result.concat("|");
					} else {
						logger.warn("Country of Birth field is empty");
//						throw new InvalidValueException("Country of Birth field is empty");
						result = result.concat("|");
					}
				} else {
					return "|";
				}
			} else {
				return "|";
			}
		}
		return result;
	}

	/* Method to write City Place of Birth */
	public String writeCityPlaceOfBirth(JSONObject record) throws InvalidValueException {
		JSONObject personalEntityDetails = (JSONObject) record.get(DetailRecordFields.PERSONAL_DETAILS);
		String result = "";

		if (personalEntityDetails != null) {
			flagIndicatingApplicantResidentForTaxPurposesInJurisdictionOutsideIndia = (Boolean) personalEntityDetails
					.get(DetailRecordFields.OVERSEAS_RESIDENT_FOR_TAX_PURPOSE);
			if (flagIndicatingApplicantResidentForTaxPurposesInJurisdictionOutsideIndia != null) {
				if (flagIndicatingApplicantResidentForTaxPurposesInJurisdictionOutsideIndia) {
					cityPlaceOfBirth = (String) record.get(DetailRecordFields.CITY_PLACE_OR_BIRTH);
					if (cityPlaceOfBirth != null) {
						result = result.concat(cityPlaceOfBirth);
						result = result.concat("|");
						return result;
					} else {
						logger.warn("City of Birth field is empty");
//						throw new InvalidValueException("City of Birth field is empty");
						result = result.concat("|");
						return result;
					}
				} else {
					return "|";
				}
			} else {
				return "|";
			}
		}
		return result;
	}

	/**
	 * 
	 * @param Single record of a person in ODP(DCH)
	 * @return Current/Permanent/Overseas Address Type
	 * @throws InvalidValueException
	 */
	public String writeCurrentPermanentOverseasAddressType(JSONObject record) throws InvalidValueException {
		JSONObject addressDetails = (JSONObject) record.get(DetailRecordFields.ADDRESS_DETAILS);
		String result = "";

		if (addressDetails != null) {
			currentPermanentOverseasAddressType = (String) addressDetails
					.get(DetailRecordFields.CURRENT_PERMANENT_OVERSEAS_ADDRESS_TYPE);
			if (currentPermanentOverseasAddressType != null) {
				if (currentPermanentOverseasAddressType
						.equalsIgnoreCase(DetailRecordValues.ADDRESS_TYPE_RESIDENT_BUSINESS)) {
					result = result.concat(DetailRecordValues.CURRENT_PERMANENT_OVERSEAS_ADDRESS_TYPE_RESIDENTBUSINESS);
					result = result.concat("|");
					return result;
				} else if (currentPermanentOverseasAddressType
						.equalsIgnoreCase(DetailRecordValues.ADDRESS_TYPE_RESIDENT)) {
					result = result.concat(DetailRecordValues.CURRENT_PERMANENT_OVERSEAS_ADDRESS_TYPE_RESIDENTIAL);
					result = result.concat("|");
					return result;
				} else if (currentPermanentOverseasAddressType
						.equalsIgnoreCase(DetailRecordValues.ADDRESS_TYPE_BUSINESS)) {
					result = result.concat(DetailRecordValues.CURRENT_PERMANENT_OVERSEAS_ADDRESS_TYPE_BUSINESS);
					result = result.concat("|");
					return result;
				} else if (currentPermanentOverseasAddressType
						.equalsIgnoreCase(DetailRecordValues.ADDRESS_TYPE_REGISTERED_OFFICE)) {
					result = result.concat(DetailRecordValues.CURRENT_PERMANENT_OVERSEAS_ADDRESS_TYPE_REGISTEREDOFFICE);
					result = result.concat("|");
					return result;
				} else if (currentPermanentOverseasAddressType
						.equalsIgnoreCase(DetailRecordValues.ADDRESS_TYPE_UNSPECIFIED)) {
					result = result.concat(DetailRecordValues.CURRENT_PERMANENT_OVERSEAS_ADDRESS_TYPE_UNSPECIFIED);
					result = result.concat("|");
					return result;
				} else {
					logger.warn("Invalid value in Address type field");
//					throw new InvalidValueException("Invalid value in Address type field");
					result = result.concat("|");
					return result;
				}
			} else {
				result = result.concat("|");
			}
		} else {
			result = result.concat("||||||||||||||||||||||||||||||||");
		}
		return result;
	}

	/**
	 * 
	 * @param Single record of a person in ODP(DCH)
	 * @return Current/Permanent/Overseas Address
	 * @throws InvalidValueException
	 */
	public String writeCurrentPermanentOverseasAddress(JSONObject record) throws InvalidValueException {
		JSONObject addressDetails = (JSONObject) record.get(DetailRecordFields.ADDRESS_DETAILS);
		String result = "";

		if (addressDetails != null) {
			JSONObject currentPermanentOverseasAddress = (JSONObject) addressDetails
					.get(DetailRecordFields.CURRENT_PERMANENT_OVERSEAS_ADDRESS);
			if (currentPermanentOverseasAddress != null) {
				currentPermanentOverseasAddressLine1 = (String) currentPermanentOverseasAddress
						.get(DetailRecordFields.CURRENT_PERMANENT_OVERSEAS_ADDRESS_LINE1);
				if (currentPermanentOverseasAddressLine1 != null) {
					result = result.concat(currentPermanentOverseasAddressLine1);
					result = result.concat("|");
				} else {
					result = result.concat("|");
				}

				currentPermanentOverseasAddressLine2 = (String) currentPermanentOverseasAddress
						.get(DetailRecordFields.CURRENT_PERMANENT_OVERSEAS_ADDRESS_LINE2);
				if (currentPermanentOverseasAddressLine2 != null) {
					result = result.concat(currentPermanentOverseasAddressLine2);
					result = result.concat("|");
				} else {
					result = result.concat("|");
				}

				currentPermanentOverseasAddressLine3 = (String) currentPermanentOverseasAddress
						.get(DetailRecordFields.CURRENT_PERMANENT_OVERSEAS_ADDRESS_LINE3);
				if (currentPermanentOverseasAddressLine3 != null) {
					result = result.concat(currentPermanentOverseasAddressLine3);
					result = result.concat("|");
				} else {
					result = result.concat("|");
				}

				currentPermanentOverseasCityTownVillage = (String) currentPermanentOverseasAddress
						.get(DetailRecordFields.CURRENT_PERMANENT_OVERSEAS_ADDRESS_CITY_TOWN_VILLAGE);
				if (currentPermanentOverseasCityTownVillage != null) {
					result = result.concat(currentPermanentOverseasCityTownVillage);
					result = result.concat("|");
				} else {
					result = result.concat("|");
				}

				currentPermanentOverseasDistrict = (String) currentPermanentOverseasAddress
						.get(DetailRecordFields.CURRENT_PERMANENT_OVERSEAS_ADDRESS_DISTRICT);
				if (currentPermanentOverseasDistrict != null) {
					result = result.concat(currentPermanentOverseasDistrict);
					result = result.concat("|");
				} else {
					result = result.concat("|");
				}

				/* STATE */
				JSONObject currentPermanentOverseasStateAsObject = (JSONObject) currentPermanentOverseasAddress
						.get(DetailRecordFields.CURRENT_PERMANENT_OVERSEAS_ADDRESS_STATE_UT);
				if (currentPermanentOverseasStateAsObject != null) {
					currentPermanentOverseasState = (String) currentPermanentOverseasStateAsObject
							.get(DetailRecordFields.CURRENT_PERMANENT_OVERSEAS_ADDRESS_STATE_UT_CODE);
					if (currentPermanentOverseasState != null) {
						result = result.concat(currentPermanentOverseasState);
						result = result.concat("|");
					} else {
						result = result.concat("|");
					}
				} else {
					result = result.concat("|");
				}

				/* COUNTRY */
				JSONObject currentPermanentOverseasCountryAsObject = (JSONObject) currentPermanentOverseasAddress
						.get(DetailRecordFields.CURRENT_PERMANENT_OVERSEAS_ADDRESS_COUNTRY);
				if (currentPermanentOverseasCountryAsObject != null) {
					currentPermanentOverseasCountry = (String) currentPermanentOverseasCountryAsObject
							.get(DetailRecordFields.CURRENT_PERMANENT_OVERSEAS_ADDRESS_COUNTRY_COUNTRYCODE);
					if (currentPermanentOverseasCountry != null) {
						result = result.concat(currentPermanentOverseasCountry);
						result = result.concat("|");
					} else {
						result = result.concat("|");
					}
				} else {
					result = result.concat("|");
				}

				/* PINCODE */
				if (currentPermanentOverseasCountry != null) {
					if (currentPermanentOverseasCountry.equalsIgnoreCase("IN")) {
						currentPermanentOverseasPincode = (String) currentPermanentOverseasAddress
								.get(DetailRecordFields.CURRENT_PERMANENT_OVERSEAS_ADDRESS_PINCODE);
						if (currentPermanentOverseasPincode != null) {
							String regex = "^[0-9]{6}$";
							Pattern pattern = Pattern.compile(regex);
							Matcher matcher = pattern.matcher(String.valueOf(currentPermanentOverseasPincode));
							if (matcher.matches()) {
								result = result.concat(String.valueOf(currentPermanentOverseasPincode));
								result = result.concat("|");
							} else {
//								logger.info(
//										"Invalid value in Current/Permanent/Overseas Address - PIN code field, Required 6 characters PIN code");
//								throw new InvalidValueException(
//										"Invalid value in Current/Permanent/Overseas Address - PIN code field, Required 6 characters PIN code");
								result = result.concat("|");
							}

						} else {
							logger.warn("Current/Permanent/Overseas Address - PIN code field is empty");
//							throw new InvalidValueException("Current/Permanent/Overseas Address - PIN code field is empty");
							result = result.concat("|");
						}
					} else {
						currentPermanentOverseasPincode = (String) currentPermanentOverseasAddress
								.get(DetailRecordFields.CURRENT_PERMANENT_OVERSEAS_ADDRESS_PINCODE);
						if (currentPermanentOverseasPincode != null) {
							result = result.concat(String.valueOf(currentPermanentOverseasPincode));
							result = result.concat("|");
						} else {
							result = result.concat("|");
						}
					}
				} else {
					result = result.concat("|");
				}
				return result;

			} else {
				logger.warn("Current/Permanent/Overseas/Address field is empty");
//				throw new InvalidValueException("Current/Permanent/Overseas/Address field is empty");
				result = result.concat("|").concat("|").concat("|").concat("|").concat("|").concat("|").concat("|")
						.concat("|");
				return result;
			}
		} else {
			return "";
		}
	}

	/**
	 * 
	 * @param Single record of a person in ODP(DCH)
	 * @return Proof of address submitted for current/permanent/overseas address
	 * @throws InvalidValueException
	 */
	public String writeProofOfAddressSubmittedForCurrentPermanentOverseas(JSONObject record)
			throws InvalidValueException {
		JSONObject addressDetails = (JSONObject) record.get(DetailRecordFields.ADDRESS_DETAILS);
		String result = "";

		if (addressDetails != null) {
			JSONObject proofOfAddressSubmittedForCurrentPermanentOverseasAsObject = (JSONObject) addressDetails
					.get(DetailRecordFields.POA_SUBMITTED_FOR_CURRENT_ADDRESS);
			if (proofOfAddressSubmittedForCurrentPermanentOverseasAsObject != null) {
				proofOfAddressSubmittedForCurrentPermanentOverseas = (String) proofOfAddressSubmittedForCurrentPermanentOverseasAsObject
						.get(DetailRecordFields.POA_SUBMITTED_FOR_CURRENT_ADDRESS_CODE);
				result = result.concat(proofOfAddressSubmittedForCurrentPermanentOverseas);
				result = result.concat("|");
				return result;
			} else {
				logger.warn("Proof of address submitted for current/permanent/overseas field is empty");
//				throw new InvalidValueException("Proof of address submitted for current/permanent/overseas field is empty");
				result = result.concat("|");
				return result;
			}
		} else {
			return result;
		}
	}

	/**
	 * 
	 * @param Single record of a person in ODP(DCH)
	 * @return Proof of address submitted for current/permanent/overseas address
	 *         others
	 * @throws InvalidValueException
	 */
	public String writeProofOfAddressSubmittedForCurrentPermanentOverseasOthers(JSONObject record)
			throws InvalidValueException {
		JSONObject addressDetails = (JSONObject) record.get(DetailRecordFields.ADDRESS_DETAILS);
		String result = "";

		if (addressDetails != null) {
			proofOfAddressSubmittedForCurrentPermanentOverseasOthers = (String) addressDetails
					.get(DetailRecordFields.POA_SUBMITTED_FOR_CURRENT_ADDRESS_OTHERS);
			if (proofOfAddressSubmittedForCurrentPermanentOverseasOthers != null) {
				if (proofOfAddressSubmittedForCurrentPermanentOverseas.equalsIgnoreCase("99")) {
					result = result.concat(proofOfAddressSubmittedForCurrentPermanentOverseasOthers);
					result = result.concat("|");
				} else {
					result = result.concat("|");
				}
			} else {
				result = result.concat("|");
			}
		} else {
			return result;
		}

		return result;
	}

	/**
	 * 
	 * @param Single record of a person in ODP(DCH)
	 * @return Flag indicating current/permanent/overseas address is same as
	 *         correspondance/local address
	 * @throws InvalidValueException
	 */
	public String writeFlagIndicatingIfCurrentPermanentAddressIsSameAsCorrespondanceLocalAddress(JSONObject record)
			throws InvalidValueException {
		JSONObject addressDetails = (JSONObject) record.get(DetailRecordFields.ADDRESS_DETAILS);
		if (addressDetails != null) {
			flagIndicatingIfCurrentPermanentAddressIsSameAsCorrespondanceLocalAddress = (Boolean) addressDetails
					.get(DetailRecordFields.FLAG_INDICATING_IF_CURRENT_SAME_AS_CORRESPONDING_ADDRESS);
			String result = "";

			if (flagIndicatingIfCurrentPermanentAddressIsSameAsCorrespondanceLocalAddress != null) {
				if (flagIndicatingIfCurrentPermanentAddressIsSameAsCorrespondanceLocalAddress) {
					result = result.concat(
							DetailRecordValues.FLAG_INDICATING_CURRENT_ADDRESS_ISSAMEAS_CORRESPONDENCE_ADDRESS_YES);
					result = result.concat("|");
					return result;
				} else {
					result = result.concat(
							DetailRecordValues.FLAG_INDICATING_CURRENT_ADDRESS_ISSAMEAS_CORRESPONDENCE_ADDRESS_NO);
					result = result.concat("|");
					return result;
				}
			} else {
				logger.warn("Flag indicating if Permanent Address is same as correspondence Address field is empty");
//				throw new InvalidValueException(
//						"Flag indicating if Permanent Address is same as correspondence Address field is empty");
				result = result.concat("|");
				return result;
			}
		} else {
			return "";
		}

	}

	/**
	 * 
	 * @param Single record of a person in ODP(DCH)
	 * @return Correspondance Address
	 * @throws InvalidValueException
	 */
	public String writeCorrespondanceAddress(JSONObject record) throws InvalidValueException {
		JSONObject addressDetails = (JSONObject) record.get(DetailRecordFields.ADDRESS_DETAILS);
		String result = "";

		if (addressDetails != null) {
			JSONObject correspondanceAddress = (JSONObject) addressDetails
					.get(DetailRecordFields.CORRESPONDENCE_ADDRESS);
			String correspondenceAddressCountryCode = null;

			if (correspondanceAddress != null) {
				correspondenceLocalAddressLine1 = (String) correspondanceAddress
						.get(DetailRecordFields.CORRESPONDENCE_ADDRESS_LINE1);

				if (flagIndicatingIfCurrentPermanentAddressIsSameAsCorrespondanceLocalAddress != null) {
					if (!flagIndicatingIfCurrentPermanentAddressIsSameAsCorrespondanceLocalAddress) {
						if (correspondenceLocalAddressLine1 != null) {
							result = result.concat(correspondenceLocalAddressLine1);
							result = result.concat("|");
						} else {
							result = result.concat("|");
						}
					} else {
						if (correspondenceLocalAddressLine1 != null) {
							result = result.concat(correspondenceLocalAddressLine1);
							result = result.concat("|");
						} else {
							result = result.concat("|");
						}
					}
				}
				correspondenceLocalAddressLine2 = (String) correspondanceAddress
						.get(DetailRecordFields.CORRESPONDENCE_ADDRESS_LINE2);
				if (correspondenceLocalAddressLine2 != null) {
					result = result.concat(correspondenceLocalAddressLine2);
					result = result.concat("|");
				} else {
					result = result.concat("|");
				}

				correspondenceLocalAddressLine3 = (String) correspondanceAddress
						.get(DetailRecordFields.CORRESPONDENCE_ADDRESS_LINE3);
				if (correspondenceLocalAddressLine3 != null) {
					result = result.concat(correspondenceLocalAddressLine3);
					result = result.concat("|");
				} else {
					result = result.concat("|");
				}

				correspondenceLocalCityTownVillage = (String) correspondanceAddress
						.get(DetailRecordFields.CORRESPONDANCE_ADDRESS_CITY_TOWN_VILLAGE);
				if (correspondenceLocalCityTownVillage != null) {
					result = result.concat(correspondenceLocalCityTownVillage);
					result = result.concat("|");
				} else {
					result = result.concat("|");
				}

				correspondenceLocalDistrict = (String) correspondanceAddress
						.get(DetailRecordFields.CORRESPONDANCE_ADDRESS_DISTRICT);
				if (correspondenceLocalDistrict != null) {
					result = result.concat(correspondenceLocalDistrict);
					result = result.concat("|");
				} else {
					result = result.concat("|");
				}

				/* STATE */
				JSONObject correspondenceAddressState = (JSONObject) correspondanceAddress
						.get(DetailRecordFields.CORRESPONDANCE_ADDRESS_STATE);
				if (correspondenceAddressState != null) {
					String correspondenceAddressStateCode = (String) correspondenceAddressState
							.get(DetailRecordFields.CORRESPONDANCE_ADDRESS_STATE_CODE);
					if (correspondenceAddressStateCode != null) {
						result = result.concat(correspondenceAddressStateCode);
						result = result.concat("|");
					} else {
						result = result.concat("|");
					}
				} else {
					result = result.concat("|");
				}

				/* COUNTRY */
				JSONObject correspondenceAddressCountry = (JSONObject) correspondanceAddress
						.get(DetailRecordFields.CORRESPONDANCE_ADDRESS_COUNTRY);
				if (correspondenceAddressCountry != null) {
					correspondenceAddressCountryCode = (String) correspondenceAddressCountry
							.get(DetailRecordFields.CORRESPONDANCE_ADDRESS_COUNTRY_CODE);
					if (correspondenceAddressCountryCode != null) {
						result = result.concat(correspondenceAddressCountryCode);
						result = result.concat("|");
					} else {
						result = result.concat("|");
					}
				} else {
					result = result.concat("|");
				}

				/* PINCODE */
				correspondenceLocalPINCODE = (String) correspondanceAddress
						.get(DetailRecordFields.CORRESPONDANCE_ADDRESS_PINCODE);
				if (correspondenceLocalPINCODE != null) {
					String regex = "^[0-9]{6}$";
					Pattern pattern = Pattern.compile(regex);
					Matcher matcher = pattern.matcher(String.valueOf(correspondenceLocalPINCODE));
					if (matcher.matches()) {
						result = result.concat(String.valueOf(correspondenceLocalPINCODE));
						result = result.concat("|");
					} else {
//						logger.info(
//								"Invalid value in Correspondence Address - PIN code field, Required 6 characters PIN code");
//						throw new InvalidValueException(
//								"Invalid value in Correspondence Address - PIN code field, Required 6 characters PIN code");
						result = result.concat("|");
					}
				} else {
					result = result.concat("|");
				}
			} else {
				result = result.concat("|").concat("|").concat("|").concat("|").concat("|").concat("|").concat("|")
						.concat("|");
			}
		} else {
			result = result.concat("");
		}
		return result;
	}

	/**
	 * 
	 * @param Single record of a person in ODP(DCH)
	 * @return Jurisdiction Address type
	 */
	public String writeJurisdictionAddressType(JSONObject record) {
		JSONObject addressDetails = (JSONObject) record.get(DetailRecordFields.ADDRESS_DETAILS);
		if (addressDetails != null) {
			jurisdictionAddressType = (String) addressDetails
					.get(DetailRecordFields.FLAG_INDICATING_IF_JURISDICTION_SAME_AS_CURRENT_OR_CORRESPONDANCE);
			if (jurisdictionAddressType != null) {
				if (jurisdictionAddressType.equalsIgnoreCase("Same as Current Address")) {
					return DetailRecordValues.JURISDICTION_ADDRESS_TYPE_SAME_AS_CURRENT_ADDRESS.concat("|");
				} else if (jurisdictionAddressType.equalsIgnoreCase("Same as Correspondence Address")) {
					return DetailRecordValues.JURISDICTION_ADDRESS_TYPE_SAME_AS_CORRESPONDENCE_ADDRESS.concat("|");
				} else {
					return DetailRecordValues.JURISDICTION_ADDRESS_TYPE_DIFFERENT.concat("|");
				}
			} else {
				return "|";
			}
		} else {
			return "";
		}
	}

	/**
	 * 
	 * @param Single record of a person in ODP(DCH)
	 * @return Jurisdiction Address
	 */
	public String writeJurisdictionAddress(JSONObject record) {
		JSONObject addressDetails = (JSONObject) record.get(DetailRecordFields.ADDRESS_DETAILS);
		String result = "";

		if (addressDetails != null) {
			JSONObject jurisdictionAddress = (JSONObject) addressDetails.get(DetailRecordFields.JURISDICTION_ADDRESS);

			if (jurisdictionAddress != null) {
				jurisdictionAddressLine1 = (String) jurisdictionAddress
						.get(DetailRecordFields.JURISDICTION_ADDRESS_LINE1);
				if (jurisdictionAddressLine1 != null) {
					result = result.concat(jurisdictionAddressLine1);
					result = result.concat("|");
				} else {
					result = result.concat("|");
				}

				jurisdictionAddressLine2 = (String) jurisdictionAddress
						.get(DetailRecordFields.JURISDICTION_ADDRESS_LINE2);
				if (jurisdictionAddressLine2 != null) {
					result = result.concat(jurisdictionAddressLine2);
					result = result.concat("|");
				} else {
					result = result.concat("|");
				}

				jurisdictionAddressLine3 = (String) jurisdictionAddress
						.get(DetailRecordFields.JURISDICTION_ADDRESS_LINE3);
				if (jurisdictionAddressLine3 != null) {
					result = result.concat(jurisdictionAddressLine3);
					result = result.concat("|");
				} else {
					result = result.concat("|");
				}

				jurisdictionAddressCityTownVillage = (String) jurisdictionAddress
						.get(DetailRecordFields.JURISDICTION_ADDRESS_CITY_TOWN_VILLAGE);
				if (jurisdictionAddressCityTownVillage != null) {
					result = result.concat(jurisdictionAddressCityTownVillage);
					result = result.concat("|");
				} else {
					result = result.concat("|");
				}

//				jurisdictionDistrict = (String) jurisdictionAddress.get(DetailRecordFields.JURISDICTION_ADDRESS_DISTRICT);
//				if(jurisdictionDistrict!=null) {
//					result=result.concat(jurisdictionDistrict);
//					result=result.concat("|");
//				}else {
//					result=result.concat("|");
//				}

				JSONObject jurisdictionState = (JSONObject) jurisdictionAddress
						.get(DetailRecordFields.JURISDICTION_ADDRESS_STATE);
				if (jurisdictionState != null) {
					jurisdictionStateCode = (String) jurisdictionState
							.get(DetailRecordFields.JURISDICTION_ADDRESS_STATE_CODE);
					if (jurisdictionStateCode != null) {
						result = result.concat(jurisdictionStateCode);
						result = result.concat("|");
					} else {
						result = result.concat("|");
					}
				} else {
					result = result.concat("|");
				}

				JSONObject jurisdictionCountry = (JSONObject) jurisdictionAddress
						.get(DetailRecordFields.JURISDICTION_ADDRESS_COUNTRY);
				if (jurisdictionCountry != null) {
					jurisdictionCountryCode = (String) jurisdictionCountry
							.get(DetailRecordFields.JURISDICTION_OF_RESIDENCE_COUNTRYCODE);
					if (jurisdictionCountryCode != null) {
						result = result.concat(jurisdictionCountryCode);
						result = result.concat("|");
					}
				} else {
					result = result.concat("|");
				}

				jurisdictionPINCODE = (String) jurisdictionAddress.get(DetailRecordFields.JURISDICTION_ADDRESS_PINCODE);
				if (jurisdictionPINCODE != null) {
					result = result.concat(jurisdictionPINCODE);
					result = result.concat("|");
				} else {
					result = result.concat("|");
				}
			} else {
				result = result.concat("|").concat("|").concat("|").concat("|").concat("|").concat("|").concat("|");
			}
		} else {
			result = result.concat("");
		}

		return result;
	}

	/**
	 * 
	 * @param Single record of a person in ODP(DCH)
	 * @return Contact Details of the applicant
	 */
	public String writeContactDetails(JSONObject record) {
		JSONObject contactDetails = (JSONObject) record.get(DetailRecordFields.CONTACT_DETAILS);
		String result = "";

		if (contactDetails != null) {
			JSONObject residence = (JSONObject) contactDetails.get(DetailRecordFields.CONTACT_DETAILS_RESIDENCE);
			if (residence != null) {
				residenceStdCode = (String) residence.get(DetailRecordFields.CONTACT_DETAILS_RESIDENCE_STDCODE);
				if (residenceStdCode != null) {
					result = result.concat(String.valueOf(residenceStdCode));
					result = result.concat("|");
				} else {
					result = result.concat("|");
				}
				residenceNumber = (String) residence.get(DetailRecordFields.CONTACT_DETAILS_RESIDENCE_NUMBER);
				if (residenceNumber != null) {
					result = result.concat(String.valueOf(residenceNumber));
					result = result.concat("|");
				} else {
					result = result.concat("|");
				}
			} else {
				result = result.concat("|").concat("|");
			}

			JSONObject office = (JSONObject) contactDetails.get(DetailRecordFields.CONTACT_DETAILS_OFFICE);
			if (office != null) {
				officeStdCode = (String) office.get(DetailRecordFields.CONTACT_DETAILS_OFFICE_STDCODE);
				if (officeStdCode != null) {
					result = result.concat(String.valueOf(officeStdCode));
					result = result.concat("|");
				} else {
					result = result.concat("|");
				}
				officeNumber = (String) office.get(DetailRecordFields.CONTACT_DETAILS_OFFICE_NUMBER);
				if (officeNumber != null) {
					result = result.concat(String.valueOf(officeNumber));
					result = result.concat("|");
				} else {
					result = result.concat("|");
				}
			} else {
				result = result.concat("|").concat("|");
			}

			JSONObject mobile = (JSONObject) contactDetails.get(DetailRecordFields.CONTACT_DETAILS_MOBILE);
			if (mobile != null) {
				mobileIsdCode = (String) mobile.get(DetailRecordFields.CONTACT_DETAILS_MOBILE_ISDCODE);
				if (mobileIsdCode != null) {
					result = result.concat(String.valueOf(mobileIsdCode));
					result = result.concat("|");
				} else {
					result = result.concat("|");
				}
				mobileNumber = (String) mobile.get(DetailRecordFields.CONTACT_DETAILS_MOBILE_NUMBER);
				if (mobileNumber != null) {
					result = result.concat(String.valueOf(mobileNumber));
					result = result.concat("|");
				} else {
					result = result.concat("|");
				}
			} else {
				result = result.concat("|").concat("|");
			}

			JSONObject fax = (JSONObject) contactDetails.get(DetailRecordFields.CONTACT_DETAILS_FAX);
			if (fax != null) {
				faxStdCode = (String) fax.get(DetailRecordFields.CONTACT_DETAILS_FAX_STDCODE);
				if (faxStdCode != null) {
					result = result.concat(String.valueOf(faxStdCode));
					result = result.concat("|");
				} else {
					result = result.concat("|");
				}
				faxNumber = (String) fax.get(DetailRecordFields.CONTACT_DETAILS_FAX_NUMBER);
				if (faxNumber != null) {
					result = result.concat(String.valueOf(faxNumber));
					result = result.concat("|");
				} else {
					result = result.concat("|");
				}
			} else {
				result = result.concat("|").concat("|");
			}

			email = (String) contactDetails.get(DetailRecordFields.CONTACT_DETAILS_EMAIL);
			if (email != null) {
				result = result.concat(String.valueOf(email));
				result = result.concat("|");
			} else {
				result = result.concat("|");
			}
		} else {
			result = result.concat("|").concat("|").concat("|").concat("|").concat("|").concat("|").concat("|")
					.concat("|").concat("|");
		}

		return result;
	}

	/* Method to write Remarks */
	public String writeRemarks(JSONObject record) {
		remarks = (String) record.get(DetailRecordFields.REMARKS);
		String result = "";
		if (remarks != null) {
			result = result.concat(remarks);
			result = result.concat("|");
		} else {
			result = result.concat("|");
		}
		return result;
	}

	/* Method to write Attestation */
	public String writeKYCVerification(JSONObject record) throws InvalidValueException {

		JSONObject attestation = (JSONObject) record.get(DetailRecordFields.KYC_VERIFICATION);
		String result = "";

		if (attestation != null) {

			// Applicant Declaration part starts
			JSONObject applicantDeclaration = (JSONObject) attestation.get(DetailRecordFields.APPLICANT_DECLARATION);
			if (applicantDeclaration != null) {
				dateOfDeclaration = (String) applicantDeclaration
						.get(DetailRecordFields.KYC_VERFICATION_DATE_OF_DECLARATION);
				if (dateOfDeclaration != null) {
					Date dod = null;
					try {
						dod = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.sssX").parse(dateOfDeclaration);
						SimpleDateFormat formatter = new SimpleDateFormat("dd-MM-yyyy");
						String strDoD = formatter.format(dod);
						result = result.concat(strDoD);
						result = result.concat("|");
					} catch (ParseException exception) {
						logger.debug("Invalid value in Date of Birth Field");
						throw new InvalidValueException("Invalid value in Date of Birth Field");
					}
				} else {
					logger.warn("Date of declaration Field is empty");
//					throw new InvalidValueException("Date of declaration Field is empty");
					result = result.concat("|");
				}

				placeOfDeclaration = (String) applicantDeclaration
						.get(DetailRecordFields.KYC_VERIFICATION_PLACE_OF_DECLARATION);
				if (placeOfDeclaration != null) {
					result = result.concat(placeOfDeclaration);
					result = result.concat("|");
				} else {
					logger.warn("Place of declaration field is empty");
//					throw new InvalidValueException("Place of declaration field is empty");
					result = result.concat("|");
				}
			} else {
				logger.warn("Applicant Declaration field is not present");
//				throw new InvalidValueException("Applicant Declaration field is not present");
				result = result.concat("|").concat("|");
			}
			// Applicant Declaration part ends

			dateWhenKYCVerificationWasCarriedOut = (String) attestation
					.get(DetailRecordFields.KYC_VERIFICATION_KYC_VERIFICATION_CARRIED_OUT_BY_DATE);
			if (dateWhenKYCVerificationWasCarriedOut != null) {
				Date dateOfKYC = null;
				try {
					dateOfKYC = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.sssX")
							.parse(dateWhenKYCVerificationWasCarriedOut);
					SimpleDateFormat formatter = new SimpleDateFormat("dd-MM-yyyy");
					String strDateOfKYC = formatter.format(dateOfKYC);
					result = result.concat(strDateOfKYC);
					result = result.concat("|");
				} catch (ParseException exception) {
					logger.debug("Invalid value in KYC Verification date Field");
					throw new InvalidValueException("Invalid value in KYC Verification date Field");
				}
			} else {
				logger.warn("KYC Verification date field is empty");
//				throw new InvalidValueException("KYC Verification date field is empty");
				result = result.concat("|");
			}

			typeOfDocumentsSubmitted = (String) attestation
					.get(DetailRecordFields.KYC_VERIFICATION_TYPES_OF_DOCUMENT_SUBMITTED);
			if (typeOfDocumentsSubmitted != null) {
				if (typeOfDocumentsSubmitted.equalsIgnoreCase(
						DetailRecordFields.KYC_VERIFICATION_TYPES_OF_DOCUMENT_SUBMITTED_CERTIFIED_COPIES)) {
					result = result.concat(DetailRecordValues.TYPES_OF_DOCUMENTS_SUBMITTED_CERTIFIED_COPIES);
					result = result.concat("|");
				}
			} else {
				logger.warn("Type of Document Submitted field is empty");
//				throw new InvalidValueException("Type of Document Submitted field is empty");
				result = result.concat("|");
			}

			JSONObject kycVerificationCarriedOutBy = (JSONObject) attestation
					.get(DetailRecordFields.KYC_VERIFICATION_KYC_VERIFICATION_CARRIED_OUT_BY);
			if (kycVerificationCarriedOutBy != null) {
				employeeName = (String) kycVerificationCarriedOutBy
						.get(DetailRecordFields.KYC_VERIFICATION_KYC_VERIFICATION_CARRIED_OUT_BY_EMPLOYEE_NAME);
				if (employeeName != null) {
					result = result.concat(employeeName);
					result = result.concat("|");
				} else {
					logger.warn("KYC Verification Name field is empty");
//					throw new InvalidValueException("KYC Verification Name field is empty");
					result = result.concat("|");
				}

				employeeDesignation = (String) kycVerificationCarriedOutBy
						.get(DetailRecordFields.KYC_VERIFICATION_KYC_VERIFICATION_CARRIED_OUT_BY_EMPLOYEE_DESIGNATION);
				if (employeeDesignation != null) {
					result = result.concat(employeeDesignation);
					result = result.concat("|");
				} else {
					logger.warn("KYC Verfication Designation field is empty");
//					throw new InvalidValueException("KYC Verification Designation field is empty");
					result = result.concat("|");
				}

				employeeBranch = (String) kycVerificationCarriedOutBy
						.get(DetailRecordFields.KYC_VERIFICATION_KYC_VERIFICATION_CARRIED_OUT_BY_EMPLOYEE_BRANCH);
				if (employeeBranch != null) {
					result = result.concat(employeeBranch);
					result = result.concat("|");
				} else {
					logger.warn("KYC Verification Branch field is empty");
//					throw new InvalidValueException("KYC Verification Branch field is empty");
					result = result.concat("|");
				}

				employeeCode = (String) kycVerificationCarriedOutBy
						.get(DetailRecordFields.KYC_VERIFICATION_KYC_VERIFICATION_CARRIED_OUT_BY_EMPLOYEE_CODE);
				if (employeeCode != null) {
					result = result.concat(employeeCode);
					result = result.concat("|");
				} else {
					logger.warn("KYC Verification Employee code is empty");
//					throw new InvalidValueException("KYC Verification Employee code is empty");
					result = result.concat("|");
				}
			} else {
				result = result.concat("|").concat("|").concat("|").concat("|");
			}
		} else {
			logger.warn("Error occurred while fetching Attestation details");
//			throw new InvalidValueException("Error occurred while fetching Attestation details");
			result = result.concat("|").concat("|").concat("|").concat("|").concat("|").concat("|").concat("|")
					.concat("|");
		}

		if (attestation != null) {
			JSONObject institutionDetails = (JSONObject) attestation
					.get(DetailRecordFields.KYC_VERIFICATION_INSTITUTION_DETAILS);
			if (institutionDetails != null) {
				institutionDetailName = (String) institutionDetails
						.get(DetailRecordFields.KYC_VERIFICATION_INSTITUTION_DETAILS_NAME);
				if (institutionDetailName != null) {
					result = result.concat(institutionDetailName);
					result = result.concat("|");
				} else {
					logger.warn("KYC Verification Organisation name field is empty");
//					throw new InvalidValueException("KYC Verification Organisation field is empty");
					result = result.concat("|");
				}

				institutionDetailCode = (String) institutionDetails
						.get(DetailRecordFields.KYC_VERIFICATION_INSTITUTION_DETAILS_CODE);
				if (institutionDetailCode != null) {
					result = result.concat(institutionDetailCode);
					result = result.concat("|");
				} else {
					logger.warn("Organisation code field is empty");
//					throw new InvalidValueException("Organisation code field is empty");
					result = result.concat("|");
				}
			} else {
				result = result.concat("|").concat("|");
			}
		} else {
			result = result.concat("|").concat("|");
		}

		return result;
	}

	public String writeNewLine() {
		return "\r\n";
	}

	/**
	 * 
	 * @param Single      record of a person in ODP(DCH)
	 * @param Application type
	 * @return flag indicating if Applicant Name has been updated
	 * @throws                         org.json.simple.parser.ParseException
	 * @throws InvalidRequestException
	 * @throws IOException
	 */
	public String writeApplicantNameUpdateFlag(JSONObject record)
			throws org.json.simple.parser.ParseException, InvalidRequestException, IOException {

		String applicantNameUpdateFlag = "";

		String applicationType = (String) record.get(DetailRecordFields.APPLICATION_TYPE);
		if (applicationType != null) {
			if (applicationType.equalsIgnoreCase(DetailRecordValues.APPLICATION_TYPE_NEW)) {
				applicantNameUpdateFlag = applicantNameUpdateFlag.concat("|");
			}
			if (applicationType.equalsIgnoreCase(DetailRecordValues.APPLICATION_TYPE_UPDATE)) {
				Boolean tempApplicantNUF = (boolean) record.get(DetailRecordFields.APPLICANT_NAME_UPDATE_FLAG);
				if(tempApplicantNUF!=null) {
					if(tempApplicantNUF) {
						applicantNameUpdateFlag=applicantNameUpdateFlag.concat(DetailRecordValues.UPDATED_FLAG_YES).concat("|");
					}else {
						applicantNameUpdateFlag=applicantNameUpdateFlag.concat(DetailRecordValues.UPDATED_FLAG_NO).concat("|");
					}
				}
				// Fetch the Audits for the record
//				if (auditsBetweenTheInterval == null) {
//					auditsBetweenTheInterval = retrieveAuditService.fetchAllAudits(record);
//				}
//
//				// Iterate each records and check if "Applicant Name Update" has been updated
//				// (By checking them in "data.new" of a record)
//				if (auditsBetweenTheInterval != null) {
//					for (int index = 0; index < auditsBetweenTheInterval.size(); index++) {
//						JSONObject singleAudit = (JSONObject) auditsBetweenTheInterval.get(index);
//						if (singleAudit != null) {
//							JSONObject data = (JSONObject) singleAudit.get("data");
//							if (data != null) {
//								JSONObject newData = (JSONObject) data.get("new");
//								if (newData != null) {
//									JSONObject personalEntityDetails = (JSONObject) newData
//											.get(DetailRecordFields.PERSONAL_DETAILS);
//									if (personalEntityDetails != null) {
//										JSONObject applicantName = (JSONObject) personalEntityDetails
//												.get(DetailRecordFields.APPLICANT_NAME);
//										if (applicantName != null) {
//											applicantNameUpdateFlag = applicantNameUpdateFlag
//													.concat(DetailRecordValues.UPDATED_FLAG_YES).concat("|");
//											// exit the loop if "Applicant Name" has been found updated even once
//											break;
//										}
//									}
//								}
//							}
//						}
//					}
//					// If "Applicant Name" hasn't been found updated.
//					if (applicantNameUpdateFlag.equals("")) {
//						applicantNameUpdateFlag = applicantNameUpdateFlag.concat(DetailRecordValues.UPDATED_FLAG_NO)
//								.concat("|");
//					}
//
//				} else {
//					logger.info("No updated records are found in the given interval");
//				}
			}
		}
		return applicantNameUpdateFlag;
	}

	/**
	 * 
	 * @param Single      record of a person in ODP(DCH)
	 * @param Application Type
	 * @return Flag indicating if Personal/Entity Details has been updated
	 * @throws InvalidRequestException
	 * @throws                         org.json.simple.parser.ParseException
	 * @throws JsonProcessingException
	 */
	public String writePersonalEntityDetailsUpdateFlag(JSONObject record)
			throws JsonProcessingException, org.json.simple.parser.ParseException, InvalidRequestException {

		String personalEntityDetailsUpdateFlag = "";
		String applicationType = (String) record.get(DetailRecordFields.APPLICATION_TYPE);
		if (applicationType != null) {
			if (applicationType.equalsIgnoreCase(DetailRecordValues.APPLICATION_TYPE_NEW)) {
				personalEntityDetailsUpdateFlag = personalEntityDetailsUpdateFlag.concat("|");
			}
			if (applicationType.equalsIgnoreCase(DetailRecordValues.APPLICATION_TYPE_UPDATE)) {
				boolean tempPersonalDetailUpdateFlag = (boolean) record.get(DetailRecordFields.PERSONAL_DETAILS_UPDATE_FLAG);
				if(tempPersonalDetailUpdateFlag) {
					personalEntityDetailsUpdateFlag = personalEntityDetailsUpdateFlag.concat(DetailRecordValues.UPDATED_FLAG_YES).concat("|");
				}else {
					personalEntityDetailsUpdateFlag=personalEntityDetailsUpdateFlag.concat(DetailRecordValues.UPDATED_FLAG_NO).concat("|");
				}

//				// Fetch the Audits for the record
//				if (auditsBetweenTheInterval == null) {
//					auditsBetweenTheInterval = retrieveAuditService.fetchAllAudits(record);
//				}
//
//				// Iterate each records and check if "Contact Details" has been updated
//				// (By checking them in "data.new" of a record)
//				for (int index = 0; index < auditsBetweenTheInterval.size(); index++) {
//					JSONObject singleAudit = (JSONObject) auditsBetweenTheInterval.get(index);
//					if (singleAudit != null) {
//						JSONObject data = (JSONObject) singleAudit.get("data");
//						if (data != null) {
//							JSONObject newData = (JSONObject) data.get("new");
//							if (newData != null) {
//								JSONObject personalEntityDetails = (JSONObject) newData
//										.get(DetailRecordFields.PERSONAL_DETAILS);
//								if (personalEntityDetails != null) {
//									personalEntityDetailsUpdateFlag = personalEntityDetailsUpdateFlag
//											.concat(DetailRecordValues.UPDATED_FLAG_YES).concat("|");
//									// exit the loop if "Contact Details" has been found updated even once
//									break;
//								}
//							}
//						}
//					}
//				}
//				// If "Contact Details" haven't been found updated.
//				if (personalEntityDetailsUpdateFlag.equals("")) {
//					personalEntityDetailsUpdateFlag = personalEntityDetailsUpdateFlag
//							.concat(DetailRecordValues.UPDATED_FLAG_NO).concat("|");
//				}

			}
		}

		return personalEntityDetailsUpdateFlag;
	}

	/**
	 * 
	 * @param Single      record of a person in ODP(DCH)
	 * @param Application Type
	 * @return Flag indicating if Address Details has been updated
	 * @throws JsonProcessingException
	 * @throws                         org.json.simple.parser.ParseException
	 * @throws InvalidRequestException
	 */
	public String writeAddressDetailsUpdateFlag(JSONObject record)
			throws JsonProcessingException, org.json.simple.parser.ParseException, InvalidRequestException {

		String addressDetailsUpdateFlag = "";
		String applicationType = (String) record.get(DetailRecordFields.APPLICATION_TYPE);
		if (applicationType != null) {
			if (applicationType.equalsIgnoreCase(DetailRecordValues.APPLICATION_TYPE_NEW)) {
				addressDetailsUpdateFlag = addressDetailsUpdateFlag.concat("|");
			}
			if (applicationType.equalsIgnoreCase(DetailRecordValues.APPLICATION_TYPE_UPDATE)) {
				boolean tempAddressDetailsUpdateFlag = (boolean) record.get(DetailRecordFields.ADDRESS_DETAILS_UPDATE_FLAG);
				if(tempAddressDetailsUpdateFlag) {
					addressDetailsUpdateFlag = addressDetailsUpdateFlag.concat(DetailRecordValues.UPDATED_FLAG_YES).concat("|");
				}else {
					addressDetailsUpdateFlag = addressDetailsUpdateFlag.concat(DetailRecordValues.UPDATED_FLAG_NO).concat("|");
				}
				
				// Fetch the Audits for the record
//				if (auditsBetweenTheInterval == null) {
//					auditsBetweenTheInterval = retrieveAuditService.fetchAllAudits(record);
//				}
//
//				// Iterate each records and check if "Contact Details" has been updated
//				// (By checking them in "data.new" of a record)
//				for (int index = 0; index < auditsBetweenTheInterval.size(); index++) {
//					JSONObject singleAudit = (JSONObject) auditsBetweenTheInterval.get(index);
//					if (singleAudit != null) {
//						JSONObject data = (JSONObject) singleAudit.get("data");
//						if (data != null) {
//							JSONObject newData = (JSONObject) data.get("new");
//							if (newData != null) {
//								JSONObject addressDetails = (JSONObject) newData
//										.get(DetailRecordFields.ADDRESS_DETAILS);
//								if (addressDetails != null) {
//									addressDetailsUpdateFlag = addressDetailsUpdateFlag
//											.concat(DetailRecordValues.UPDATED_FLAG_YES).concat("|");
//									// exit the loop if "Contact Details" has been found updated even once
//									break;
//								}
//							}
//						}
//					}
//				}
//				// If "Contact Details" haven't been found updated.
//				if (addressDetailsUpdateFlag.equals("")) {
//					addressDetailsUpdateFlag = addressDetailsUpdateFlag.concat(DetailRecordValues.UPDATED_FLAG_NO)
//							.concat("|");
//				}

			}

		}

		return addressDetailsUpdateFlag;
	}

	/**
	 * 
	 * @param Single          record of a person from ODP(DCH)
	 * @param applicationType of the record
	 * @return "01" or "02" based on whether the "Contact Details" has been updated
	 *         or not
	 * @throws JsonProcessingException
	 * @throws                         org.json.simple.parser.ParseException
	 * @throws InvalidRequestException
	 */
	public String writeContactDetailsUpdateFlag(JSONObject record)
			throws JsonProcessingException, org.json.simple.parser.ParseException, InvalidRequestException {
		String contactDetailsUpdateFlag = "";

		String applicationType = (String) record.get(DetailRecordFields.APPLICATION_TYPE);
		if (applicationType != null) {
			if (applicationType.equalsIgnoreCase(DetailRecordValues.APPLICATION_TYPE_NEW)) {
				contactDetailsUpdateFlag = contactDetailsUpdateFlag.concat("|");
			}
			if (applicationType.equalsIgnoreCase(DetailRecordValues.APPLICATION_TYPE_UPDATE)) {
				boolean tempContactDetailsUpdateFlag = (boolean) record.get(DetailRecordFields.CONTACT_DETAILS_UPDATE_FLAG);
				if(tempContactDetailsUpdateFlag) {
					contactDetailsUpdateFlag = contactDetailsUpdateFlag.concat(DetailRecordValues.UPDATED_FLAG_YES).concat("|");
				}else {
					contactDetailsUpdateFlag=contactDetailsUpdateFlag.concat(DetailRecordValues.UPDATED_FLAG_NO).concat("|");
				}
				// Fetch the Audits for the record
//				if (auditsBetweenTheInterval == null) {
//					auditsBetweenTheInterval = retrieveAuditService.fetchAllAudits(record);
//				}
//
//				// Iterate each records and check if "Contact Details" has been updated
//				// (By checking them in "data.new" of a record)
//				for (int index = 0; index < auditsBetweenTheInterval.size(); index++) {
//					JSONObject singleAudit = (JSONObject) auditsBetweenTheInterval.get(index);
//					if (singleAudit != null) {
//						JSONObject data = (JSONObject) singleAudit.get("data");
//						if (data != null) {
//							JSONObject newData = (JSONObject) data.get("new");
//							if (newData != null) {
//								JSONObject contactDetails = (JSONObject) newData
//										.get(DetailRecordFields.CONTACT_DETAILS);
//								if (contactDetails != null) {
//									contactDetailsUpdateFlag = contactDetailsUpdateFlag
//											.concat(DetailRecordValues.UPDATED_FLAG_YES).concat("|");
//									// exit the loop if "Contact Details" has been found updated even once
//									break;
//								}
//							}
//						}
//					}
//				}
//				// If "Contact Details" haven't been found updated.
//				if (contactDetailsUpdateFlag.equals("")) {
//					contactDetailsUpdateFlag = contactDetailsUpdateFlag.concat(DetailRecordValues.UPDATED_FLAG_NO)
//							.concat("|");
//				}

			}

		}

		return contactDetailsUpdateFlag;
	}

	/**
	 * 
	 * @param Single      record of a person from ODP(DCH)
	 * @param Application Type
	 * @return Flag indicating Remarks update
	 * @throws JsonProcessingException
	 * @throws                         org.json.simple.parser.ParseException
	 * @throws InvalidRequestException
	 */
	public String writeRemarksUpdateFlag(JSONObject record)
			throws JsonProcessingException, org.json.simple.parser.ParseException, InvalidRequestException {
		String remarksUpdateFlag = "";

		String applicationType = (String) record.get(DetailRecordFields.APPLICATION_TYPE);
		if (applicationType != null) {
			if (applicationType.equalsIgnoreCase(DetailRecordValues.APPLICATION_TYPE_NEW)) {
				remarksUpdateFlag = remarksUpdateFlag.concat("|");
			}
			if (applicationType.equalsIgnoreCase(DetailRecordValues.APPLICATION_TYPE_UPDATE)) {
				boolean tempRemarksUpdateFlag = (boolean) record.get(DetailRecordFields.REMARKS_UPDATE_FLAG);
				if(tempRemarksUpdateFlag) {
					remarksUpdateFlag = remarksUpdateFlag.concat(DetailRecordValues.UPDATED_FLAG_YES).concat("|");
				}else {
					remarksUpdateFlag = remarksUpdateFlag.concat(DetailRecordValues.UPDATED_FLAG_NO).concat("|");
				}
				// Fetch the Audits for the record
//				if (auditsBetweenTheInterval == null) {
//					auditsBetweenTheInterval = retrieveAuditService.fetchAllAudits(record);
//				}
//
//				// Iterate each records and check if "Remarks" has been updated
//				// (By checking them in "data.new" of a record)
//				if (auditsBetweenTheInterval != null) {
//					for (int index = 0; index < auditsBetweenTheInterval.size(); index++) {
//						JSONObject singleAudit = (JSONObject) auditsBetweenTheInterval.get(index);
//						if (singleAudit != null) {
//							JSONObject data = (JSONObject) singleAudit.get("data");
//							if (data != null) {
//								JSONObject newData = (JSONObject) data.get("new");
//								if (newData != null) {
//									String remarks = (String) newData.get(DetailRecordFields.REMARKS);
//									if (remarks != null) {
//										renameUpdateFlag = renameUpdateFlag.concat(DetailRecordValues.UPDATED_FLAG_YES)
//												.concat("|");
//										// exit the loop if "Remarks" has been found updated even once
//										break;
//									}
//								}
//							}
//						}
//					}
//					// If "Remarks" hasn't been found updated.
//					if (renameUpdateFlag.equals("")) {
//						renameUpdateFlag = renameUpdateFlag.concat(DetailRecordValues.UPDATED_FLAG_NO).concat("|");
//					}
//
//				} else {
//					logger.info("No updated records are found in the given interval");
//				}

			}
		}

		return remarksUpdateFlag;
	}

	/**
	 * 
	 * @param Single      record of a person in ODP(DCH)
	 * @param Application Type
	 * @return KYC Verification update flag
	 * @throws JsonProcessingException
	 * @throws                         org.json.simple.parser.ParseException
	 * @throws InvalidRequestException
	 */
	public String writeKycVerificationUpdateFlag(JSONObject record)
			throws JsonProcessingException, org.json.simple.parser.ParseException, InvalidRequestException {
		String kycVerficationUpdateFlag = "";

		String applicationType = (String) record.get(DetailRecordFields.APPLICATION_TYPE);
		if (applicationType != null) {
			if (applicationType.equalsIgnoreCase(DetailRecordValues.APPLICATION_TYPE_NEW)) {
				kycVerficationUpdateFlag = kycVerficationUpdateFlag.concat("|");
			}
			if (applicationType.equalsIgnoreCase(DetailRecordValues.APPLICATION_TYPE_UPDATE)) {
				boolean tempKycVerificationUpdateFlag = (boolean) record.get(DetailRecordFields.KYC_VERIFICATION_UPDATE_FLAG);
				if(tempKycVerificationUpdateFlag) {
					kycVerficationUpdateFlag=kycVerficationUpdateFlag.concat(DetailRecordValues.UPDATED_FLAG_YES).concat("|");
				}else {
					kycVerficationUpdateFlag=kycVerficationUpdateFlag.concat(DetailRecordValues.UPDATED_FLAG_NO).concat("|");
				}
//				// Fetch the Audits for the record
//				if (auditsBetweenTheInterval == null) {
//					auditsBetweenTheInterval = retrieveAuditService.fetchAllAudits(record);
//				}
//
//				// Iterate each records and check if "KYC Verification details" has been updated
//				// (By checking them in "data.new" of a record)
//				if (auditsBetweenTheInterval != null) {
//					for (int index = 0; index < auditsBetweenTheInterval.size(); index++) {
//						JSONObject singleAudit = (JSONObject) auditsBetweenTheInterval.get(index);
//						if (singleAudit != null) {
//							JSONObject data = (JSONObject) singleAudit.get("data");
//							if (data != null) {
//								JSONObject newData = (JSONObject) data.get("new");
//								if (newData != null) {
//									JSONObject applicantName = (JSONObject) newData
//											.get(DetailRecordFields.KYC_VERIFICATION);
//									if (applicantName != null) {
//										kycVerficationUpdateFlag = kycVerficationUpdateFlag
//												.concat(DetailRecordValues.UPDATED_FLAG_YES).concat("|");
//										// exit the loop if "KYC Verification details" has been found updated even once
//										break;
//									}
//								}
//							}
//						}
//					}
//					// If "KYC Verification details" haven't been found updated.
//					if (kycVerficationUpdateFlag.equals("")) {
//						kycVerficationUpdateFlag = kycVerficationUpdateFlag.concat(DetailRecordValues.UPDATED_FLAG_NO)
//								.concat("|");
//					}
//
//				} else {
//					logger.info("No updated records are found in the given interval");
//				}

			}
		}

		return kycVerficationUpdateFlag;
	}

	/**
	 * 
	 * @param Single      record of a person in ODP(DCH)
	 * @param Application Type
	 * @return Identity Details Update flag
	 * @throws JsonProcessingException
	 * @throws                         org.json.simple.parser.ParseException
	 * @throws InvalidRequestException
	 */
	public String writeIdentityDetailsUpdateFlag(JSONObject record)
			throws JsonProcessingException, org.json.simple.parser.ParseException, InvalidRequestException {
		String identityDetailsUpdateFlag = "";

		String applicationType = (String) record.get(DetailRecordFields.APPLICATION_TYPE);
		if (applicationType != null) {
			if (applicationType.equalsIgnoreCase(DetailRecordValues.APPLICATION_TYPE_NEW)) {
				identityDetailsUpdateFlag = identityDetailsUpdateFlag.concat("|");
			}
			if (applicationType.equalsIgnoreCase(DetailRecordValues.APPLICATION_TYPE_UPDATE)) {
				boolean tempIdentityUpdateFlag = (boolean) record.get(DetailRecordFields.IDENTITY_DETAILS_UPDATE_FLAG);
				if(tempIdentityUpdateFlag) {
					identityDetailsUpdateFlag = identityDetailsUpdateFlag.concat(DetailRecordValues.UPDATED_FLAG_YES).concat("|");
				}else {
					identityDetailsUpdateFlag=identityDetailsUpdateFlag.concat(DetailRecordValues.UPDATED_FLAG_NO).concat("|");
					
//				// Fetch the Audits for the record
//				if (auditsBetweenTheInterval == null) {
//					auditsBetweenTheInterval = retrieveAuditService.fetchAllAudits(record);
//				}
//
//				// Iterate each records and check if "KYC Verification details" has been updated
//				// (By checking them in "data.new" of a record)
//				if (auditsBetweenTheInterval != null) {
//					for (int index = 0; index < auditsBetweenTheInterval.size(); index++) {
//						JSONObject singleAudit = (JSONObject) auditsBetweenTheInterval.get(index);
//						if (singleAudit != null) {
//							JSONObject data = (JSONObject) singleAudit.get("data");
//							if (data != null) {
//								JSONObject newData = (JSONObject) data.get("new");
//								if (newData != null) {
//									JSONArray applicantName = (JSONArray) newData.get(DetailRecordFields.IDENTITY);
//									if (applicantName != null) {
//										identityDetailsUpdateFlag = identityDetailsUpdateFlag
//												.concat(DetailRecordValues.UPDATED_FLAG_YES).concat("|");
//										// exit the loop if "KYC Verification details" has been found updated even once
//										break;
//									}
//								}
//							}
//						}
//					}
//					// If "KYC Verification details" haven't been found updated.
//					if (identityDetailsUpdateFlag.equals("")) {
//						identityDetailsUpdateFlag = identityDetailsUpdateFlag.concat(DetailRecordValues.UPDATED_FLAG_NO)
//								.concat("|");
//					}
//
//				} else {
//					logger.info("No updated records are found in the given interval");
				}

			}
		}

		return identityDetailsUpdateFlag;
	}

	/**
	 * 
	 * @param Single      record of a person in ODP(DCH)
	 * @param Application Type
	 * @return Related Person Details update flag
	 * @throws JsonProcessingException
	 * @throws                         org.json.simple.parser.ParseException
	 * @throws InvalidRequestException
	 */
	public String writeRelatedPersonDetailsFlag(JSONObject record)
			throws JsonProcessingException, org.json.simple.parser.ParseException, InvalidRequestException {

		String relatedPersonDetailsUpdateFlag = "";
		String applicationType = (String) record.get(DetailRecordFields.APPLICATION_TYPE);
		if (applicationType != null) {
			if (applicationType.equalsIgnoreCase(DetailRecordValues.APPLICATION_TYPE_NEW)) {
				relatedPersonDetailsUpdateFlag = relatedPersonDetailsUpdateFlag.concat("|");
			}
			if (applicationType.equalsIgnoreCase(DetailRecordValues.APPLICATION_TYPE_UPDATE)) {
				Boolean tempRelatedPersonUpdateFlag = (Boolean) record.get(DetailRecordFields.RELATED_PERSON_DETAILS_FLAG);
				if(tempRelatedPersonUpdateFlag!=null) {
					if(tempRelatedPersonUpdateFlag) {
						relatedPersonDetailsUpdateFlag=relatedPersonDetailsUpdateFlag.concat(DetailRecordValues.UPDATED_FLAG_YES).concat("|");
					}else {
						relatedPersonDetailsUpdateFlag=relatedPersonDetailsUpdateFlag.concat(DetailRecordValues.UPDATED_FLAG_NO).concat("|");
					}
				}
				// Fetch the Audits for the record
//				if (auditsBetweenTheInterval == null) {
//					auditsBetweenTheInterval = retrieveAuditService.fetchAllAudits(record);
//				}
//
//				// Iterate each records and check if "Related person details" has been updated
//				// (By checking them in "data.new" of a record)
//				if (auditsBetweenTheInterval != null) {
//					for (int index = 0; index < auditsBetweenTheInterval.size(); index++) {
//						JSONObject singleAudit = (JSONObject) auditsBetweenTheInterval.get(index);
//						if (singleAudit != null) {
//							JSONObject data = (JSONObject) singleAudit.get("data");
//							if (data != null) {
//								JSONObject newData = (JSONObject) data.get("new");
//								if (newData != null) {
//									JSONArray applicantName = (JSONArray) newData
//											.get(DetailRecordFields.RELATED_PERSON);
//									if (applicantName != null) {
//										relatedPersonDetailsUpdateFlag = relatedPersonDetailsUpdateFlag
//												.concat(DetailRecordValues.UPDATED_FLAG_YES).concat("|");
//										// exit the loop if "Related person details" has been found updated even once
//										break;
//									}
//								}
//							}
//						}
//					}
//					// If "Related person details" haven't been found updated.
//					if (relatedPersonDetailsUpdateFlag.equals("")) {
//						relatedPersonDetailsUpdateFlag = relatedPersonDetailsUpdateFlag
//								.concat(DetailRecordValues.UPDATED_FLAG_NO).concat("|");
//					}
//
//				} else {
//					logger.info("No updated records are found in the given interval");
//				}

			}
		}

		return relatedPersonDetailsUpdateFlag;
	}

	public String writeControllingPersonDetailsFlag(JSONObject record) {
		String controllingPersonUpdateFlag = "";
		String applicationType = (String) record.get(DetailRecordFields.APPLICATION_TYPE);
		if (applicationType != null) {
			if (applicationType.equalsIgnoreCase(DetailRecordValues.APPLICATION_TYPE_NEW)) {
				controllingPersonUpdateFlag = controllingPersonUpdateFlag.concat("|");
			}
			if (applicationType.equalsIgnoreCase(DetailRecordValues.APPLICATION_TYPE_UPDATE)) {
				Boolean tempRelatedPersonUpdateFlag = (Boolean) record.get(DetailRecordFields.CONTROLLING_PERSON_DETAILS_FLAG);
				if(tempRelatedPersonUpdateFlag!=null) {
					if(tempRelatedPersonUpdateFlag) {
						controllingPersonUpdateFlag=controllingPersonUpdateFlag.concat(DetailRecordValues.UPDATED_FLAG_YES).concat("|");
					}else {
						controllingPersonUpdateFlag=controllingPersonUpdateFlag.concat(DetailRecordValues.UPDATED_FLAG_NO).concat("|");
					}
				}
			}
		}
		return controllingPersonUpdateFlag;
	}

	/**
	 * 
	 * @param record
	 * @return
	 * @throws InvalidRequestException
	 * @throws                         org.json.simple.parser.ParseException
	 * @throws JsonProcessingException
	 */
	public String writeImageDetailsUpdateFlag(JSONObject record)
			throws JsonProcessingException, org.json.simple.parser.ParseException, InvalidRequestException {

		String imageDetailsUpdateFlag = "";
		String applicationType = (String) record.get(DetailRecordFields.APPLICATION_TYPE);
		if (applicationType != null) {
			if (applicationType.equalsIgnoreCase(DetailRecordValues.APPLICATION_TYPE_NEW)) {
				imageDetailsUpdateFlag = imageDetailsUpdateFlag.concat("|");
			}
			if (applicationType.equalsIgnoreCase(DetailRecordValues.APPLICATION_TYPE_UPDATE)) {
				boolean tempImageUpdateFlag = (boolean) record.get(DetailRecordFields.IMAGE_DETAILS_UPDATE_FLAG);
				if(tempImageUpdateFlag) {
					imageDetailsUpdateFlag=imageDetailsUpdateFlag.concat(DetailRecordValues.UPDATED_FLAG_YES).concat("|");
				}else {
					imageDetailsUpdateFlag=imageDetailsUpdateFlag.concat(DetailRecordValues.UPDATED_FLAG_NO).concat("|");
				}
				// Fetch the Audits for the record
//				if (auditsBetweenTheInterval == null) {
//					auditsBetweenTheInterval = retrieveAuditService.fetchAllAudits(record);
//				}
//
//				// Iterate each records and check if "Image details" has been updated
//				// (By checking them in "data.new" of a record)
//				if (auditsBetweenTheInterval != null) {
//					for (int index = 0; index < auditsBetweenTheInterval.size(); index++) {
//						JSONObject singleAudit = (JSONObject) auditsBetweenTheInterval.get(index);
//						if (singleAudit != null) {
//							JSONObject data = (JSONObject) singleAudit.get("data");
//							if (data != null) {
//								JSONObject newData = (JSONObject) data.get("new");
//								if (newData != null) {
//									JSONArray applicantName = (JSONArray) newData.get(DetailRecordFields.DOCUMENTS);
//									if (applicantName != null) {
//										imageDetailsUpdateFlag = imageDetailsUpdateFlag
//												.concat(DetailRecordValues.UPDATED_FLAG_YES).concat("|");
//										// exit the loop if "Image details" has been found updated even once
//										break;
//									}
//								}
//							}
//						}
//					}
//					// If "Image details" haven't been found updated.
//					if (imageDetailsUpdateFlag.equals("")) {
//						imageDetailsUpdateFlag = imageDetailsUpdateFlag.concat(DetailRecordValues.UPDATED_FLAG_NO)
//								.concat("|");
//					}
//
//				} else {
//					logger.info("No updated records are found in the given interval");
//				}

			}
		}

		// reseting the auditsBetweenTheInterval
//		auditsBetweenTheInterval = null;

		return imageDetailsUpdateFlag;
	}

	public String writeAccountHolderTypeFlag(JSONObject record) {
		JSONObject accountDetails = (JSONObject) record.get(DetailRecordFields.ACCOUNT_DETAILS);
		if (accountDetails != null) {
			String accountTypeHolder = (String) record.get(DetailRecordFields.ACCOUNT_HOLDER_TYPE_FLAG);
			if (accountTypeHolder != null) {
				if (accountTypeHolder.equalsIgnoreCase("US Reportable")) {
					return DetailRecordValues.ACCOUNT_TYPE_HOLDER_FLAG_US_REPORTABLE.concat("|");
				} else {
					return DetailRecordValues.ACCOUNT_TYPE_HOLDER_FLAG_OTHER_REPORTABLE.concat("|");
				}
			} else {
				return "|";
			}
		}
		return "";
	}

	public String writeAccountHolderType(JSONObject record) {
		JSONObject accountDetails = (JSONObject) record.get(DetailRecordFields.ACCOUNT_DETAILS);
		if (accountDetails != null) {
			String accountHolderType = (String) record.get(DetailRecordFields.ACCOUNT_HOLDER_FLAG);
			if (accountHolderType != null) {
				return accountHolderType.concat("|");
			} else {
				return "|";
			}
		}
		return "";
	}

	public String writePlaceOfInCorporation(JSONObject record) {
		String placeOfCorporation = (String) record.get(DetailRecordFields.PLACE_OF_INCORPORATION);
		if (placeOfCorporation != null) {
			return placeOfCorporation.concat("|");
		} else {
			return "|";
		}
	}

	public String writeDateOfCommencementOfBusiness(JSONObject record) {
		String dateOfCommencementOfBusiness = (String) record.get(DetailRecordFields.DATE_OF_COMMENCEMENT_OF_BUSINESS);
		if (dateOfCommencementOfBusiness != null) {
			// Need to add date parsing
			return dateOfCommencementOfBusiness.concat("|");
		} else {
			return "|";
		}
	}

	public String writeCountryOfIncorporation(JSONObject record) {
		JSONObject countryOfIncorporation = (JSONObject) record.get(DetailRecordFields.COUNTRY_OF_INCORPORATION);
		if (countryOfIncorporation != null) {
			// Need to add object parsing
			return "add Country of Incorporation";
		} else {
			return "|";
		}
	}

	public String writeCountryOfResidenceAsPerTaxLaws(JSONObject record) {
		JSONObject countryOfResidence = (JSONObject) record
				.get(DetailRecordFields.COUNTRY_OF_RESIDENCE_AS_PER_TAX_LAWS);
		if (countryOfResidence != null) {
			// Need to add object parsing
			return "add Country of Residence";
		} else {
			return "|";
		}
	}

	public String writeIdentificationType(JSONObject record) {
		JSONObject identificationType = (JSONObject) record.get(DetailRecordFields.IDENTIFICATION_TYPE);
		if (identificationType != null) {
			// Need to add object parsing
			return "add Identification type";
		} else {
			return "|";
		}
	}

	public String writeTIN(JSONObject record) {
		String tin = (String) record.get(DetailRecordFields.TAX_IDENTIFICATION_NUMBER);
		if (tin != null) {
			return tin.concat("|");
		} else {
			return "|";
		}
	}

	public String writeTINIssuingCountry(JSONObject record) {
		JSONObject tinIssuingCountry = (JSONObject) record.get(DetailRecordFields.TIN_ISSUING_COUNTRY);
		if (tinIssuingCountry != null) {
			// Need to add object parsing
			return "add TIN Issuing country";
		} else {
			return "|";
		}
	}

	public String writePAN(JSONObject record) {
		String pan = (String) record.get(DetailRecordFields.PAN);
		if (pan != null) {
			return pan.concat("|");
		} else {
			return "|";
		}
	}

	public String writeCorrespondenceAddressType(JSONObject record) {
		String correspondenceAddressType = (String) record.get(DetailRecordFields.CORRESPONDENCE_ADDRESS_TYPE);
		if (correspondenceAddressType != null) {
			return "add correspondence address type";
		} else {
			return "|";
		}
	}

	public String writePOASubmittedForCorrespondenceAddress(JSONObject record) {
		String poaSubmittedForCorrespondenceAddress = (String) record
				.get(DetailRecordFields.POA_SUBMITTED_FOR_CORRESPONDENCE_ADDRESS);
		if (poaSubmittedForCorrespondenceAddress != null) {
			return "add POA Submitted for Correspondence Address";
		} else {
			return "|";
		}
	}

	public String writeAddressInJurisdictionType(JSONObject record) {
		String addressInJurisdictionType = (String) record.get(DetailRecordFields.ADDRESS_IN_JURISDICTION_TYPE);
		if (addressInJurisdictionType != null) {
			return "add Address In Jurisdiction Type";
		} else {
			return "|";
		}
	}

	public String writePOASubmittedForAddressInJuriscition(JSONObject record) {
		String poaSubmittedForAddressInJurisdiction = (String) record
				.get(DetailRecordFields.POA_SUBMITTED_FOR_JURISDICTION_ADDRESS);
		if (poaSubmittedForAddressInJurisdiction != null) {
			return poaSubmittedForAddressInJurisdiction.concat("|");
		} else {
			return "|";
		}
	}

	public String writeNumberOfIdentityDetails(JSONObject record) {
		JSONArray identity = (JSONArray) record.get(DetailRecordFields.IDENTITY);
		if (identity != null) {
			return String.valueOf(new DecimalFormat("00").format(identity.size())).concat("|");
		} else {
			return "0".concat("|");
		}
	}

	public String writeNumberOfRelatedPerson(JSONObject record) {
		JSONArray relatedPerson = (JSONArray) record.get(DetailRecordFields.RELATED_PERSON);
		if (relatedPerson != null) {
			return String.valueOf(new DecimalFormat("00").format(relatedPerson.size())).concat("|");
		} else {
			return "0".concat("|");
		}
	}

	public String writeNumberOfDocumentDetails(JSONObject record) {
		JSONArray documents = (JSONArray) record.get(DetailRecordFields.DOCUMENTS);
		if (documents != null) {
			return String.valueOf(new DecimalFormat("00").format(documents.size())).concat("|");
		} else {
			return "0".concat("|");
		}
	}

	public String writeNumberOfControllingPersonResidentOutsideIndia(JSONObject record) {
		JSONArray numberOfControllingPersonResidentOutsideIndia = (JSONArray) record
				.get(DetailRecordFields.CONTROLLING_PERSON);
		if (numberOfControllingPersonResidentOutsideIndia != null) {
			return String.valueOf(new DecimalFormat("00").format(numberOfControllingPersonResidentOutsideIndia.size()))
					.concat("|");
		} else {
			return "0".concat("|");
		}
	}

	public String writeNumberOfLocalAddress(JSONObject record) {
		JSONArray numberOfLocalAddress = (JSONArray) record.get(DetailRecordFields.LOCAL_ADDRESS);
		if (numberOfLocalAddress != null) {
			return String.valueOf(new DecimalFormat("00").format(numberOfLocalAddress.size())).concat("|");
		} else {
			return "0".concat("|");
		}
	}

	public String writeErrorCode(JSONObject record) {
		return "|";
	}

	public String writeFiller1(JSONObject record) {
		return "|";
	}

	public String writeFiller2(JSONObject record) {
		return "|";
	}

	public String writeFiller3(JSONObject record) {
		return "|";
	}

	public String writeFiller4(JSONObject record) {
		return "|";
	}

}
