package com.capiot.ckyc.bulkfile.service;


import org.json.simple.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import com.capiot.ckyc.bulkfile.constant.ImageDetailsFields;
import com.capiot.ckyc.bulkfile.constant.ImageDetailsValues;
import com.capiot.ckyc.bulkfile.exception.InvalidValueException;

@Component
public class ImageDetailsService {
	
	private final Logger logger = LoggerFactory.getLogger(ImageDetailsService.class);
	
	public String writeRecordType() {
		return ImageDetailsValues.RECORD_TYPE.concat("|");
	}
	
	
	public String writeLineNumber(int lineNumber) {
		return String.valueOf(lineNumber).concat("|");
	}
	
	
	public String writeFileName(JSONObject singleImageDetailRecord) throws InvalidValueException {
		JSONObject file = (JSONObject) singleImageDetailRecord.get(ImageDetailsFields.ATTACHMENT);
		String fileName = null;
		if(file!=null) {
			JSONObject metadata = (JSONObject) file.get(ImageDetailsFields.METADATA);
			if(metadata!=null) {
					fileName = (String) metadata.get(ImageDetailsFields.FILE_NAME);
					return fileName.concat("|");
			}else {
				logger.info("Image name in folder field is empty");
				throw new InvalidValueException("Image name in folder field is empty");
			}
		}else {
//			logger.info("Image name in folder field is empty");
//			throw new InvalidValueException("Image name in folder field is empty");
			return "|";
		}
	}
	
	
	public String writeImageType(JSONObject singleImageDetailRecord) throws InvalidValueException {
		JSONObject imageType = (JSONObject) singleImageDetailRecord.get(ImageDetailsFields.FILE_TYPE);
		if(imageType!=null) {
			String code = (String) imageType.get(ImageDetailsFields.FILE_TYPE_CODE);
			return code.concat("|");
		}else {
//			logger.info("Image Type field is empty");
//			throw new InvalidValueException("Image Type field is empty");
			return "|";
		}
	}
	
	
	public String writeGlobalOrLocal(JSONObject singleImageDetailRecord) throws InvalidValueException {
		String globalOrLocal = (String) singleImageDetailRecord.get(ImageDetailsFields.GLOBAL_OR_LOCAL);
		if(globalOrLocal!=null) {
			if(globalOrLocal.equalsIgnoreCase(ImageDetailsValues.GLOBAL)) {
				return ImageDetailsValues.GLOBAL_VALUE.concat("|");
			}else {
				return ImageDetailsValues.LOCAL_VALUE.concat("|");
			}
		}else {
//			logger.info("Flag for Global or Local Image field is Empty");
//			throw new InvalidValueException("Flag for Global or Local Image field is Empty");
			return "|";
		}
	}
	
	
	public String writeBranchCode(JSONObject singleImageDetailRecord) throws InvalidValueException {
		String branchCode = (String) singleImageDetailRecord.get(ImageDetailsFields.BRANCH_CODE);
		if(branchCode!=null) {
			return branchCode.concat("|");
		}else {
//			logger.info("Branch code field is empty");
//			throw new InvalidValueException("Branch code field is empty");
			return "|";
		}
	}
	
	
	public String writeFiller1(JSONObject singleImageDetailRecord) {
		String filler1 = (String) singleImageDetailRecord.get(ImageDetailsFields.FILLER_ONE);
		if(filler1!=null) {
			return filler1.concat("|");
		}else {
			return "|";
		}
	}
	
	
	public String writeFiller2(JSONObject singleImageDetailRecord) {
		String filler2 = (String) singleImageDetailRecord.get(ImageDetailsFields.FILLER_TWO);
		if(filler2!=null) {
			return filler2.concat("|");
		}else {
			return "|";
		}
	}
	
	
	public String writeFiller3(JSONObject singleImageDetailRecord) {
		String filler3 = (String) singleImageDetailRecord.get(ImageDetailsFields.FILLER_THREE);
		if(filler3!=null) {
			return filler3.concat("|");
		}else {
			return "|";
		}
	}


	public String writeNewLine() {
		return "\r\n";
	}


	public void downloadDocuments(JSONObject singleImageDetailRecord) {
		JSONObject singleDocument = (JSONObject) singleImageDetailRecord.get(ImageDetailsFields.ATTACHMENT);
		if(singleDocument!=null) {
			
		}
	}
	
	
	
	
}
