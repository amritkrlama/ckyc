package com.capiot.ckyc.bulkfile.service;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import org.json.simple.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import com.capiot.ckyc.bulkfile.constant.RelatedPersonFields;
import com.capiot.ckyc.bulkfile.constant.RelatedPersonValues;
import com.capiot.ckyc.bulkfile.exception.InvalidValueException;

@Component
public class RelatedPersonService {
	
	private final Logger logger = LoggerFactory.getLogger(RelatedPersonService.class);
	
	public String writeRecordType(JSONObject record) {
		return RelatedPersonValues.RECORD_TYPE.concat("|");
	}
	
	
	public String writeLineNumber(int lineNumber) {
		return String.valueOf(lineNumber).concat("|");
	}
	
	
	public String writeTypeOfRelationship(JSONObject record) {
		JSONObject typeOfRelationship = (JSONObject) record.get(RelatedPersonFields.TYPE_OF_RELATIONSHIP);
		if(typeOfRelationship!=null) {
			String code = (String) typeOfRelationship.get(RelatedPersonFields.TYPE_OF_RELATIONSHIP_CODE);
			if(code!=null) {
				return code.concat("|");
			}else {
				logger.warn("Type of relationship for related person is missing");
				return "|";
			}
		}else {
			logger.warn("Type of relationship for related person is missing");
			return "|";
		}
	}
	
	
	public String writeAdditionDeletionOfRelatedPerson(JSONObject record) {
		String additionOrDeletion = (String) record.get(RelatedPersonFields.ADDITION_DELETION);
		if(additionOrDeletion!=null) {
			if(additionOrDeletion.equalsIgnoreCase(RelatedPersonValues.ADDITION)) {
				return RelatedPersonValues.ADDITION_VALUE.concat("|");
			}else {
				return RelatedPersonValues.DELETION_VALUE.concat("|");
			}
		}else {
			logger.warn("Addition Deletion for related person is missing");
			return "|";
		}
	}
	
	
	public String writeKycNumber(JSONObject record) {
		String kycNumber = (String) record.get(RelatedPersonFields.KYC_NUMBER);
		if(kycNumber!=null) {
			return kycNumber.concat("|");
		}else {
			logger.warn("KYC Number for related person is missing");
			return "|";
		}
	}
	
	
	public String writeName(JSONObject record) {
		String result="";
		JSONObject name = (JSONObject) record.get(RelatedPersonFields.NAME);
		if(name!=null) {
					String prefix = (String) name.get(RelatedPersonFields.NAME_PREFIX);
					if(prefix!=null) {
						result=result.concat(prefix).concat("|");
					}else {
						logger.warn("Prefix is missing in Applicant name for related person");
						result=result.concat("|");
					}
					String firstName = (String) name.get(RelatedPersonFields.FIRST_NAME);
					if(firstName!=null) {
						result=result.concat(firstName).concat("|");
					}else {
						logger.warn("First Name is missing in Applicant name for related person");
						result=result.concat("|");
					}
					
					String middleName = (String) name.get(RelatedPersonFields.MIDDLE_NAME);
					if(middleName!=null) {
						result=result.concat(middleName).concat("|");
					}else {
						logger.warn("Middle name is missing in Applicant name for related person");
						result=result.concat("|"); 
					}
					
					
					String lastName = (String) name.get(RelatedPersonFields.LAST_NAME);
					if(lastName!=null) {
						result=result.concat(lastName).concat("|");
					}else {
						logger.warn("Last name is missing in Applicant name for related person");
						result=result.concat("|");
					}
		}
		
		return result;
	}
	
	
	public String writePANCard(JSONObject record) {
		String panCard = (String) record.get(RelatedPersonFields.PAN_CARD);
		if(panCard!=null) {
			return panCard.concat("|");
		}else {
			return "|";
		}
	}
	
	
	public String writeUidAadhar(JSONObject record) {
		String uidAadhar = (String) record.get(RelatedPersonFields.UID_AADHAR);
		if(uidAadhar!=null) {
			return uidAadhar.concat("|");
		}else {
			return "|";
		}
	}
	
	
	public String writeVoterIDCard(JSONObject record) {
		String voterIDCard = (String) record.get(RelatedPersonFields.VOTER_ID_CARD);
		if(voterIDCard!=null) {
			return voterIDCard.concat("|");
		}else {
			return "|";
		}
	}
	
	
	public String writeNREGAJobCard(JSONObject record) {
		String nregaJobCard = (String) record.get(RelatedPersonFields.NREGA_JOB_CARD);
		if(nregaJobCard!=null) {
			return nregaJobCard.concat("|");
		}else {
			return "|";
		}
	}
	
	
	public String writePassportNumber(JSONObject record) {
		String passportNumber = (String) record.get(RelatedPersonFields.PASSPORT_NUMBER);
		if(passportNumber!=null) {
			return passportNumber.concat("|");
		}else {
			return "|";
		}
	}
	
	public String writePassportExpiryDate(JSONObject record) {
		String expiryDate = (String) record.get(RelatedPersonFields.PASSPORT_EXPIRY_DATE);
		if(expiryDate!=null) {
			Date expiryDateAsDate=null;
			try {
				expiryDateAsDate = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.sssX").parse(expiryDate);
			} catch (ParseException e) {
				logger.warn("Error in parsing Passport Expiry Date");
				e.printStackTrace();
			}		
			SimpleDateFormat formatter = new SimpleDateFormat("dd-MM-yyyy");
			String strExpiryDate = formatter.format(expiryDateAsDate);
			return strExpiryDate.concat("|");
		}else {
			return "|";
		}
	}
	
	
	public String writeDrivingLicenseNumber(JSONObject record) {
		String drivingLicenseNumber = (String) record.get(RelatedPersonFields.DRIVING_LICENSE_NUMBER);
		if(drivingLicenseNumber!=null) {
			return drivingLicenseNumber.concat("|");
		}else {
			return "|";
		}
	}
	
	
	public String writeDrivingLicenseExpiryDate(JSONObject record){
		String drivingLicenseExpiryDate = (String) record.get(RelatedPersonFields.DRIVING_LICENSE_EXPIRY_DATE);
		if(drivingLicenseExpiryDate!=null) {
			Date dlExpiryDate=null;
			try {
				dlExpiryDate = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.sssX").parse(drivingLicenseExpiryDate);
			} catch (ParseException e) {
				logger.warn("Error in parsing Driving License Expiry Date");
				e.printStackTrace();
			}		
			SimpleDateFormat formatter = new SimpleDateFormat("dd-MM-yyyy");
			String strExpiryDate = formatter.format(dlExpiryDate);
			return strExpiryDate.concat("|");
		}else {
			return "|";
		}
	}
	
	
	public String writeIDNameOther(JSONObject record) {
		String otherIDName = (String) record.get(RelatedPersonFields.ID_NAME_OTHER);
		if(otherIDName!=null) {
			return otherIDName.concat("|");
		}else {
			return "|";
		}
	}
	
	
	public String writeIDNumberOther(JSONObject record) {
		String otherIDnumber = (String) record.get(RelatedPersonFields.ID_NUMBER_OTHER);
		if(otherIDnumber!=null) {
			return otherIDnumber.concat("|");
		}else {
			return "|";
		}
	}
	
	
	public String writeDocumentTypeCode(JSONObject record) {
		String documentTypeCode = (String) record.get(RelatedPersonFields.DOCUMENT_TYPE_CODE);
		if(documentTypeCode!=null) {
			return documentTypeCode.concat("|");
		}else {
			return "|";
		}
	}
	
	
	public String writeDocumentIdentificationNumber(JSONObject record) {
		String documentIdentificationNumber = (String) record.get(RelatedPersonFields.DOCUMENT_IDENTIFICATION_NUMBER);
		if(documentIdentificationNumber!=null) {
			return documentIdentificationNumber.concat("|");
		}else {
			return "|";
		}
	}
	
	
	public String writeKycDetails(JSONObject record) {
		StringBuilder result = new StringBuilder();
		JSONObject kycVerification = (JSONObject) record.get(RelatedPersonFields.KYC_VERIFICATION);
		if(kycVerification!=null) {
			JSONObject applicantDeclaration = (JSONObject) kycVerification.get(RelatedPersonFields.APPLICANT_DECLARATION);
			if(applicantDeclaration!=null) {
				String dateOfDeclaration = (String) applicantDeclaration.get(RelatedPersonFields.DATE_OF_DECLARATION);
				if(dateOfDeclaration!=null) {
					result.append(dateOfDeclaration);
				}else {
					logger.warn("Date of Declaration is missing in KYC for related person");
					result.append("|");
				}
				String placeOfDeclaration = (String) applicantDeclaration.get(RelatedPersonFields.PLACE_OF_DECLARATION);
				if(placeOfDeclaration!=null) {
					result.append(placeOfDeclaration);
				}else {
					logger.warn("Place of declaration is missing in KYC for related person");
					result.append("|");
				}
			}else {
				result.append("|").append("|");
			}
			
			String kycVerificationDate = (String) kycVerification.get(RelatedPersonFields.KYC_VERIFICATION_DATE);
			if(kycVerificationDate!=null) {
				result.append(kycVerificationDate);
			}else {
				logger.warn("KYC Verification date is missing for related person");
				result.append("|");
			}
			
			String typeOfDocumentSubmitted = (String) kycVerification.get(RelatedPersonFields.TYPE_OF_DOCUMENTS);
			if(typeOfDocumentSubmitted!=null && typeOfDocumentSubmitted.equalsIgnoreCase("Certified Copies")) {
				result.append(typeOfDocumentSubmitted);
			}else {
				logger.warn("Type of document submitted is missing in KYC for related person");
				result.append("|");
			}
			
			JSONObject employeeDetails = (JSONObject) kycVerification.get(RelatedPersonFields.KYC_VERIFICATION_CARRIED_OUT_BY);
			if(employeeDetails!=null) {
				String empName = (String) employeeDetails.get(RelatedPersonFields.KYC_VERIFICATION_EMPLOYEE_NAME);
				if(empName!=null) {
					result.append(empName);
				}else {
					logger.warn("Employee Name is missing in KYC for related person");
					result.append("|");
				}
				
				String empDesignation = (String) employeeDetails.get(RelatedPersonFields.KYC_VERIFICATION_EMPLOYEE_DESIGNATION);
				if(empDesignation!=null) {
					result.append(empDesignation);
				}else {
					logger.warn("Employee Designation is missing in KYC for related person");
					result.append("|");
				}
				
				String empBranch = (String) employeeDetails.get(RelatedPersonFields.KYC_VERIFICATION_EMPLOYEE_BRANCH);
				if(empBranch!=null) {
					result.append(empBranch);
				}else {
					logger.warn("Employee Branch is missing in KYC for related person");
					result.append("|");
				}
				
				String empCode = (String) employeeDetails.get(RelatedPersonFields.KYC_VERIFICATION_EMPLOYEE_CODE);
				if(empCode!=null) {
					result.append(empCode);
				}else {
					logger.warn("Employee code is missing in KYC for related person");
					result.append("|");
				}
			}else {
				logger.warn("Employee details are missing in KYC for related person");
				result.append("|").append("|").append("|").append("|");
			}
			
			JSONObject institutionDetails = (JSONObject) kycVerification.get(RelatedPersonFields.INSTITUTION_DETAILS);
			if(institutionDetails!=null) {
				String instName = (String) institutionDetails.get(RelatedPersonFields.INSTITUTION_DETAILS_NAME);
				if(instName!=null) {
					result.append(instName);
				}else {
					logger.warn("Institution name is missing in KYC for related person");
					result.append("|");
				}
				
				String instCode = (String) institutionDetails.get(RelatedPersonFields.INSTITUTION_DETAILS_CODE);
				if(instCode!=null) {
					result.append(instCode);
				}else {
					logger.warn("Institution code is missing in KYC for related person");
					result.append("|");
				}
			}else {
				logger.warn("Institution details are missing in KYC for related person");
				result.append("|").append("|");
			}
		}else {
			logger.warn("KYC Details are missing in related person");
			result.append("|").append("|").append("|").append("|").append("|").append("|").append("|")
			.append("|").append("|").append("|");
		}
		return result.toString();
	}
	
	public String writeFiller1(JSONObject record) {
		return "|";
	}
	
	public String writeFiller2(JSONObject record) {
		return "|";
	}
	
	public String writeFiller3(JSONObject record) {
		return "|";
	}
	
	public String writeFiller4(JSONObject record) {
		return "|";
	}
	
	public String writeNewLine() {
		return "\r\n";
	}
	
	
	
	/*For Legal entities*/
	public String writeRelatedPersonMaidenName(JSONObject record) {
		StringBuilder result = new StringBuilder();
		JSONObject maidenName =(JSONObject) record.get(RelatedPersonFields.MAIDEN_NAME);
		if(maidenName!=null) {
			String prefix = (String) maidenName.get(RelatedPersonFields.NAME_PREFIX);
			if(prefix!=null) {
				result.append(prefix).append("|");
			}else {
				logger.warn("Prefix is missing in Applicant maiden name for related person");
				result.append("|");
			}
			
			String firstName = (String) maidenName.get(RelatedPersonFields.FIRST_NAME);
			if(firstName!=null) {
				result.append(firstName).append("|");
			}else {
				logger.warn("Firstname is missing in Applicant maiden name for related person");
				result.append("|");
			}
			
			String middleName = (String) maidenName.get(RelatedPersonFields.MIDDLE_NAME);
			if(middleName!=null) {
				result.append(middleName).append("|");
			}else {
				logger.warn("Middle Name is missing in Applicant maiden name for related person");
				result.append("|");
			}
			
			String lastName = (String) maidenName.get(RelatedPersonFields.LAST_NAME);
			if(lastName!=null) {
				result.append(lastName).append("|");
			}else {
				logger.warn("Last name is missing in Applicant maiden name for related person");
				result.append("|");
			}
		}else {
			logger.warn("Applicant maiden name is missing for related person");
			result.append("|".concat("|").concat("|").concat("|")) ;
		}
		
		return result.toString();
	}
	
	
	public String writeFlagIndicationRelatedPersonFatherOrSpouse(JSONObject record) {
		StringBuilder result = new StringBuilder();
		String fatherOrSpouse = (String) record.get(RelatedPersonFields.FATHER_OR_SPOUSE_FLAG);
		if(fatherOrSpouse!=null) {
			if(fatherOrSpouse.equalsIgnoreCase("Father")) {
				result.append("01").append("|");
			}else {
				result.append("02").append("|");
			}
		}else {
			logger.warn("Flag indicating father or spouse is missing in related person");
			result.append("|");
		}
		return result.toString();
	}
	
	
	
	public String writeRelatedPersonFatherOrSpouseName(JSONObject record) {
		StringBuilder result = new StringBuilder();
		JSONObject fatherOrSpouseName =(JSONObject) record.get(RelatedPersonFields.FATHER_OR_SPOUSE_NAME);
		if(fatherOrSpouseName!=null) {
			String prefix = (String) fatherOrSpouseName.get(RelatedPersonFields.NAME_PREFIX);
			if(prefix!=null) {
				result.append(prefix).append("|");
			}else {
				logger.warn("Prefix is missing in Father/Spouse name for related person");
				result.append("|");
			}
			
			String firstName = (String) fatherOrSpouseName.get(RelatedPersonFields.FIRST_NAME);
			if(firstName!=null) {
				result.append(firstName).append("|");
			}else {
				logger.warn("First name is missing in Father/Spouse name for related person");
				result.append("|");
			}
			
			String middleName = (String) fatherOrSpouseName.get(RelatedPersonFields.MIDDLE_NAME);
			if(middleName!=null) {
				result.append(middleName).append("|");
			}else {
				logger.warn("Middle name is missing in Father/Spouse name for related person");
				result.append("|");
			}
			
			String lastName = (String) fatherOrSpouseName.get(RelatedPersonFields.LAST_NAME);
			if(lastName!=null) {
				result.append(lastName).append("|");
			}else {
				logger.warn("Last name is missing in Father/Spouse name for related person");
				result.append("|");
			}
		}else {
			logger.warn("Father/Spouse name are missing for related person");
			result.append("|".concat("|").concat("|").concat("|")) ;
		}
		
		return result.toString();
	}
	
	
	public String writeRelatedPersonMotherName(JSONObject record) {
		StringBuilder result = new StringBuilder();
		JSONObject motherName =(JSONObject) record.get(RelatedPersonFields.MOTHER_NAME);
		if(motherName!=null) {
			String prefix = (String) motherName.get(RelatedPersonFields.NAME_PREFIX);
			if(prefix!=null) {
				result.append(prefix).append("|");
			}else {
				logger.warn("Prefix is missing in Mother name for related person");
				result.append("|");
			}
			
			String firstName = (String) motherName.get(RelatedPersonFields.FIRST_NAME);
			if(firstName!=null) {
				result.append(firstName).append("|");
			}else {
				logger.warn("First name is missing in Mother name for related person");
				result.append("|");
			}
			
			String middleName = (String) motherName.get(RelatedPersonFields.MIDDLE_NAME);
			if(middleName!=null) {
				result.append(middleName).append("|");
			}else {
				logger.warn("Middle name is missing in Mother name for related person");
				result.append("|");
			}
			
			String lastName = (String) motherName.get(RelatedPersonFields.LAST_NAME);
			if(lastName!=null) {
				result.append(lastName).append("|");
			}else {
				logger.warn("Last name is missing in Mother name for related person");
				result.append("|");
			}
		}else {
			logger.warn("Mother name detials are missing for related person");
			result.append("|".concat("|").concat("|").concat("|")) ;
		}
		
		return result.toString();
	}
	
	
	public String writeRelatedPersonDateOfBirth(JSONObject record) throws InvalidValueException {
		StringBuilder result = new StringBuilder();
		String dateOfBirth = (String) record.get(RelatedPersonFields.DATE_OF_BIRTH);
		if(dateOfBirth!=null) {
			Date dob = null;
			try {
				dob = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.sssX").parse(dateOfBirth);
				SimpleDateFormat formatter = new SimpleDateFormat("dd-MM-yyyy");
				String strDob = formatter.format(dob);
				result.append(strDob).append("|");
			} catch (ParseException exception) {
				logger.warn("Invalid value in Date of Birth Field");
				throw new InvalidValueException("Invalid value in Date of Birth Field for Related Person");
			}
		} else {
			logger.warn("Date of birth is missing for related person");
			result.append("|");
		}
		return result.toString();
	}
	
	
	
	public String writeGender(JSONObject record) {
		String gender = (String) record.get(RelatedPersonFields.GENDER);
		if(gender!=null) {
			switch (gender)
			{
			case "Male": return "M".concat("|");
			case "Female" : return "F".concat("|"); 
			case "Transgender": return "T".concat("|");
			case "Others" : return "O".concat("|");
			}
		}
		return "|";
	}
	
	
	public String writeMaritalStatus(JSONObject record) {
		String maritalStatus = (String) record.get(RelatedPersonFields.MARITAL_STATUS);
		if(maritalStatus!=null) {
			switch(maritalStatus) {
			case "Married": return "01".concat("|");
			case "Unmarried": return "02".concat("|");
			}
		}
		return "|";
	}
	
	
	public String writeNationality(JSONObject record) {
		JSONObject nationality = (JSONObject) record.get(RelatedPersonFields.NATIONALITY);
		if(nationality!=null) {
			return (String) nationality.get(RelatedPersonFields.COUNTRY_CODE);
		}
		return "|";
	}
	
	
	public String writeResidentialStatus(JSONObject record) {
		String residentialStatus = (String) record.get(RelatedPersonFields.RESIDENTIAL_STATUS);
		if(residentialStatus!=null) {
			switch(residentialStatus) {
			case "Resident Individual": return "01".concat("|");
			case "Non Resident Indian": return "02".concat("|");
			case "Foreign National": return "03".concat("|");
			case "Person of Indian Origin": return "04".concat("|");
			}
		}
		return "|";
	}
	
	
	public String writeOccupationType(JSONObject record) {
		JSONObject occupation = (JSONObject) record.get(RelatedPersonFields.OCCUPATION_TYPE);
		if(occupation!=null) {
			String code = (String) occupation.get(RelatedPersonFields.OCCUPATION_CODE);
			return code;
		}
		return "|";
	}
	
	
	public String writeFlagIndicatingResidentForTax(JSONObject record) {
		boolean flag = (boolean) record.get(RelatedPersonFields.FLAG_INDICATING_RELATED_PERSON_RESIDENT_FOR_TAX_PURPOSE);
		if(flag) {
			return "01".concat("|");
		}else {
			return "02".concat("|");
		}
	}
	
	public String writeJurisdictionOfResidence(JSONObject record) {
		StringBuilder result = new StringBuilder();
		JSONObject jurisdictionOfResidence = (JSONObject) record.get(RelatedPersonFields.JURISDICTION_OF_RESIDENCE);
		if(jurisdictionOfResidence!=null) {
			JSONObject jurisdiction = (JSONObject) jurisdictionOfResidence.get(RelatedPersonFields.JURISDICTION_COUNTRY);
			if(jurisdiction!=null) {
				String countryCode = (String) jurisdiction.get(RelatedPersonFields.COUNTRY_CODE);
				if(countryCode!=null) {
					result.append(countryCode).append("|");
				}else {
					logger.warn("Country code is missing in Jurisdiction address for related person");
					result.append("|");
				}
			}else {
				logger.warn("Country is missing in Jurisdiction address for related person");
				result.append("|");
			}
			
			String taxId = (String) jurisdictionOfResidence.get(RelatedPersonFields.JURISDICTION_TAXID);
			if(taxId!=null) {
				result.append(taxId).append("|");
			}else {
				logger.warn("Tax ID is missing in Jurisdiction address for related person");
				result.append("|");
			}
			
			JSONObject countryOfBirth = (JSONObject) jurisdictionOfResidence.get(RelatedPersonFields.COUNTRY_OF_BIRTH);
			if(countryOfBirth!=null) {
				String countryCode = (String) countryOfBirth.get(RelatedPersonFields.COUNTRY_CODE);
				if(countryCode!=null) {
					result.append(countryCode).append("|");
				}else {
					logger.warn("Country code is missing in Country of Birth Jurisdiction address for related person");
					result.append("|");
				}
			}else {
				logger.warn("Country of birth is missing in Jurisdiction address for related person");
				result.append("|");
			}
			
			String cityPlaceOfBirth = (String) jurisdictionOfResidence.get(RelatedPersonFields.JURISDICTION_CITY_OF_BIRTH);
			if(cityPlaceOfBirth!=null) {
				result.append(cityPlaceOfBirth).append("|");
			}else {
				logger.warn("City place of birth is missing in Jurisdiction address for related person");
				result.append("|");
			}
		}else {
			logger.warn("Jurisdiction address is missing for related person");
			result.append("|").append("|").append("|").append("|");
		}
		return result.toString();
	}


	
	public String writeAddress(JSONObject record) {
		StringBuilder result = new StringBuilder();
		JSONObject address = (JSONObject) record.get(RelatedPersonFields.LOCAL_ADDRESS);
		if(address!=null) {
			String addressType = (String) address.get(RelatedPersonFields.ADDRESS_TYPE);
			if(addressType!=null) {
				switch(addressType) {
				case "Resident/Business" : result.append("01").append("|"); break;
				case "Residential" : result.append("02").append("|"); break;
				case "Business": result.append("03").append("|"); break;
				case "Registered Office": result.append("04").append("|"); break;
				case "Unspecified": result.append("05").append("|"); break;
				default : result.append("|"); break;
				}
			}else {
				logger.warn("Address type is missing in Address for related person");
				result.append("|");
			}
			
			String line1 = (String) address.get(RelatedPersonFields.LINE1);
			if(line1!=null) {
				result.append(line1).append("|");
			}else {
				logger.warn("Line 1 is missing in Address for related person");
				result.append("|");
			}
			
			String line2 = (String) address.get(RelatedPersonFields.LINE2);
			if(line2!=null) {
				result.append(line2).append("|");
			}else {
				logger.warn("Line 2 is missing in Address for related person");
				result.append("|");
			}
			
			String line3 = (String) address.get(RelatedPersonFields.LINE3);
			if(line3!=null) {
				result.append(line3).append("|");
			}else {
				logger.warn("Line 3 is missing in Address for related person");
				result.append("|");
			}
			
			String city = (String) address.get(RelatedPersonFields.ADDRESS_CITY_TOWN_VILLAGE);
			if(city!=null) {
				result.append(city).append("|");
			}else {
				logger.warn("City is missing in Address for related person");
				result.append("|");
			}
			
			String pinCode = (String) address.get(RelatedPersonFields.ADDRESS_PINCODE);
			if(pinCode!=null) {
				result.append(pinCode).append("|");
			}else {
				logger.warn("Pincode is missing in Address for related person");
				result.append("|");
			}
			
			JSONObject state = (JSONObject) address.get(RelatedPersonFields.ADDRESS_STATE);
			if(state!=null) {
				String stateCode = (String) state.get(RelatedPersonFields.STATE_CODE);
				if(stateCode!=null) {
					result.append(stateCode).append("|");
				}else {
					logger.warn("State code is missing in Address for related person");
					result.append("|");
				}
			}else {
				logger.warn("State is missing in Address for related person");
				result.append("|");
			}
			
			JSONObject country = (JSONObject) address.get(RelatedPersonFields.ADDRESS_COUNTRY);
			if(country!=null) {
				String countryCode = (String) country.get(RelatedPersonFields.COUNTRY_CODE);
				if(countryCode!=null) {
					result.append(countryCode).append("|");
				}else {
					logger.warn("Country code is missing in Address for related person");
					result.append("|");
				}
			}else {
				logger.warn("Country is missing in Address for related person");
				result.append("|");
			}
			
			JSONObject proofOfAddress = (JSONObject) address.get(RelatedPersonFields.PROOF_OF_ADDRESS);
			if(proofOfAddress!=null) {
				String code = (String) proofOfAddress.get(RelatedPersonFields.TYPE_OF_RELATIONSHIP_CODE);
				if(code!=null) {
					result.append(code).append("|");
				}else {
					logger.warn("Code for proof of address is missing in Address for related person");
					result.append("|");
				}
			}else {
				logger.warn("Proof of Address is missing in Address for related person");
				result.append("|");
			}
			
			String proofOfAddressOthers = (String) address.get(RelatedPersonFields.PROOF_OF_ADDRESS_OTHERS);
			if(proofOfAddressOthers!=null) {
				result.append(proofOfAddressOthers).append("|");
			}else {
				logger.warn("Proof of address others is missing in Address for related person");
				result.append("|");
			}
		}else {
			logger.warn("Address is missing for related person");
			result.append("|").append("|").append("|").append("|").append("|").append("|")
			.append("|").append("|").append("|").append("|");
		}
		return result.toString();
	}

}
