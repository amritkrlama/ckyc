package com.capiot.ckyc.bulkfile.support;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;

import org.apache.tomcat.util.http.fileupload.FileUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;



@Component
public class ZipFolderService {
	
	private static final Logger logger = LoggerFactory.getLogger(ZipFolderService.class);
	List<String> fileList;
	
	
	
	public void zipFolder(String folderToZipPath, String zippedFilePath) {	
		fileList = new ArrayList<>();
		generateFileList(folderToZipPath, new File(folderToZipPath));
		zipIt(folderToZipPath, zippedFilePath);
		//deleteFileOrFolder(folderToZipPath);
		
	}
	
	
	
	/*Traverses a directory and get all the files*/
	 public void generateFileList(String folderToZipPath, File node){

	    	//add file only
		if(node.isFile()){
			fileList.add(generateZipEntry(folderToZipPath, node.getAbsoluteFile().toString()));
		}
			
		if(node.isDirectory()){
			String[] subNode = node.list();
			for(String filename : subNode){
				generateFileList(folderToZipPath, new File(node, filename));
			}
		}
	}
	  
	  
	  /**
	     * Format the file path for zip
	     * @param file file path
	     * @return Formatted file path
	     */
	    private String generateZipEntry(String folderToZipPath, String file){
	    	return file.substring(folderToZipPath.length()+1, file.length());
	    }
	


	public void zipIt(String folderToZipPath, String zippedFilePath) {
		
		byte[] buffer = new byte[1024];
		String baseFolder = new File(folderToZipPath).getName();
		
		try(ZipOutputStream zipOutputStream = new ZipOutputStream(new FileOutputStream(createZipFile(zippedFilePath)))) {
			for(String file:this.fileList) {	
				ZipEntry zipEntry = new ZipEntry(baseFolder+File.separator+file);
				zipOutputStream.putNextEntry(zipEntry);
				
				try(FileInputStream fileInputStream = new FileInputStream(folderToZipPath+File.separator+file)){
					logger.info("Reading file: "+file);
					int length;
					while((length = fileInputStream.read(buffer))>0) {
						zipOutputStream.write(buffer,0,length);
					}
					logger.info("File added: "+file);
				}		
			}
		} catch (FileNotFoundException e) {
			logger.debug("Zipped path/file not found");
			e.printStackTrace();
		} catch (IOException e) {
			logger.debug("Error in processing the Zip file");
			e.printStackTrace();
		}
	}
	  
	
	
	public File createZipFile(String zippedFilePath) {
		File zippedFile = new File(zippedFilePath+".zip");
		return zippedFile;
	}
	
	
	/*To delete the file or folder (after zipping)*/
	public void deleteFileOrFolder(String folderToZipPath) {
		try {
			FileUtils.deleteDirectory(new File(folderToZipPath));
		} catch (IOException e) {
			logger.debug("Error in deleting the folder/file");
			e.printStackTrace();
		}
	}
	  
}
