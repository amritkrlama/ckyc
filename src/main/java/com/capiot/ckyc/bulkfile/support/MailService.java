package com.capiot.ckyc.bulkfile.support;

import java.io.File;
import java.io.IOException;

import org.apache.commons.io.FileUtils;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import org.springframework.core.env.Environment;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestClientException;
import org.springframework.web.client.RestTemplate;

@Component	
@Configuration
@PropertySource("classpath:ckyc.properties")
public class MailService {
	
	public static final Logger logger = LoggerFactory.getLogger(MailService.class);
	
	@Autowired
	Environment environment;
	@Autowired
	RestTemplate template;
	@Value("${url_for_mailing}")
	private String urlForMailing;
	
	@SuppressWarnings("unchecked")
	public ResponseEntity<JSONObject> mailBulkFile(String filepath, String filename) {
		ResponseEntity<JSONObject> exchange=null;
		try {
			byte[] fileContent = FileUtils.readFileToByteArray(new File(filepath+filename+".zip"));
			final String url =urlForMailing;
			logger.info("URL for mailing {}", url);
			HttpHeaders header = new HttpHeaders();
			JSONObject body = new JSONObject();
			String recipient = "amrit.kumar@capiot.com";
			body.put("templateID", "NOT1001");
			body.put("TransactionID", "1234");
			body.put("toEmail", recipient);
			body.put("attachmentfileName", filename);
			body.put("encodedFileData", fileContent);
			body.put("params", new JSONArray());
			header.setContentType(MediaType.APPLICATION_JSON);
			HttpEntity<JSONObject> request = new HttpEntity<>(body,header);

			try {
				logger.info("Mailing the bulk file to the recipient {}", recipient);
				exchange = template.exchange(url, HttpMethod.POST, request, JSONObject.class);
				logger.info("Mail has been successfully sent to the recipient {}", recipient);
			} catch (RestClientException exception) {
				throw new IllegalArgumentException("Error while sending the mail. Error in server handling the mailing part.", exception);
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
		return exchange;
	}
}
