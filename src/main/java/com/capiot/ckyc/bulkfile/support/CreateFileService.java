package com.capiot.ckyc.bulkfile.support;

import java.io.File;
import java.io.IOException;
import java.text.DecimalFormat;

import org.json.simple.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Component;

import com.capiot.ckyc.bulkfile.exception.PSFFileException;

@Component
@Configuration
@PropertySource("classpath:ckyc.properties")
public class CreateFileService {
	
	@Autowired
	Environment environment;

	private final Logger logger = LoggerFactory.getLogger(CreateFileService.class);
	String fileSeparator = System.getProperty("file.separator");
	public static String outerFolderName = null;
	public static String outerFolderPath = null;
	public static String innerFolderName = null;
	public static String innerFolderPath = null;
	public static String psfFileName = null;
	public static String psfFilePath = null;
	public static String triggerFilePath = null;  
	private String home = System.getProperty("user.home");
	int counter=1;
	@Value("${certificate}")
	private String certificate;
	@Value("${keystore_password}")
	private String keyStorePassword;
	@Value("${key_password}")
	private String keyPassword;

	public CreateFileService() {
		super();
	}

	/*Method to generate PSF file*/
	public File createPSF(String FICode, String branchCode, String userId, int batchNumber) throws PSFFileException {	
		String commonPath =home+environment.getProperty("commonPath");
		psfFileName = outerFolderName;	
		psfFilePath = commonPath+outerFolderName+fileSeparator+psfFileName+".txt";
		File file = null;
		try {
			file = new File(psfFilePath);
			if(file.createNewFile()) {
				logger.info("Pipe Separated File {} has been created successfully at: {}", psfFileName, psfFilePath);
			}else {
				logger.info("Pipe Separated File {} already exists at: {}", psfFileName, psfFilePath);
			}
		}catch(NullPointerException | IOException exception) {
			throw new PSFFileException("Not able to create the Pipe Separated file", exception);
		}
		
		return file;
	}
	
	
	
	/*Method to create the Outer folder*/
	public void createOuterFolder(String FICode, String branchCode, String userId, int batchNumber) {
		String commonPath = home+environment.getProperty("commonPath");
		outerFolderName = FICode+"_"+branchCode+"_"+CommonUtility.getCurrentDateTime()+"_"+userId+"_"+"U"+new DecimalFormat("00000").format(batchNumber);	
		outerFolderPath = commonPath+outerFolderName;
		File outerFolder = new File(outerFolderPath);
		if(!outerFolder.exists()) {
			if(outerFolder.mkdirs()) {
				logger.info("Outer Folder {} has been successfully created at: {}", outerFolderName, outerFolderPath);
			}else {
				logger.info("Error in creating the Outer Folder {} at: {} ", outerFolderName, outerFolderPath);
			}
		}else {
			logger.info("Outer Folder {} already exists at: {}", outerFolderName, outerFolderPath);
		}
	}
	
	
	
	/*Method to create the Inner folder*/
	public void createInnerFolders(JSONObject documentRecord) {
		innerFolderName = CommonUtility.getCurrentDateTime()+"_"+counter;
		innerFolderPath = outerFolderPath+fileSeparator+innerFolderName;
		File innerFolder = new File(innerFolderPath);
		if(!innerFolder.exists()) {
			if(innerFolder.mkdirs()) {
				logger.info("Inner folder {} has been successfully created at: {}", innerFolderName, innerFolderPath);
			}else {
				logger.info("Error in creating the inner folder {} at: {}", innerFolderName, innerFolderPath);
			}
		}else {
			logger.info("Inner folder {} already exists at: {}", innerFolderName, innerFolderPath);
		}
		counter++;
	}
	
	
	
	/*Method to generate PSF file*/
	public File createTriggerFile(String FICode, String branchCode, String userId, int batchNumber) throws PSFFileException {	
		triggerFilePath = outerFolderPath+".trg";
		File triggerFile = null;
		File signedTriggerFile = null;
		try {
			triggerFile = new File(triggerFilePath);
			if(triggerFile.createNewFile()) {
				logger.info("Trigger File has been created successfully at: {}", triggerFilePath);
				logger.info("Signing the trigger file");
			}else {
				logger.info("Trigger File already exists.");
			}
			signedTriggerFile = CommonUtility.digitalSign(triggerFile, certificate, keyStorePassword, keyPassword);
			logger.info("Trigger file has been signed successfully");
		}catch(NullPointerException | IOException exception) {
			throw new PSFFileException("Not able to create the Trigger file", exception);
		}
		
		return signedTriggerFile;
	}
	

}
