package com.capiot.ckyc.bulkfile.support;

import java.io.File;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.nio.file.Files;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.security.Signature;
import java.security.SignatureException;
import java.text.SimpleDateFormat;
import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.util.Date;
import java.util.TimeZone;

import org.apache.commons.io.FileUtils;
import org.springframework.stereotype.Component;

import com.capiot.ckyc.api.support.CommonService;

@Component
public class CommonUtility {
	
	public final static String indianTimeZone = "GMT+5:30";
	
	/*Method to return current date time */
	public static String getCurrentDateTime() {
		SimpleDateFormat dateFormat = new SimpleDateFormat("ddMMyyyyHHmmss");
		dateFormat.setTimeZone(TimeZone.getTimeZone(indianTimeZone));
		Date now = new Date();
		String currentDateTime = dateFormat.format(now);
		return currentDateTime;
	}
	
	public static String getCurrentDateTimeStamp(){
	    ZonedDateTime now = ZonedDateTime.now(ZoneId.of("Asia/Kolkata"));
	    return now.toString();
	}
	
	
	/**
	 * @param The whole request
	 * @param File Name of the keystore
	 * @return Digitally signed request
	 */
	public static File digitalSign(File file, String fileName, String keyStorePassword, String keyPassword) {
		try {
			Signature signature = Signature.getInstance("SHA256WithRSA");
			signature.initSign(CommonService.getPrivateKey(fileName, keyStorePassword, keyPassword));
			signature.update(Files.readAllBytes(file.toPath()));
			byte[] signatureBytes = signature.sign();
			FileUtils.writeByteArrayToFile(file, signatureBytes);
			return file;
		} catch (NoSuchAlgorithmException e) {
			e.printStackTrace();
		} catch (InvalidKeyException e) {
			e.printStackTrace();
		} catch (SignatureException e) {
			e.printStackTrace();
		} catch (UnsupportedEncodingException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		return null;
	}
	
}
