package com.capiot.ckyc.bulkfile.support;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardWatchEventKinds;
import java.nio.file.WatchEvent;
import java.nio.file.WatchKey;
import java.nio.file.WatchService;
import java.util.ArrayList;
import java.util.List;
import java.util.regex.Pattern;

import org.json.simple.JSONObject;
import org.json.simple.parser.ParseException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.HttpServerErrorException;
import org.springframework.web.client.RestTemplate;

import com.capiot.ckyc.bulkfile.exception.InvalidRequestException;
import com.capiot.ckyc.bulkfile.odp.TokenGenerateService;
import com.fasterxml.jackson.core.JsonProcessingException;



@Component
public class ResponseFileListener {
	
	@Autowired
	RestTemplate template;
	@Autowired
	Environment env;
	@Autowired
	TokenGenerateService loginTokenService;
	
	private static final Logger logger = LoggerFactory.getLogger(ResponseFileListener.class);
	private static final String home = System.getProperty("user.home");
	private static final String fileSeparator = System.getProperty("file.separator");
	private static final Path responseDirPath = Paths.get(home+fileSeparator+"Downloads"+fileSeparator+"CKYC"+fileSeparator+"Response");
	private static String fileName = null;
	
	
	/*Listens for any file upload*/
	public void listenToResponseFile() {
		try {
			logger.info("Started listening to Response folder");
			WatchService watcher = responseDirPath.getFileSystem().newWatchService();
			responseDirPath.register(watcher, StandardWatchEventKinds.ENTRY_CREATE);
			WatchKey watchKey = null;
			
			
			/*Infinite loop to listen*/
			while(true) {
				watchKey = watcher.take();
				List<WatchEvent<?>> events = watchKey.pollEvents();
				if(events!=null) {
					for(WatchEvent<?> event: events) {					
						fileName = event.context().toString();
						logger.info(":::Reading Response file:::"+ fileName);
						updateCKYCNumbers();
					}
				}		
				if(!watchKey.reset()) {
					logger.info("Inside break of ResponseFileListener");
					break;
				}
			}
		}catch(Exception exception) {
			logger.debug("Exception occured in Listener");
			exception.printStackTrace();
		}
	}
	
	
	
	
	/*Method to get the file extension*/
	 @SuppressWarnings("unused")
	private static String getFileExtension(String fileName) {
	        if(fileName.lastIndexOf(".") != -1 && fileName.lastIndexOf(".") != 0)
	        return fileName.substring(fileName.lastIndexOf(".")+1);
	        else return "";
	    }
	 
	 
	 
	 
	 /*Method to get the file name without extension*/
	 @SuppressWarnings("unused")
	private static String getFileNameWithoutExtension(String fileName) {
		String[] fileNameAsArray = fileName.split("\\.");
		return fileNameAsArray[0];
	 }
	 
	 
	 

	 public List<String> fetchFIReferenceNumbers() {
			
		 List<String> FIReferenceNumbers = new ArrayList<>();
			
		 try(BufferedReader bufferedReader = new BufferedReader(new FileReader(responseDirPath+fileName))){
			 String currentLine;
			 int counterToEscapeFirstLine=1;
				
			 while((currentLine = bufferedReader.readLine())!=null) {
				 if(counterToEscapeFirstLine==1) {
					 counterToEscapeFirstLine++;
					 continue;
				 }
				 String[] stringFromPipeSeparated = currentLine.split(Pattern.quote("|"));
				 if(stringFromPipeSeparated.length>0) {
					 FIReferenceNumbers.add(stringFromPipeSeparated[7]);
				 }
			 }
		 } catch (FileNotFoundException e) {
			 logger.debug("Response file not found at the mentioned location: "+responseDirPath);
			 e.printStackTrace();
		 } catch (IOException e) {
			 logger.debug("Exception while reading the Response file");
			 e.printStackTrace();
		 }
		 return FIReferenceNumbers;
	 }

		
		
		public List<String> fetchCKYCNumbers() {
			
			List<String> CKYCNumbers = new ArrayList<>();
			try(BufferedReader bufferedReader = new BufferedReader(new FileReader(responseDirPath+fileName))){
				String currentLine;
				int counterToEscapeFirstLine=1;
				
				while((currentLine = bufferedReader.readLine())!=null) {
					if(counterToEscapeFirstLine==1) {
						counterToEscapeFirstLine++;
						continue;
					}
					String[] stringFromPipeSeparated = currentLine.split(Pattern.quote("|"));
					if(stringFromPipeSeparated.length>0) {
						CKYCNumbers.add(stringFromPipeSeparated[8]);
					}
				}
			} catch (FileNotFoundException e) {
				logger.debug("Response file not found at the mentioned location: "+responseDirPath);
				e.printStackTrace();
			} catch (IOException e) {
				logger.debug("Exception while reading the Response file");
				e.printStackTrace();
			}
			
			return CKYCNumbers;
		}




		@SuppressWarnings("unchecked")
		public ResponseEntity<String> updateCKYCNumbers() throws InvalidRequestException, ParseException, JsonProcessingException {
			
			final String BUSINESS_CENTER = env.getProperty("business_center");
			final String DOMAIN = env.getProperty("domain");
			final String ENTITY = env.getProperty("entity");
			
			/* JWT Token */
			String token = "someinvalidtokentostartwith";  
			ResponseEntity<String> responseOfUpdateRequest = null;
			
			
			List<String> FIReferenceNumbers = fetchFIReferenceNumbers();
			
			for(int index=0; index<FIReferenceNumbers.size(); index++) {
				
				String url = BUSINESS_CENTER+DOMAIN+"/"+ENTITY+"/"+FIReferenceNumbers.get(index);
				HttpHeaders header = new HttpHeaders();
				header.setContentType(MediaType.APPLICATION_JSON);
				header.set("Authorization", "JWT " + token);
				logger.info("URL: " + url);
				
				START: while (responseOfUpdateRequest == null) {
					try {
						JSONObject ckycNumber = new JSONObject();
						ckycNumber.put("ckycNumber", fetchCKYCNumbers().get(index));
						HttpEntity<JSONObject> entity = new HttpEntity<JSONObject>(ckycNumber,header);
						long startTime = System.currentTimeMillis();
						responseOfUpdateRequest = template.exchange(url, HttpMethod.PUT, entity, String.class);
						long elapsedTime = (System.currentTimeMillis() - startTime);
						logger.info("Time elapsed (in milliseconds) for updating the record in ODP: "+elapsedTime);;
					} catch (HttpClientErrorException exception) {
						if (exception.getStatusText().contains("Unauthorized")) {
							token = loginTokenService.generateToken();
							header.set("Authorization", "JWT " + token);
							continue START;
						} else {
							throw new InvalidRequestException("Invalid request for updating records", exception);
						}
					}catch(HttpServerErrorException exception) {
						throw new InvalidRequestException("Invalid request for updating records",exception);
					}
				}
				
			}
			
			return responseOfUpdateRequest;
		}

	
}
