package com.capiot.ckyc.api.search.request;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

import org.springframework.stereotype.Component;

@Component
@XmlRootElement(name="PID_DATA")
@XmlAccessorType(XmlAccessType.FIELD)
public class SearchRequestPIDData {
	
	@XmlElement(name="DATE_TIME")
	String dateTime;
	@XmlElement(name="ID_TYPE")
	String idType;
	@XmlElement(name="ID_NO")
	String idNumber;
	
	public SearchRequestPIDData(String dateTime, String idType, String idNumber) {
		super();
		this.dateTime = dateTime;
		this.idType = idType;
		this.idNumber = idNumber;
	}

	public SearchRequestPIDData() {
		super();
	}

	public String getDateTime() {
		return dateTime;
	}

	public void setDateTime(String dateTime) {
		this.dateTime = dateTime;
	}

	public String getCkycNumber() {
		return idType;
	}

	public void setCkycNumber(String ckycNumber) {
		this.idType = ckycNumber;
	}

	public String getDateOfBirth() {
		return idNumber;
	}

	public void setDateOfBirth(String dateOfBirth) {
		this.idNumber = dateOfBirth;
	}

	@Override
	public String toString() {
		return "PIDData [dateTime=" + dateTime + ", idType=" + idType + ", idNumber=" + idNumber + "]";
	}
	
	
}
