package com.capiot.ckyc.api.search.response;



import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;
import org.w3c.dom.Document;

import com.capiot.ckyc.api.support.CommonService;
import com.capiot.ckyc.api.support.ResponseSecureService;
import com.capiot.ckyc.bulkfile.exception.InvalidSignatureException;

@RestController
public class SearchResponseService {
	
	public static final Logger logger =  LoggerFactory.getLogger(SearchResponseService.class);
	@Autowired
	ResponseSecureService responseSecureService;
	@Autowired
	Environment environment;
	
	@PostMapping(value="/api/searchCkyc/handle")
	public String handleResponse(@RequestBody String payload) throws Exception{
		logger.info("Response received from Cersai for Search {} ", System.lineSeparator() + payload);
		Document document = CommonService.convertStringToDocument(payload);
		boolean sign = responseSecureService.verifySign(document, environment.getProperty("cersaiPublicKey"));
		if(!sign) {
			logger.info("Unable to verify the Signature in the response from Cersai");
			throw new InvalidSignatureException("Unable to verify the Signature. Response discarded");
		}else {
			logger.info("Signature verfied");
		}
		
		SearchResponseReqRoot root = responseSecureService.unmarshalToSearchResponseReqRoot(document);
		String sessionKey = responseSecureService.getSessionKeyFromResponse(root);
		String decryptedSessionKey = responseSecureService.decryptSessionKey(sessionKey, environment.getProperty("keystoreName"), environment.getProperty("keystorePassword"), environment.getProperty("keyPassword"));
		logger.info(decryptedSessionKey);
		return "Done";
	}	
}
