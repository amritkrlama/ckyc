package com.capiot.ckyc.api.search.request;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.URI;
import java.net.URISyntaxException;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;

import org.json.simple.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import org.springframework.core.env.Environment;
import org.springframework.http.MediaType;
import org.springframework.http.RequestEntity;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;
import org.w3c.dom.Document;

import com.capiot.ckyc.api.search.response.SearchResponseReqRoot;
import com.capiot.ckyc.api.support.CommonService;
import com.capiot.ckyc.api.support.RequestSecureService;
import com.capiot.ckyc.api.support.ResponseSecureService;
import com.capiot.ckyc.bulkfile.exception.FieldMissingException;
import com.capiot.ckyc.bulkfile.exception.InvalidRequestException;

@RestController
@Configuration
@PropertySource("classpath:ckyc.properties")
public class SearchRequestService {

	@Autowired
	Environment environment;
	@Autowired
	RestTemplate template;
	@Autowired
	RequestSecureService requestSecureService;
	@Autowired
	ResponseSecureService responseSecureService;
	@Autowired
	InitializeSearchTagsService initializeSearchTagsService;

	private static final Logger logger = LoggerFactory.getLogger(SearchRequestService.class);
	public static final String ID_TYPE = "idType";
	public static final String ID_NUMBER = "idNumber";

	public static String pidAsStringXml = null;
	public static String rootAsStringXml = null;
	public static String sessionKey = null;
	public static ByteArrayOutputStream pidStream = null;
	public static ByteArrayOutputStream rootStream = null;

	public static SearchRequestReqRoot reqRoot;
	public static SearchRequestHeader header;
	public static SearchRequestPID pid;
	public static SearchRequestPIDData pidData;
	public static SearchRequestCkycInq ckycInq;

	/**
	 * @param Request received from UI Initializes all the Tags and Marshal the
	 *                Download request.
	 * @throws FieldMissingException 
	 */
	public String createSearchRequest(String payload) throws FieldMissingException {
		JSONObject payloadAsJSON = CommonService.convertRequestToJSONObject(payload);
		String idType = (String) payloadAsJSON.get(ID_TYPE);
		String idNumber = (String) payloadAsJSON.get(ID_NUMBER);
		if(idType==null) {
			throw new FieldMissingException("ID Type is missing");
		}
		if(idNumber==null) {
			throw new FieldMissingException("ID Number is missing");
		}
		
		logger.info("Recieved ID Type: {}", idType);
		logger.info("Recieved ID Number: {}", idNumber);

		sessionKey = requestSecureService.generateSessionKey();

		initializeSearchTagsService.initializePIDDataAndMarshal(idType, idNumber);
		initializeSearchTagsService.initializePID(pidData);
		initializeSearchTagsService.intializeCkycInq(requestSecureService.encryptSessionKeyUsingCersaiPublicKey(
								environment.getProperty("cersaiPublicKey"), sessionKey),
						requestSecureService.encryptPID(sessionKey, pidAsStringXml));
		initializeSearchTagsService.intializeHeader("IN106", "02", "1.0");
		initializeSearchTagsService.initializeReqRoot(header, ckycInq);

		// Marshaling and storing the requests in strings
		try {
			rootStream = new ByteArrayOutputStream();
			JAXBContext context = JAXBContext.newInstance(SearchRequestReqRoot.class);
			Marshaller marshaller = context.createMarshaller();
			marshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);
			marshaller.marshal(reqRoot, rootStream);
			rootAsStringXml = rootStream.toString("UTF-8");
		} catch (JAXBException exception) {
			logger.debug("Unable to marshal the Search Request");
			exception.printStackTrace();
		} catch (UnsupportedEncodingException e) {
			logger.debug("Unable to marshal the Search Request");
			e.printStackTrace();
		} finally {
			try {
				if (rootStream != null)
					rootStream.close();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}

		logger.info("Decrypted PID {}", responseSecureService
				.decryptPID(requestSecureService.encryptPID(sessionKey, pidAsStringXml), sessionKey));
		logger.info(requestSecureService.digitalSign(rootAsStringXml, environment.getProperty("keystoreName"),
				environment.getProperty("keystorePassword"), environment.getProperty("keyPassword")));

		return rootAsStringXml;
	}

	public SearchResponseReqRoot postToCersai(Document requestRoot) throws URISyntaxException, InvalidRequestException {
		String url = environment.getProperty("cersai_search");
		logger.info("Cersai's URL for Search CKYC is {}", url);
		RequestEntity<Document> request = RequestEntity.post(new URI(url)).contentType(MediaType.APPLICATION_XML)
				.body(requestRoot);
		ResponseEntity<SearchResponseReqRoot> exchange = template.exchange(request, SearchResponseReqRoot.class);
		if (exchange.hasBody()) {
			return exchange.getBody();
		} else {
			throw new InvalidRequestException("Response from Cersai for Search is empty");
		}
	}
	
	

	/**
	 * @param Request as XML
	 */
	public Document signRequest(Document request) {
		return requestSecureService.sign(request, environment.getProperty("keystoreName"),
				environment.getProperty("keystorePassword"), environment.getProperty("keyPassword"));
	}

}