package com.capiot.ckyc.api.search.service;

import org.springframework.stereotype.Component;

@Component
public class SearchRequest {
	
	private String type;
	
	private String searchBy;
	
	private String idType;
	
	private String idNumber;
	
	private String ckycNumber;
	
	private String firstName;
	
	private String lastName;
	
	public SearchRequest() {
		super();
	}
	
	public SearchRequest(String type, String searchBy, String idType, String idNumber, String ckycNumber,
			String firstName, String lastName) {
		super();
		this.type = type;
		this.searchBy = searchBy;
		this.idType = idType;
		this.idNumber = idNumber;
		this.ckycNumber = ckycNumber;
		this.firstName = firstName;
		this.lastName = lastName;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String getSearchBy() {
		return searchBy;
	}

	public void setSearchBy(String searchBy) {
		this.searchBy = searchBy;
	}

	public String getIdType() {
		return idType;
	}

	public void setIdType(String idType) {
		this.idType = idType;
	}

	public String getIdNumber() {
		return idNumber;
	}

	public void setIdNumber(String idNumber) {
		this.idNumber = idNumber;
	}

	public String getCkycNumber() {
		return ckycNumber;
	}

	public void setCkycNumber(String ckycNumber) {
		this.ckycNumber = ckycNumber;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}
	
	
}
