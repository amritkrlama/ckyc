package com.capiot.ckyc.api.search.service;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.ParseException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.env.Environment;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.HttpServerErrorException;
import org.springframework.web.client.RestTemplate;

import com.capiot.ckyc.bulkfile.exception.InvalidRequestException;
import com.capiot.ckyc.bulkfile.odp.RetrieveRecordService;
import com.capiot.ckyc.bulkfile.odp.TokenGenerateService;
import com.fasterxml.jackson.core.JsonProcessingException;

@Component
public class SearchInHubService {
	
	public static final Logger logger = LoggerFactory.getLogger(SearchInHubService.class);
	@Autowired
	private Environment environment;
	@Autowired
	private RestTemplate template;
	@Autowired
	private TokenGenerateService tokenGenerateService;
	@Value("${business_center}")
	private String businessCenter;
	
	@SuppressWarnings("unchecked")
	public JSONArray searchByIdentity(JSONObject requestAsJSON) throws InvalidRequestException, JsonProcessingException, ParseException {
		String idType = (String) requestAsJSON.get("idType");
		String idNumber = (String) requestAsJSON.get("idNumber");
		if(idType==null || idNumber == null)
			throw new InvalidRequestException("Id Type or Id Number is missing");
		
		JSONObject filter = new JSONObject();
		filter.put("identity.type.categoryName", idType);
		filter.put("identity.number", idNumber);
		String filterAsString = filter.toJSONString();
		
		String url = businessCenter+environment.getProperty("domain")+"/"+environment.getProperty("ckycHub")+"?filter={filterAsString}&expand=true";
		ResponseEntity<JSONArray> response = null;
		HttpHeaders header = new HttpHeaders();
		header.setContentType(MediaType.APPLICATION_JSON);
		header.set("Authorization", "JWT " + RetrieveRecordService.token);
		logger.info("URL: " + url);

		START: while (response == null) {
			try {
				HttpEntity<String> entity = new HttpEntity<>(header);
				logger.info("Searching matching records from ODP(CKYC Hub).");
				long startTime = System.currentTimeMillis();
				response = template.exchange(url, HttpMethod.GET, entity, JSONArray.class, filterAsString);
				long elapsedTime = (System.currentTimeMillis() - startTime);
				logger.info("Time elapsed (in milliseconds) for fetching all the records from ODP(CKYC Hub): "+elapsedTime);;
			} catch (HttpClientErrorException exception) {
				if (exception.getStatusText().contains("Unauthorized")) {
					logger.info("Invalid JWT token. Generating a new token from Login.");
					long startTime = System.currentTimeMillis();
					RetrieveRecordService.token = tokenGenerateService.generateToken();
					long elapsedTime = (System.currentTimeMillis() - startTime);
					header.set("Authorization", "JWT " + RetrieveRecordService.token);
					logger.info("Time elapsed (in milliseconds) for generating the JWT: "+elapsedTime);;
					continue START;
				} else {
					throw new InvalidRequestException("Invalid request for fetching records", exception);
				}
			}catch(HttpServerErrorException exception) {
				throw new InvalidRequestException("Invalid request for fetching records",exception);
			}
		}
		
		return response.getBody();
	}

	@SuppressWarnings("unchecked")
	public JSONArray searchByCKYCNumber(JSONObject requestAsJSON) throws InvalidRequestException, JsonProcessingException, ParseException {
		String ckycNumber = (String) requestAsJSON.get("ckycNumber");
		if(ckycNumber==null)
			throw new InvalidRequestException("CKYC Number is missing");
		
		JSONObject filter = new JSONObject();
		filter.put("_id", ckycNumber);
		String filterAsString = filter.toJSONString();
		
		String url = businessCenter+environment.getProperty("domain")+"/"+environment.getProperty("ckycHub")+"?filter={filterAsString}&expand=true";
		ResponseEntity<JSONArray> response = null;
		HttpHeaders header = new HttpHeaders();
		header.setContentType(MediaType.APPLICATION_JSON);
		header.set("Authorization", "JWT " + RetrieveRecordService.token);
		logger.info("URL: " + url);

		START: while (response == null) {
			try {
				HttpEntity<String> entity = new HttpEntity<>(header);
				logger.info("Searching matching records from ODP(CKYC Hub).");
				long startTime = System.currentTimeMillis();
				response = template.exchange(url, HttpMethod.GET, entity, JSONArray.class, filterAsString);
				long elapsedTime = (System.currentTimeMillis() - startTime);
				logger.info("Time elapsed (in milliseconds) for fetching all the records from ODP(CKYC Hub): "+elapsedTime);;
			} catch (HttpClientErrorException exception) {
				if (exception.getStatusText().contains("Unauthorized")) {
					logger.info("Invalid JWT token. Generating a new token from Login.");
					long startTime = System.currentTimeMillis();
					RetrieveRecordService.token = tokenGenerateService.generateToken();
					long elapsedTime = (System.currentTimeMillis() - startTime);
					header.set("Authorization", "JWT " + RetrieveRecordService.token);
					logger.info("Time elapsed (in milliseconds) for generating the JWT: "+elapsedTime);;
					continue START;
				} else {
					throw new InvalidRequestException("Invalid request for fetching records", exception);
				}
			}catch(HttpServerErrorException exception) {
				throw new InvalidRequestException("Invalid request for fetching records",exception);
			}
		}
		
		return response.getBody();
	}

	
	
	@SuppressWarnings("unchecked")
	public JSONArray searchByName(JSONObject requestAsJSON) throws InvalidRequestException, JsonProcessingException, ParseException {
		String firstName = (String) requestAsJSON.get("firstName");
		String lastName = (String) requestAsJSON.get("lastName");
		if(firstName==null||lastName==null)
			throw new InvalidRequestException("First Name or Last Name is missing");
		
		JSONObject filter = new JSONObject();
		filter.put("personalDetails.applicantName.firstName", firstName);
		filter.put("personalDetails.applicantName.lastName", lastName);
		String filterAsString = filter.toJSONString();
		
		String url = businessCenter+environment.getProperty("domain")+"/"+environment.getProperty("ckycHub")+"?filter={filterAsString}&expand=true";
		ResponseEntity<JSONArray> response = null;
		HttpHeaders header = new HttpHeaders();
		header.setContentType(MediaType.APPLICATION_JSON);
		header.set("Authorization", "JWT " + RetrieveRecordService.token);
		logger.info("URL: " + url);

		START: while (response == null) {
			try {
				HttpEntity<String> entity = new HttpEntity<>(header);
				logger.info("Searching matching records from ODP(CKYC Hub).");
				long startTime = System.currentTimeMillis();
				response = template.exchange(url, HttpMethod.GET, entity, JSONArray.class, filterAsString);
				long elapsedTime = (System.currentTimeMillis() - startTime);
				logger.info("Time elapsed (in milliseconds) for fetching all the records from ODP(CKYC Hub): "+elapsedTime);;
			} catch (HttpClientErrorException exception) {
				if (exception.getStatusText().contains("Unauthorized")) {
					logger.info("Invalid JWT token. Generating a new token from Login.");
					long startTime = System.currentTimeMillis();
					RetrieveRecordService.token = tokenGenerateService.generateToken();
					long elapsedTime = (System.currentTimeMillis() - startTime);
					header.set("Authorization", "JWT " + RetrieveRecordService.token);
					logger.info("Time elapsed (in milliseconds) for generating the JWT: "+elapsedTime);;
					continue START;
				} else {
					throw new InvalidRequestException("Invalid request for fetching records", exception);
				}
			}catch(HttpServerErrorException exception) {
				throw new InvalidRequestException("Invalid request for fetching records",exception);
			}
		}
		
		return response.getBody();
	}

}
