package com.capiot.ckyc.api.search.response;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

import org.springframework.stereotype.Component;

@Component
@XmlRootElement(name="PID")
@XmlAccessorType(XmlAccessType.FIELD)
public class SearchResponsePID {
	
	@XmlElement(name="PID_DATA", type=SearchResponsePIDData.class)
	SearchResponsePIDData pidData;

	public SearchResponsePID() {
		super();
	}

	public SearchResponsePID(SearchResponsePIDData pidData) {
		super();
		this.pidData = pidData;
	}

	public SearchResponsePIDData getPidData() {
		return pidData;
	}

	public void setPidData(SearchResponsePIDData pidData) {
		this.pidData = pidData;
	}

	@Override
	public String toString() {
		return "PID [pidData=" + pidData + "]";
	}
	
	
	
}
