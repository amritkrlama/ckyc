package com.capiot.ckyc.api.search.request;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.StringReader;
import java.io.UnsupportedEncodingException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.TimeZone;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Component;
import org.w3c.dom.Document;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;

import com.capiot.ckyc.api.support.RequestSecureService;

@Component
@Configuration
@PropertySource("classpath:ckyc.properties")
public class InitializeSearchTagsService {
		
	@Autowired
	Environment environment;
	@Autowired
	RequestSecureService requestSecureService;
	
	public static final Logger logger = LoggerFactory.getLogger(InitializeSearchTagsService.class);

	
	// To initialize and Marshal the PID part 
		public Document initializePIDDataAndMarshal(String idType, String idNumber) {
			intializePidData(getCurrentDateTime(), idType, idNumber);
			
			SearchRequestService.pidStream = new ByteArrayOutputStream();
			try {
				JAXBContext context = JAXBContext.newInstance(SearchRequestPIDData.class);
				Marshaller marshaller = context.createMarshaller();
				marshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);
				marshaller.marshal(SearchRequestService.pidData, SearchRequestService.pidStream);
				SearchRequestService.pidAsStringXml = SearchRequestService.pidStream.toString("UTF-8");
				logger.info("PID Data: {}{}", System.lineSeparator(), SearchRequestService.pidAsStringXml);
				
				DocumentBuilderFactory documentBuilderFactory = DocumentBuilderFactory.newInstance();
				DocumentBuilder documentBuilder = documentBuilderFactory.newDocumentBuilder();
				Document PIDData = documentBuilder.parse(new InputSource(new StringReader(SearchRequestService.pidAsStringXml)));
				return PIDData;
			} catch (JAXBException exception) {
				logger.debug("Exception while Marshaling");
				exception.printStackTrace();
			} catch (UnsupportedEncodingException e) {
				logger.debug("Error while Marshaling");
				e.printStackTrace();
			} catch (SAXException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (ParserConfigurationException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} finally {
				try {
					if(SearchRequestService.pidStream!=null)
					SearchRequestService.pidStream.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
			return null;
		}
		
		public void intializeHeader(String fiCode, String requestId, String version) {
			SearchRequestService.header = new SearchRequestHeader();
			SearchRequestService.header.setFiCode(fiCode);
			SearchRequestService.header.setRequestId(requestId);
			SearchRequestService.header.setVersion(version);
		}

		public void intializePidData(String dateTime, String ckycNumber, String dateOfBirth) {
			SearchRequestService.pidData = new SearchRequestPIDData();
			SearchRequestService.pidData.setDateTime(dateTime);
			SearchRequestService.pidData.setCkycNumber(ckycNumber);
			SearchRequestService.pidData.setDateOfBirth(dateOfBirth);
		}

		public void initializePID(SearchRequestPIDData pidData) {
			SearchRequestService.pid = new SearchRequestPID();
			SearchRequestService.pid.setPidData(pidData);
		}

		public void intializeCkycInq(String sessionKey, String pid) {
			SearchRequestService.ckycInq = new SearchRequestCkycInq();
			SearchRequestService.ckycInq.setSessionKey(sessionKey);
			SearchRequestService.ckycInq.setPid(pid);
		}

		public void initializeReqRoot(SearchRequestHeader header, SearchRequestCkycInq ckycInq) {
			SearchRequestService.reqRoot = new SearchRequestReqRoot();
			SearchRequestService.reqRoot.setCkycInq(ckycInq);
			SearchRequestService.reqRoot.setHeader(header);
		}

		
		
		/* Method to return current date time */
		public String getCurrentDateTime() {
			String indianTimeZone = environment.getProperty("timeZone");
			SimpleDateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy HH:mm:ss");
			dateFormat.setTimeZone(TimeZone.getTimeZone(indianTimeZone));
			Date now = new Date();
			String currentDateTime = dateFormat.format(now);
			return currentDateTime;
		}
}