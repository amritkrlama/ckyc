package com.capiot.ckyc.api.search.request;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

import org.springframework.stereotype.Component;

@Component
@XmlRootElement(name="PID")
@XmlAccessorType(XmlAccessType.FIELD)
public class SearchRequestPID {
	
	@XmlElement(name="PID_DATA", type=SearchRequestPIDData.class)
	SearchRequestPIDData pidData;

	public SearchRequestPID() {
		super();
	}

	public SearchRequestPID(SearchRequestPIDData pidData) {
		super();
		this.pidData = pidData;
	}

	public SearchRequestPIDData getPidData() {
		return pidData;
	}

	public void setPidData(SearchRequestPIDData pidData) {
		this.pidData = pidData;
	}

	@Override
	public String toString() {
		return "PID [pidData=" + pidData + "]";
	}
	
	
	
}