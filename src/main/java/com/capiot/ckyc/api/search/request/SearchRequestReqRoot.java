package com.capiot.ckyc.api.search.request;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

import org.springframework.stereotype.Component;

@Component
@XmlRootElement(name="REQ_ROOT")
@XmlAccessorType(XmlAccessType.FIELD)
public class SearchRequestReqRoot {

	@XmlElement(name="HEADER", type=SearchRequestHeader.class)
	SearchRequestHeader header;

	@XmlElement(name="CKYC_INQ", type=SearchRequestCkycInq.class)
	SearchRequestCkycInq ckycInq;
	
	public SearchRequestReqRoot(SearchRequestHeader header, SearchRequestCkycInq ckycInq) {
		super();
		this.header = header;
		this.ckycInq = ckycInq;
	}

	public SearchRequestReqRoot() {
		super();
	}

	public SearchRequestHeader getHeader() {
		return header;
	}

	public void setHeader(SearchRequestHeader header) {
		this.header = header;
	}

	public SearchRequestCkycInq getCkycInq() {
		return ckycInq;
	}

	public void setCkycInq(SearchRequestCkycInq ckycInq) {
		this.ckycInq = ckycInq;
	}

	@Override
	public String toString() {
		return "ReqRoot [header=" + header + ", ckycInq=" + ckycInq + "]";
	}
	

}
