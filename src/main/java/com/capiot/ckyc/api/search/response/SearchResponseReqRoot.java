package com.capiot.ckyc.api.search.response;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

import org.springframework.stereotype.Component;

@Component
@XmlRootElement(name="REQ_ROOT")
@XmlAccessorType(XmlAccessType.FIELD)
public class SearchResponseReqRoot {

	@XmlElement(name="HEADER", type=SearchResponseHeader.class)
	SearchResponseHeader header;

	@XmlElement(name="CKYC_INQ", type=SearchResponseCkycInq.class)
	SearchResponseCkycInq ckycInq;
	
	public SearchResponseReqRoot(SearchResponseHeader header, SearchResponseCkycInq ckycInq) {
		super();
		this.header = header;
		this.ckycInq = ckycInq;
	}

	public SearchResponseReqRoot() {
		super();
	}

	public SearchResponseHeader getHeader() {
		return header;
	}

	public void setHeader(SearchResponseHeader header) {
		this.header = header;
	}

	public SearchResponseCkycInq getCkycInq() {
		return ckycInq;
	}

	public void setCkycInq(SearchResponseCkycInq ckycInq) {
		this.ckycInq = ckycInq;
	}

	@Override
	public String toString() {
		return "ReqRoot [header=" + header + ", ckycInq=" + ckycInq + "]";
	}
	

}