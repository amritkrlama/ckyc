package com.capiot.ckyc.api.search.request;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

import org.springframework.stereotype.Component;

@Component
@XmlRootElement(name="CKYC_INQ")
@XmlAccessorType(XmlAccessType.FIELD)
public class SearchRequestCkycInq {
	
	@XmlElement(name="SESSION_KEY")
	String sessionKey;
		
	@XmlElement(name="PID")
	String pid;
	
	
	public SearchRequestCkycInq() {
		super();
	}


	public String getSessionKey() {
		return sessionKey;
	}


	public void setSessionKey(String sessionKey) {
		this.sessionKey = sessionKey;
	}


	public String getPid() {
		return pid;
	}


	public void setPid(String pid) {
		this.pid = pid;
	}

	
	
	
}