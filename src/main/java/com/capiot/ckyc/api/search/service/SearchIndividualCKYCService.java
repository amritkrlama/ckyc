package com.capiot.ckyc.api.search.service;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import org.springframework.core.env.Environment;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.HttpServerErrorException;
import org.springframework.web.client.RestTemplate;

import com.capiot.ckyc.bulkfile.constant.SearchIndividualCKYCValues;
import com.capiot.ckyc.bulkfile.exception.InvalidRequestException;
import com.capiot.ckyc.bulkfile.odp.RetrieveRecordService;
import com.capiot.ckyc.bulkfile.odp.TokenGenerateService;
import com.fasterxml.jackson.core.JsonProcessingException;

@RestController
@Configuration
@PropertySource("classpath:ckyc.properties")
public class SearchIndividualCKYCService {

	Logger logger = LoggerFactory.getLogger(SearchIndividualCKYCService.class);

	@Autowired
	private Environment environment;
	@Autowired
	RestTemplate template;
	@Autowired
	TokenGenerateService tokenGenerateService;
	@Value("${business_center}")
	private String businessCenter;

	@SuppressWarnings("unchecked")
	@PostMapping(value = "/api/searchIndividualCkyc", produces = "application/json")
	public JSONArray searchForCKYC(@RequestBody String payload)
			throws JsonProcessingException, InvalidRequestException, ParseException {

		JSONParser parser = new JSONParser();

		ResponseEntity<?> resultFromSearch = null;
		JSONArray response = null;
		JSONObject identityType = null;
		String categoryName = null;
		String identityId = null;
		String identityNumber = null;

		JSONObject payloadAsJSON = (JSONObject) parser.parse(payload);

		
		JSONObject identity = (JSONObject) payloadAsJSON.get(SearchIndividualCKYCValues.IDENTITY);
		if (identity != null) {
			identityNumber = (String) identity.get(SearchIndividualCKYCValues.NUMBER);
			if (identityNumber == null) {
				logger.info("Identity Number is empty");
				throw new InvalidRequestException("Enter the Identity Number first");
			}
			identityType = (JSONObject) identity.get(SearchIndividualCKYCValues.IDENTITY_TYPE);
			if (identityType != null) {
				categoryName = (String) identityType.get(SearchIndividualCKYCValues.CATEGORY_NAME);
			} else {
				throw new InvalidRequestException("Select an Identity Type first");
			}
		} else {
			throw new InvalidRequestException("Select an Identity first");
		}

		
		
		// Fetching Identity Code for the given Category Name
		JSONObject filterForIdentityCode = new JSONObject();
		filterForIdentityCode.put("categoryName", categoryName);
		logger.info("Filter for searching Identity Codes: {}", filterForIdentityCode.toJSONString());
		ResponseEntity<?> searchFromIdentityCodes = searchFromIdentityCodes(filterForIdentityCode.toJSONString());
		JSONArray searchFromIdentityCodesAsArray = (JSONArray) parser
				.parse(searchFromIdentityCodes.getBody().toString());
		if (searchFromIdentityCodesAsArray.isEmpty()) {
			throw new InvalidRequestException("Invalid request received");
		}

		JSONObject searchFromIdentityCodesAsObject = (JSONObject) searchFromIdentityCodesAsArray.get(0);
		if (searchFromIdentityCodesAsObject != null) {
			identityId = (String) searchFromIdentityCodesAsObject.get(SearchIndividualCKYCValues.ID);
		} else {
			logger.info("Empty response for Search from Identity Codes");
		}
		
		

		// Filter for searching in CKYC Registry or Existing Entity
		JSONObject filterForIndividualCKYC = new JSONObject();
		filterForIndividualCKYC.put("identity.type._id", identityId);
		filterForIndividualCKYC.put("identity.number", identityNumber);
		logger.info("Filter for searching Individual CKYC: {}", filterForIndividualCKYC.toJSONString());

		// Fetching Search Type
		String searchType = (String) payloadAsJSON.get(SearchIndividualCKYCValues.SEARCH_TYPE);
		if (searchType == null) {
			logger.info("Search Type is missing");
		}
		else if (searchType.equalsIgnoreCase(SearchIndividualCKYCValues.CKYC_REGISTRY)) {
			resultFromSearch = searchFromCKYCRegistry(filterForIndividualCKYC.toJSONString());
			response = (JSONArray) parser.parse(resultFromSearch.getBody().toString());
		} else if (searchType.equalsIgnoreCase(SearchIndividualCKYCValues.EXISTING_ENTITY)) {
			resultFromSearch = searchFromExistingEntity(filterForIndividualCKYC.toJSONString());
			response = (JSONArray) parser.parse(resultFromSearch.getBody().toString());
		} else {
			throw new InvalidRequestException("Invalid Search Type");
		}
		
		
		if (response == null) {
			logger.info("No matching record found");
			throw new InvalidRequestException("No matching record found");
		}

		return response;

	}

	
	
	/* Method to fetch matching record from Existing Data Service */
	public ResponseEntity<?> searchFromExistingEntity(String filter)
			throws InvalidRequestException, JsonProcessingException, ParseException {

		final String BUSINESS_CENTER =environment.getProperty("business_center");
		final String DOMAIN = environment.getProperty("domain");
		final String ENTITY = environment.getProperty("entity");

		ResponseEntity<String> response = null;

		String url = BUSINESS_CENTER + DOMAIN + "/" + ENTITY + "/?filter={filter}&expand=true";
		HttpHeaders header = new HttpHeaders();
		header.setContentType(MediaType.APPLICATION_JSON);
		header.set("Authorization", "JWT " + RetrieveRecordService.token);
		logger.info("URL: " + url);

		START: while (response == null) {
			try {
				HttpEntity<String> entity = new HttpEntity<>(header);
				long startTime = System.currentTimeMillis();
				response = template.exchange(url, HttpMethod.GET, entity, String.class, filter);
				long elapsedTime = (System.currentTimeMillis() - startTime);
				logger.info("Time elapsed (in milliseconds) for getting the matched record:{} ", elapsedTime);
				;
			} catch (HttpClientErrorException exception) {
				if (exception.getStatusText().contains("Unauthorized")) {
					RetrieveRecordService.token = tokenGenerateService.generateToken();
					header.set("Authorization", "JWT " + RetrieveRecordService.token);
					continue START;
				} else {
					throw new InvalidRequestException("Invalid request for searching records", exception);
				}
			} catch (HttpServerErrorException exception) {
				throw new InvalidRequestException("Invalid request for searching records", exception);
			}
		}
		return response;
	}
	
	

	/* Method to fetch matching record from Mock Data Service */
	public ResponseEntity<?> searchFromCKYCRegistry(String filter)
			throws InvalidRequestException, JsonProcessingException, ParseException {

		final String BUSINESS_CENTER = businessCenter;
		final String DOMAIN = environment.getProperty("domain");
		final String ENTITY = environment.getProperty("mockEntity");

		ResponseEntity<String> response = null;

		String url = BUSINESS_CENTER + DOMAIN + "/" + ENTITY + "/?filter={filter}&expand=true";
		HttpHeaders header = new HttpHeaders();
		header.setContentType(MediaType.APPLICATION_JSON);
		header.set("Authorization", "JWT " + RetrieveRecordService.token);
		logger.info("URL: " + url);

		START: while (response == null) {
			try {
				HttpEntity<String> entity = new HttpEntity<>(header);
				long startTime = System.currentTimeMillis();
				response = template.exchange(url, HttpMethod.GET, entity, String.class, filter);
				long elapsedTime = (System.currentTimeMillis() - startTime);
				logger.info("Time elapsed (in milliseconds) for getting the matched record:{} ", elapsedTime);
			} catch (HttpClientErrorException exception) {
				if (exception.getStatusText().contains("Unauthorized")) {
					RetrieveRecordService.token = tokenGenerateService.generateToken();
					header.set("Authorization", "JWT " + RetrieveRecordService.token);
					continue START;
				} else {
					throw new InvalidRequestException("Invalid request for searching records", exception);
				}
			} catch (HttpServerErrorException exception) {
				throw new InvalidRequestException("Invalid request for searching records", exception);
			}
		}
		return response;
	}
	
	

	/* Method to fetch matching record from Identity Codes Data Service */
	public ResponseEntity<?> searchFromIdentityCodes(String filter)
			throws InvalidRequestException, JsonProcessingException, ParseException {

		final String BUSINESS_CENTER = businessCenter;
		final String DOMAIN = environment.getProperty("domain");
		final String ENTITY = environment.getProperty("identityEntity");

		ResponseEntity<String> response = null;

		String url = BUSINESS_CENTER + DOMAIN + "/" + ENTITY + "/?filter={filter}&expand=true";
		HttpHeaders header = new HttpHeaders();
		header.setContentType(MediaType.APPLICATION_JSON);
		header.set("Authorization", "JWT " + RetrieveRecordService.token);
		logger.info("URL: " + url);

		START: while (response == null) {
			try {
				HttpEntity<String> entity = new HttpEntity<>(header);
				long startTime = System.currentTimeMillis();
				response = template.exchange(url, HttpMethod.GET, entity, String.class, filter);
				long elapsedTime = (System.currentTimeMillis() - startTime);
				logger.info(
						"Time elapsed (in milliseconds) for getting the matched record from Identity Codes Service:{} ",
						elapsedTime);
			} catch (HttpClientErrorException exception) {
				if (exception.getStatusText().contains("Unauthorized")) {
					RetrieveRecordService.token = tokenGenerateService.generateToken();
					header.set("Authorization", "JWT " + RetrieveRecordService.token);
					continue START;
				} else {
					throw new InvalidRequestException("Invalid request for searching records", exception);
				}
			} catch (HttpServerErrorException exception) {
				throw new InvalidRequestException("Invalid request for searching records", exception);
			}
		}
		return response;
	}

}
