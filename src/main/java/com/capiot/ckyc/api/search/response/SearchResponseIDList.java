package com.capiot.ckyc.api.search.response;

import java.util.List;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

import org.springframework.stereotype.Component;

@Component
@XmlRootElement(name="ID_LIST")
@XmlAccessorType(XmlAccessType.FIELD)
public class SearchResponseIDList {
	
	@XmlElement(name="ID", type=SearchResponseID.class)
	List<SearchResponseID> id;

	public SearchResponseIDList(List<SearchResponseID> id) {
		super();
		this.id = id;
	}

	public SearchResponseIDList() {
		super();
	}

	public List<SearchResponseID> getId() {
		return id;
	}

	public void setId(List<SearchResponseID> id) {
		this.id = id;
	}
	
	
	
}
