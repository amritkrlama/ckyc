package com.capiot.ckyc.api.search.response;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

import org.springframework.stereotype.Component;

@Component
@XmlRootElement(name="CKYC_INQ")
@XmlAccessorType(XmlAccessType.FIELD)
public class SearchResponseCkycInq {
	
	@XmlElement(name="SESSION_KEY")
	String sessionKey;
		
	@XmlElement(name="PID")
	String pid;
	
	
	public SearchResponseCkycInq() {
		super();
	}

	public SearchResponseCkycInq(String sessionKey, String pid) {
		super();
		this.sessionKey = sessionKey;
		this.pid = pid;
	}

	public String getSessionKey() {
		return sessionKey;
	}

	public void setSessionKey(String secretKey) {
		this.sessionKey = secretKey;
	}

	public String getPid() {
		return pid;
	}

	public void setPid(String pid) {
		this.pid = pid;
	}

	@Override
	public String toString() {
		return "CkycInq [sessionKey=" + sessionKey + ", pid=" + pid + "]";
	}
	
	
	
}
