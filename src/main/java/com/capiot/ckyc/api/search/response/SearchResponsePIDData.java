package com.capiot.ckyc.api.search.response;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

import org.springframework.stereotype.Component;

@Component
@XmlRootElement(name="PID_DATA")
@XmlAccessorType(XmlAccessType.FIELD)
public class SearchResponsePIDData {
	
	@XmlElement(name="AGE")
	String age;
	@XmlElement(name="CKYC_NO")
	String ckycNumber;
	@XmlElement(name="FATHERS_NAME")
	String fatherName;
	@XmlElement(name="IMAGE_TYPE")
	String imageType;
	@XmlElement(name="KYC_DATE")
	String kycDate;
	@XmlElement(name="NAME")
	String name;
	@XmlElement(name="PHOTO")
	String photo;
	@XmlElement(name="UPDATED_DATE")
	String updatedDate;
	@XmlElement(name="ID_LIST", type=SearchResponseIDList.class)
	SearchResponseIDList idList;
	@XmlElement(name="REMARKS")
	String remarks;
	
	
	public SearchResponsePIDData(String age, String ckycNumber, String fatherName, String imageType, String kycDate, String name,
			String photo, String updatedDate, SearchResponseIDList idList, String remarks) {
		super();
		this.age = age;
		this.ckycNumber = ckycNumber;
		this.fatherName = fatherName;
		this.imageType = imageType;
		this.kycDate = kycDate;
		this.name = name;
		this.photo = photo;
		this.updatedDate = updatedDate;
		this.idList = idList;
		this.remarks = remarks;
	}
	
	public SearchResponsePIDData() {
		super();
	}
	
	public String getAge() {
		return age;
	}
	public void setAge(String age) {
		this.age = age;
	}
	public String getCkycNumber() {
		return ckycNumber;
	}
	public void setCkycNumber(String ckycNumber) {
		this.ckycNumber = ckycNumber;
	}
	public String getFatherName() {
		return fatherName;
	}
	public void setFatherName(String fatherName) {
		this.fatherName = fatherName;
	}
	public String getImageType() {
		return imageType;
	}
	public void setImageType(String imageType) {
		this.imageType = imageType;
	}
	public String getKycDate() {
		return kycDate;
	}
	public void setKycDate(String kycDate) {
		this.kycDate = kycDate;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getPhoto() {
		return photo;
	}
	public void setPhoto(String photo) {
		this.photo = photo;
	}
	public String getUpdatedDate() {
		return updatedDate;
	}
	public void setUpdatedDate(String updatedDate) {
		this.updatedDate = updatedDate;
	}
	public SearchResponseIDList getIdList() {
		return idList;
	}
	public void setIdList(SearchResponseIDList idList) {
		this.idList = idList;
	}
	public String getRemarks() {
		return remarks;
	}
	public void setRemarks(String remarks) {
		this.remarks = remarks;
	}


	@Override
	public String toString() {
		return "PIDData [age=" + age + ", ckycNumber=" + ckycNumber + ", fatherName=" + fatherName + ", imageType="
				+ imageType + ", kycDate=" + kycDate + ", name=" + name + ", photo=" + photo + ", updatedDate="
				+ updatedDate + ", idList=" + idList + ", remarks=" + remarks + "]";
	}
	
	
	
	
}
