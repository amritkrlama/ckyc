package com.capiot.ckyc.api.search.response;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

import org.springframework.stereotype.Component;

@Component
@XmlRootElement(name="ID")
@XmlAccessorType(XmlAccessType.FIELD)
public class SearchResponseID {
	
	@XmlElement(name="STATUS")
	String status;
	@XmlElement(name="TYPE")
	String type;
	
	
	public SearchResponseID(String status, String type) {
		super();
		this.status = status;
		this.type = type;
	}
	
	public SearchResponseID() {
		super();
	}
	
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}

	@Override
	public String toString() {
		return "ID [status=" + status + ", type=" + type + "]";
	}
	
	
	
}
