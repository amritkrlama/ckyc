package com.capiot.ckyc.api.support;

import java.io.StringReader;
import java.security.PublicKey;
import java.util.Base64;

import javax.crypto.Cipher;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Unmarshaller;
import javax.xml.crypto.dsig.XMLSignature;
import javax.xml.crypto.dsig.XMLSignatureFactory;
import javax.xml.crypto.dsig.dom.DOMValidateContext;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;
import org.w3c.dom.Document;
import org.w3c.dom.NodeList;

import com.capiot.ckyc.api.search.request.SearchRequestReqRoot;
import com.capiot.ckyc.api.search.response.SearchResponseCkycInq;
import com.capiot.ckyc.api.search.response.SearchResponseReqRoot;

@Component
public class ResponseSecureService {

	
	public static final Logger logger = LoggerFactory.getLogger(ResponseSecureService.class);
	

	
	/**
	 * @param Response to be verified
	 * @param Cersai public Key Path
	 * @return
	 * @throws Exception 
	 */
	public boolean verifySign(Document document, String publicKeyPath ) throws Exception {
		boolean verified = false;
		NodeList nodeList = document.getElementsByTagNameNS(XMLSignature.XMLNS, "Signature");
		if(nodeList.getLength()==0)
			throw new Exception("No XML Digital Signature has been found in the Response from Cersai");
		PublicKey publicKey = CommonService.getPublicKey(publicKeyPath);
		DOMValidateContext validateContext = new DOMValidateContext(publicKey, nodeList.item(0));
		XMLSignatureFactory xmlSignatureFactory = XMLSignatureFactory.getInstance("DOM");
		XMLSignature xmlSignature = xmlSignatureFactory.unmarshalXMLSignature(validateContext);
		verified = xmlSignature.validate(validateContext);
		return verified;
	}
	
	
	
	/**
	 * @param Response as SearchResponseReqRoot
	 * @return Session Key as String
	 */
	public String getSessionKeyFromResponse(SearchResponseReqRoot response) {
		SearchResponseCkycInq ckycInq = response.getCkycInq();
		return ckycInq.getSessionKey();	
	}
	
	
	public String getPIDDataFromResponse(SearchResponseReqRoot response) {
		SearchResponseCkycInq ckycInq = response.getCkycInq();
		return ckycInq.getPid();
	}

	/**
	 * @param Session Key
	 * @param key Store FileName
	 * @param keyStore Password
	 * @param key Password
	 * @return
	 */
	public String decryptSessionKey(String sessionKey, String keyStoreFileName, String keyStorePass, String keyPass) {
		try {
			Cipher cipher = Cipher.getInstance("RSA/ECB/PKCS1Padding");
			cipher.init(Cipher.DECRYPT_MODE, CommonService.getPrivateKey(keyStoreFileName, keyStorePass, keyPass));
			return new String(cipher.doFinal(Base64.getDecoder().decode(sessionKey)));
		}catch(Exception exception) {
			logger.debug("Unable to decrypt Session Key");
			exception.printStackTrace();
		}
		return null;
	}
	
	
	/**
	 * @param PID Data as String
	 * @param Decypted Session Key
	 * @return
	 */
	public String decryptPID(String pid, String sessionKey) {
		try {
			Cipher cipher = Cipher.getInstance("AES/CBC/PKCS5Padding");
			cipher.init(Cipher.DECRYPT_MODE, CommonService.stringToSecretKey(sessionKey), CommonService.generateRandomIV());
			return new String(cipher.doFinal(Base64.getDecoder().decode(pid)));
		}catch(Exception exception) {
			logger.debug("Unable to decrypt");
			exception.printStackTrace();
		}
		return null;
	}
	
	
	/**
	 * @param Cersai Response as String
	 * @return Marshalled XML request
	 */
	public SearchRequestReqRoot unmarshal(String response) {
		SearchRequestReqRoot root = null;
		try {
			JAXBContext context = JAXBContext.newInstance(SearchRequestReqRoot.class);
			Unmarshaller unmarshaller = context.createUnmarshaller();
			StringReader reader = new StringReader(response);
			root = (SearchRequestReqRoot) unmarshaller.unmarshal(reader);
		} catch (JAXBException e) {
			logger.debug("Unable to Unmarshal the Search Response came from Cersai");
			e.printStackTrace();
		}
		return root;
	}
	
	
	public SearchResponseReqRoot unmarshalToSearchResponseReqRoot(Document document) {
		SearchResponseReqRoot root =  null;
		try {
			JAXBContext context = JAXBContext.newInstance(SearchResponseReqRoot.class);
			Unmarshaller unmarshaller = context.createUnmarshaller();
			root = (SearchResponseReqRoot) unmarshaller.unmarshal(document);
		} catch (JAXBException e) {
			logger.debug("Unable to Unmarshal the Search Response came from Cersai");
			e.printStackTrace();
		}
		return root;
	}
}
