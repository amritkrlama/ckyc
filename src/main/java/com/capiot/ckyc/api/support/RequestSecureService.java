package com.capiot.ckyc.api.support;


import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.security.InvalidAlgorithmParameterException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.security.PrivateKey;
import java.security.SecureRandom;
import java.security.Signature;
import java.security.SignatureException;
import java.util.Arrays;
import java.util.Base64;
import java.util.Collections;

import javax.crypto.BadPaddingException;
import javax.crypto.Cipher;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.KeyGenerator;
import javax.crypto.NoSuchPaddingException;
import javax.crypto.SecretKey;
import javax.xml.crypto.dsig.CanonicalizationMethod;
import javax.xml.crypto.dsig.DigestMethod;
import javax.xml.crypto.dsig.Reference;
import javax.xml.crypto.dsig.SignatureMethod;
import javax.xml.crypto.dsig.SignedInfo;
import javax.xml.crypto.dsig.Transform;
import javax.xml.crypto.dsig.XMLSignature;
import javax.xml.crypto.dsig.XMLSignatureFactory;
import javax.xml.crypto.dsig.dom.DOMSignContext;
import javax.xml.crypto.dsig.spec.C14NMethodParameterSpec;
import javax.xml.crypto.dsig.spec.TransformParameterSpec;

import org.apache.xml.security.encryption.XMLCipher;
import org.apache.xml.security.encryption.XMLEncryptionException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;
import org.w3c.dom.Document;
import org.w3c.dom.Element;



@Component
public class RequestSecureService {

	public static final Logger logger = LoggerFactory.getLogger(RequestSecureService.class);

	/**
	 * @return Generated Session Key
	 */
	public String generateSessionKey() {
		SecretKey secretKey = null;
		try {
			KeyGenerator keyGenerator = KeyGenerator.getInstance("AES");
			SecureRandom secureRandom = new SecureRandom();
			keyGenerator.init(256, secureRandom);
			secretKey = keyGenerator.generateKey();
		} catch (NoSuchAlgorithmException e) {
			e.printStackTrace();
		}
		return new String(Base64.getEncoder().encodeToString(secretKey.getEncoded()));
	}

	
	/**
	 * @param Session Key
	 * @param PID XML as String to Encrypt
	 * @return Encrypted PID in Base64 encoded form
	 */
	public String encryptPID(String sessionKey, String pidAsStringXml) {
		try {
			Cipher cipher = Cipher.getInstance("AES/CBC/PKCS5Padding");
			cipher.init(Cipher.ENCRYPT_MODE, CommonService.stringToSecretKey(sessionKey), CommonService.generateRandomIV());
			return Base64.getEncoder().encodeToString(cipher.doFinal(pidAsStringXml.getBytes("UTF-8")));
		} catch (NoSuchAlgorithmException | NoSuchPaddingException e) {
			e.printStackTrace();
		} catch (InvalidKeyException e) {
			e.printStackTrace();
		} catch (IllegalBlockSizeException e) {
			e.printStackTrace();
		} catch (BadPaddingException e) {
			e.printStackTrace();
		} catch (UnsupportedEncodingException e) {
			e.printStackTrace();
		} catch (InvalidAlgorithmParameterException e) {
			e.printStackTrace();
		}
		return null;
	}
	
	
	
	/**
	 * @param Session Key
	 * @param PID as XML Document
	 * @return Encrypted PIDData
	 */
	public String encryptPID(String sessionKey, Document pid) {
		Element rootElement = pid.getDocumentElement();
		try {
			XMLCipher cipher = XMLCipher.getInstance(XMLCipher.AES_256);
			cipher.init(XMLCipher.ENCRYPT_MODE, CommonService.stringToSecretKey(sessionKey));
			pid = cipher.doFinal(pid, rootElement, true);
			return null;
		} catch (XMLEncryptionException e) {
			e.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}


	
	/**
	 * @param Cersai Public key filename
	 * @param Session Key
	 * @return Encrypted Session Key in Base64 coded form
	 * @throws IOException 
	 */
	public String encryptSessionKeyUsingCersaiPublicKey(String fileName, String sessionKey){
		try {
			Cipher cipher = Cipher.getInstance("RSA/ECB/PKCS1Padding");
			cipher.init(Cipher.ENCRYPT_MODE, CommonService.getPublicKey(fileName));
			return Base64.getEncoder().encodeToString(cipher.doFinal(sessionKey.getBytes("UTF-8")));
		} catch (NoSuchAlgorithmException | NoSuchPaddingException e) {
			e.printStackTrace();
		} catch (InvalidKeyException e) {
			e.printStackTrace();
		} catch (IllegalBlockSizeException e) {
			e.printStackTrace();
		} catch (BadPaddingException e) {
			e.printStackTrace();
		} catch (UnsupportedEncodingException e) {
			e.printStackTrace();
		} 
		return null;
	}
	
	
	
	/**
	 * @param The whole request
	 * @param File Name of the keystore
	 * @return Digitally signed request
	 */
	public String digitalSign(String request, String fileName, String keyStorePassword, String keyPassword) {
		try {
			Signature signature = Signature.getInstance("SHA256WithRSA");
			signature.initSign(CommonService.getPrivateKey(fileName, keyStorePassword, keyPassword));
			signature.update(request.getBytes("UTF-8"));
			byte[] signatureBytes = signature.sign();
			return Arrays.toString(signatureBytes);
			//return Base64.getEncoder().encodeToString(signatureBytes);
		} catch (NoSuchAlgorithmException e) {
			e.printStackTrace();
		} catch (InvalidKeyException e) {
			e.printStackTrace();
		} catch (SignatureException e) {
			e.printStackTrace();
		} catch (UnsupportedEncodingException e) {
			e.printStackTrace();
		}
		return null;
	}
	
	
	/**
	 * @param Request as XML
	 * @param KeyStore fileName
	 * @param keyStorePassword
	 * @param keyPassword
	 * @return Signed Request as XML
	 */
	public Document sign(Document request, String fileName, String keyStorePassword, String keyPassword) {
		XMLSignatureFactory xmlSignatureFactory = XMLSignatureFactory.getInstance("DOM");
		PrivateKey privateKey = CommonService.getPrivateKey(fileName, keyStorePassword, keyPassword);
		DOMSignContext domSignContext = new DOMSignContext(privateKey, request.getDocumentElement());
		Reference reference = null;
		SignedInfo signedInfo = null;
		try {
			reference = xmlSignatureFactory.newReference("", xmlSignatureFactory.newDigestMethod(DigestMethod.SHA1, null));
			Collections.singletonList(xmlSignatureFactory.newTransform(Transform.ENVELOPED, (TransformParameterSpec)null));
			signedInfo = xmlSignatureFactory.newSignedInfo(xmlSignatureFactory.newCanonicalizationMethod(CanonicalizationMethod.INCLUSIVE, (C14NMethodParameterSpec)null),
						xmlSignatureFactory.newSignatureMethod(SignatureMethod.RSA_SHA1, null), Collections.singletonList(reference));
			XMLSignature xmlSignature = xmlSignatureFactory.newXMLSignature(signedInfo, null);
			xmlSignature.sign(domSignContext);
			return request;
		}catch(Exception e) {
			e.printStackTrace();
		}
		return null;
	}


}
