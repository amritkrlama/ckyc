package com.capiot.ckyc.api.support;

import java.io.ByteArrayInputStream;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.security.KeyStore;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.PrivateKey;
import java.security.PublicKey;
import java.security.SecureRandom;
import java.security.UnrecoverableEntryException;
import java.security.KeyStore.PrivateKeyEntry;
import java.security.cert.Certificate;
import java.security.cert.CertificateException;
import java.security.cert.CertificateFactory;
import java.util.Base64;
import java.util.Enumeration;


import javax.crypto.Cipher;
import javax.crypto.SecretKey;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.SecretKeySpec;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.OutputKeys;
import javax.xml.transform.Transformer;

import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;

import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;
import org.w3c.dom.Document;

import com.capiot.ckyc.api.search.response.SearchResponseReqRoot;

@Component
public class CommonService {
		
	public static final Logger logger = LoggerFactory.getLogger(CommonService.class);
	/**
	 * @param String to Decrypt
	 * @param sessionKey
	 * @return Decrypted String
	 */
	public static String decrypt(String stringToDecrypt, String sessionKey) {
		try {
			Cipher cipher = Cipher.getInstance("AES/CBC/PKCS5Padding");
			cipher.init(Cipher.DECRYPT_MODE, stringToSecretKey(sessionKey), generateRandomIV());
			return new String(cipher.doFinal(Base64.getDecoder().decode(stringToDecrypt)));
		}catch(Exception exception) {
			logger.debug("Unable to decrypt");
			exception.printStackTrace();
		}
		return null;
	}
	
	

	
	/**
	 * @param Session Key as String
	 * @return Session Key as SecretKey
	 */
	public static SecretKey stringToSecretKey(String secretKey) {
		byte[] encodedKey = Base64.getDecoder().decode(secretKey);
		SecretKey originalKey = new SecretKeySpec(encodedKey, 0, encodedKey.length, "AES");
		return originalKey;
	}
	
	
	
	/**
	 * @param Cersai Public key file name
	 * @return Public key
	 * @throws IOException 
	 */
	public static PublicKey getPublicKey(String fileName) {
		InputStream inputStream = null;
		try {
			inputStream = new FileInputStream(fileName);
			CertificateFactory certFactory = CertificateFactory.getInstance("X.509");
			Certificate crtfct = certFactory.generateCertificate(inputStream);
			PublicKey publicKey = crtfct.getPublicKey();
			return publicKey;
		}catch(Exception exception) {
			logger.debug("Error in fetching public key from the file");
			exception.printStackTrace();
		}finally {
			if(inputStream!=null)
				try {
					inputStream.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
		}
		return null;
	}
	
	
	
	/**
	 * @param File name of the Keystore
	 * @return Private key
	 */
	public static PrivateKey getPrivateKey(String fileName, String keyStorePass, String keyPass) {
		try {
			KeyStore keyStore = KeyStore.getInstance(KeyStore.getDefaultType());			
			char[] keyStorePassword = keyStorePass.toCharArray();
			char[] keyPassword = keyPass.toCharArray();
			try(InputStream keyStoreData = new FileInputStream(fileName)){
				keyStore.load(keyStoreData, keyStorePassword);
				Enumeration<String> aliases = keyStore.aliases();
				if(aliases.hasMoreElements()) {
					String alias = aliases.nextElement();
					KeyStore.ProtectionParameter entryPassword = new KeyStore.PasswordProtection(keyPassword);
					KeyStore.PrivateKeyEntry privateKeyEntry = (PrivateKeyEntry) keyStore.getEntry(alias, entryPassword);
					return privateKeyEntry.getPrivateKey();
				}
			} catch (FileNotFoundException e) {
				e.printStackTrace();
			} catch (IOException e) {
				e.printStackTrace();
			} catch (NoSuchAlgorithmException e) {
				e.printStackTrace();
			} catch (CertificateException e) {
				e.printStackTrace();
			} catch (UnrecoverableEntryException e) {
				e.printStackTrace();
			}
		} catch (KeyStoreException e) {
			e.printStackTrace();
		}
		return null;
	}
	
	
	
	
	/**
	 * @return Random generated IV(Initialization Vector)
	 */
	public static IvParameterSpec generateRandomIV() {
		byte[] IV = new byte[16];
		SecureRandom secureRandom = new SecureRandom();
		secureRandom.nextBytes(IV);
		return new IvParameterSpec(IV);
	}
	
	
	
	
	/**
	 * @param XML Document to be printed
	 * @param Outputstream
	 * @throws IOException
	 * @throws TransformerException
	 */
	public static void printDocument(Document doc, OutputStream out) throws IOException, TransformerException {
	    TransformerFactory tf = TransformerFactory.newInstance();
	    Transformer transformer = tf.newTransformer();
	    transformer.setOutputProperty(OutputKeys.OMIT_XML_DECLARATION, "no");
	    transformer.setOutputProperty(OutputKeys.METHOD, "xml");
	    transformer.setOutputProperty(OutputKeys.INDENT, "yes");
	    transformer.setOutputProperty(OutputKeys.ENCODING, "UTF-8");
	    transformer.setOutputProperty("{http://xml.apache.org/xslt}indent-amount", "4");

	    transformer.transform(new DOMSource(doc), 
	         new StreamResult(new OutputStreamWriter(out, "UTF-8")));
	}
	
	

	/**
	 * @param String to be converted
	 * @return Corresponding Document object
	 */
	public static Document convertStringToDocument(String xmlStr) {
		DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
		factory.setNamespaceAware(true);
		DocumentBuilder builder;
		try {
			builder = factory.newDocumentBuilder();
			StringBuilder xmlStringBuilder = new StringBuilder();
			xmlStringBuilder.append(xmlStr);
			ByteArrayInputStream inputStream = new ByteArrayInputStream(xmlStringBuilder.toString().getBytes("UTF-8"));
			Document doc = builder.parse(inputStream);
			return doc;
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}


	/**
	 * @param Request as String
	 * @return Request as JSONObject
	 */
	public static JSONObject convertRequestToJSONObject(String payload) {
		JSONParser parser = new JSONParser();
		JSONObject payloadAsJSON = null;
		try {
			payloadAsJSON = (JSONObject) parser.parse(payload);
		} catch (ParseException e) {
			logger.debug("Unable to parse the request received for Download CKYC");
			e.printStackTrace();
		}
		if (payloadAsJSON != null) {
			logger.info("Recieved request for Downloading CKYC: {}", payloadAsJSON.toJSONString());
		} else {
			logger.info("Empty request received for Downloading CKYC");
		}
		return payloadAsJSON;
	}


	/**
	 * @param SearchResponseReqRoot
	 * @return Corresponding Document
	 */
	public static Document convertToDocument(SearchResponseReqRoot object) {
		try {
			DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
	        DocumentBuilder db = dbf.newDocumentBuilder();
	        Document document = db.newDocument();
			
			JAXBContext context = JAXBContext.newInstance(SearchResponseReqRoot.class);
			Marshaller marshaller = context.createMarshaller();
			marshaller.marshal(object, document);
			
			 TransformerFactory tf = TransformerFactory.newInstance();
		     Transformer transformer = tf.newTransformer();
		     transformer.setOutputProperty(OutputKeys.INDENT, "yes");
		     transformer.setOutputProperty("{http://xml.apache.org/xslt}indent-amount", "4");
		     DOMSource source = new DOMSource(document);
		     StreamResult result = new StreamResult(System.out);
		     transformer.transform(source, result);
		        
			return document; 
		} catch (JAXBException e) {
			e.printStackTrace();
		} catch (TransformerException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (ParserConfigurationException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} 
		return null;
	}

}
