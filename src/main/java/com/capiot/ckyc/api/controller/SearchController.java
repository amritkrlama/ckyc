package com.capiot.ckyc.api.controller;

import java.io.IOException;

import javax.xml.transform.TransformerException;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;
import org.w3c.dom.Document;

import com.capiot.ckyc.api.download.request.DownloadRequestReqRoot;
import com.capiot.ckyc.api.download.request.DownloadRequestService;
import com.capiot.ckyc.api.search.request.SearchRequestService;
import com.capiot.ckyc.api.search.service.SearchInHubService;
import com.capiot.ckyc.api.support.CommonService;
import com.capiot.ckyc.bulkfile.exception.FieldMissingException;
import com.capiot.ckyc.bulkfile.exception.InvalidRequestException;

@RestController
public class SearchController {
	
	public static final Logger logger = LoggerFactory.getLogger(SearchController.class);
	
	@Autowired
	SearchRequestService searchRequestService;
	@Autowired
	DownloadRequestService downloadRequestService;
	@Autowired
	SearchInHubService searchHub;
	private static final String CERSAI = "Cersai";
	private static final String DCH = "DCH";
	JSONParser parser;
	
	@PostMapping(value="/api/searchCkyc")
	public Object searchCkyc(@RequestBody String request) throws IOException, TransformerException, FieldMissingException, InvalidRequestException, ParseException {
		 logger.info("Received request for Search {}", request);
		 ResponseEntity<JSONObject> result =null;
		 parser = new JSONParser();
		 JSONObject requestAsJSON = (JSONObject)parser.parse(request);
		 String type = (String)requestAsJSON.get("type");
		 if(type==null)
			 throw new InvalidRequestException("Search type is missing");
		 
		 if(type.equalsIgnoreCase(CERSAI)) {
			 String searchRequest = searchRequestService.createSearchRequest(request);
			 Document searchRequestAsXML = CommonService.convertStringToDocument(searchRequest);
			 Document signedRequest = searchRequestService.signRequest(searchRequestAsXML);
			 CommonService.printDocument(signedRequest, System.out);
			 return searchRequestService.signRequest(signedRequest);
		 }else if(type.equalsIgnoreCase(DCH)){
			 JSONArray response = null;
			 String searchBy = (String) requestAsJSON.get("searchBy");
			 if(searchBy==null)
				 throw new InvalidRequestException("Search By is missing");
			 switch(searchBy) {
			 case "Identity": response = searchHub.searchByIdentity(requestAsJSON); 
			 				  return response; 
			 case "CKYC Number": response = searchHub.searchByCKYCNumber(requestAsJSON);
			 				  return response;
			 case "Name": response = searchHub.searchByName(requestAsJSON);
			 				  return response;
			 				
			 }
		 }else {
			 logger.debug("Received search type {}", type);
			 throw new InvalidRequestException("Invalid Search type");
		 }
		 
		 return result;
	}
	
	
	@PostMapping(value="/api/downloadCkyc", produces="application/xml")
	public DownloadRequestReqRoot downloadCkyc(@RequestBody String request) {
		return downloadRequestService.createDownloadRequest(request);
	}
}
