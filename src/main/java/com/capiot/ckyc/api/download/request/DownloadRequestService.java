package com.capiot.ckyc.api.download.request;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.UnsupportedEncodingException;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;

import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import org.springframework.core.env.Environment;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.capiot.ckyc.api.support.CommonService;
import com.capiot.ckyc.api.support.RequestSecureService;


@RestController
@Configuration
@PropertySource("classpath:ckyc.properties")
public class DownloadRequestService {

	@Autowired
	Environment environment;
	@Autowired
	RequestSecureService requestSecureService;
	@Autowired
	InitializeDownloadTagsService initializeTagsService;

	private static final Logger logger = LoggerFactory.getLogger(DownloadRequestService.class);
	public static final String CKYC_NO = "ckycNumber";
	public static final String DOB = "dob";

	
	public static String pidAsStringXml=null;
	public static String rootAsStringXml=null;
	public static String sessionKey = null;
	public static ByteArrayOutputStream pidStream = null;
	public static ByteArrayOutputStream rootStream = null;
	
	public static DownloadRequestReqRoot reqRoot;
	public static DownloadRequestHeader header;
	public static DownloadRequestPID pid;
	public static DownloadRequestPIDData pidData;
	public static DownloadRequestCkycInq ckycInq;

	
	/**
	 * @param Request received from UI
	 * Initializes all the Tags and Marshal the Download request.
	 */
	public DownloadRequestReqRoot createDownloadRequest(@RequestBody String payload) {

		JSONObject payloadAsJSON = convertRequestToJSONObject(payload);
		
		// Fetching ID Type and ID Number
		String ckycNumber = (String) payloadAsJSON.get(CKYC_NO);
		String dateOfBirth = (String) payloadAsJSON.get(DOB);

		logger.info("Recieved ID Type: {}", ckycNumber);
		logger.info("Recieved ID Number: {}", dateOfBirth);
		
		
		//Generating a Session Key
		sessionKey = requestSecureService.generateSessionKey();
		
		
		//Initializing all the tag objects
		initializeTagsService.initializePIDDataAndMarshal(ckycNumber, dateOfBirth);
		initializeTagsService.initializePID(pidData);
		initializeTagsService.intializeCkycInq(requestSecureService.encryptSessionKeyUsingCersaiPublicKey(environment.getProperty("cersaiPublicKey"), sessionKey),
													requestSecureService.encryptPID(sessionKey, pidAsStringXml)); 
		initializeTagsService.intializeHeader("IN106", "02", "1.0");
		initializeTagsService.initializeReqRoot(header, ckycInq);
		

		// Marshaling and storing the requests in strings
		try {
			rootStream = new ByteArrayOutputStream();
			
			JAXBContext context = JAXBContext.newInstance(DownloadRequestReqRoot.class);
			Marshaller marshaller = context.createMarshaller();
			marshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);				
			marshaller.marshal(reqRoot, rootStream);
			rootAsStringXml = rootStream.toString("UTF-8");
//			logger.info("Download request: {}{}",System.lineSeparator(),rootAsStringXml);
		} catch (JAXBException exception) {
			logger.debug("Exception while Marshaling");
			exception.printStackTrace();
		} catch (UnsupportedEncodingException e) {
			logger.debug("Error while Marshaling");
			e.printStackTrace();
		}finally {
			try {
				if(rootStream!=null)
				rootStream.close();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		
		logger.info("Decrypted PID: {}",CommonService.decrypt(requestSecureService.encryptPID(sessionKey, pidAsStringXml), sessionKey));
		logger.info(requestSecureService.digitalSign(rootAsStringXml, environment.getProperty("keystoreName"), "keystore", "keystore"));
		
		return reqRoot;
	}


	
	
	public JSONObject convertRequestToJSONObject(String payload) {
		JSONParser parser = new JSONParser();
		JSONObject payloadAsJSON = null;
		try {
			payloadAsJSON = (JSONObject) parser.parse(payload);
		} catch (ParseException e) {
			logger.debug("Unable to parse the request received for Download CKYC");
			e.printStackTrace();
		}

		if (payloadAsJSON != null) {
			logger.info("Recieved request for Downloading CKYC: {}", payloadAsJSON.toJSONString());
		} else {
			logger.info("Empty request received for Downloading CKYC");
		}
		
		return payloadAsJSON;
	}

}
