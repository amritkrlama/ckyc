package com.capiot.ckyc.api.download.response;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

import org.springframework.stereotype.Component;

@Component
@XmlRootElement(name="HEADER")
@XmlAccessorType(XmlAccessType.FIELD)
public class DownloadResponseHeader {
	
	@XmlElement(name="FI_CODE")
	String fiCode;
	@XmlElement(name="REQUEST_ID")
	String requestId;
	@XmlElement(name="REQ_DATE")
	String requestDate;
	@XmlElement(name="VERSION")
	String version;
	
	public DownloadResponseHeader() {
		super();
	}

	public DownloadResponseHeader(String fiCode, String requestId, String requestDate, String version) {
		super();
		this.fiCode = fiCode;
		this.requestId = requestId;
		this.requestDate = requestDate;
		this.version = version;
	}

	public String getFiCode() {
		return fiCode;
	}

	public void setFiCode(String fiCode) {
		this.fiCode = fiCode;
	}

	public String getRequestId() {
		return requestId;
	}
	
	public void setRequestId(String requestId) {
		this.requestId = requestId;
	}

	public String getVersion() {
		return version;
	}
	
	public void setVersion(String version) {
		this.version = version;
	}

	public String getRequestDate() {
		return requestDate;
	}

	public void setRequestDate(String requestDate) {
		this.requestDate = requestDate;
	}

	@Override
	public String toString() {
		return "Header [fiCode=" + fiCode + ", requestId=" + requestId + ", version=" + version + "]";
	}
	
	
	
}