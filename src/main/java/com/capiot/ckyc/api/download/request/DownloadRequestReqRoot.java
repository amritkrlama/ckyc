package com.capiot.ckyc.api.download.request;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

import org.springframework.stereotype.Component;

@Component
@XmlRootElement(name="CKYC_DOWNLOAD_REQUEST")
@XmlAccessorType(XmlAccessType.FIELD)
public class DownloadRequestReqRoot {

	@XmlElement(name="HEADER", type=DownloadRequestHeader.class)
	DownloadRequestHeader DownloadRequestHeader;

	@XmlElement(name="CKYC_INQ", type=DownloadRequestCkycInq.class)
	DownloadRequestCkycInq ckycInq;
	
	public DownloadRequestReqRoot(DownloadRequestHeader DownloadRequestHeader, DownloadRequestCkycInq ckycInq) {
		super();
		this.DownloadRequestHeader = DownloadRequestHeader;
		this.ckycInq = ckycInq;
	}

	public DownloadRequestReqRoot() {
		super();
	}

	public DownloadRequestHeader getHeader() {
		return DownloadRequestHeader;
	}

	public void setHeader(DownloadRequestHeader DownloadRequestHeader) {
		this.DownloadRequestHeader = DownloadRequestHeader;
	}

	public DownloadRequestCkycInq getCkycInq() {
		return ckycInq;
	}

	public void setCkycInq(DownloadRequestCkycInq ckycInq) {
		this.ckycInq = ckycInq;
	}

	@Override
	public String toString() {
		return "ReqRoot [DownloadRequestHeader=" + DownloadRequestHeader + ", ckycInq=" + ckycInq + "]";
	}
	

}
