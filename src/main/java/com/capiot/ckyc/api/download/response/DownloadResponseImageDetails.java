package com.capiot.ckyc.api.download.response;

import java.util.List;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

import org.springframework.stereotype.Component;

@Component
@XmlRootElement(name="IMAGE_DETAILS")
@XmlAccessorType(XmlAccessType.FIELD)	
public class DownloadResponseImageDetails {
	
	@XmlElement(name="IMAGE", type=DownloadResponseImage.class)
	List<DownloadResponseImage> images;

	public DownloadResponseImageDetails() {
		super();
	}

	public DownloadResponseImageDetails(List<DownloadResponseImage> images) {
		super();
		this.images = images;
	}

	public List<DownloadResponseImage> getImages() {
		return images;
	}

	public void setImages(List<DownloadResponseImage> images) {
		this.images = images;
	}
	
	
}
