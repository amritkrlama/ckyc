package com.capiot.ckyc.api.download.request;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

import org.springframework.stereotype.Component;

@Component
@XmlRootElement(name="PID_DATA")
@XmlAccessorType(XmlAccessType.FIELD)
public class DownloadRequestPIDData {
	
	@XmlElement(name="DATE_TIME")
	String dateTime;
	@XmlElement(name="CKYC_NO")
	String ckycNumber;
	@XmlElement(name="DOB")
	String dateOfBirth;
	
	public DownloadRequestPIDData(String dateTime, String ckycNumber, String dateOfBirth) {
		super();
		this.dateTime = dateTime;
		this.ckycNumber = ckycNumber;
		this.dateOfBirth = dateOfBirth;
	}

	public DownloadRequestPIDData() {
		super();
	}

	public String getDateTime() {
		return dateTime;
	}

	public void setDateTime(String dateTime) {
		this.dateTime = dateTime;
	}

	public String getCkycNumber() {
		return ckycNumber;
	}

	public void setCkycNumber(String ckycNumber) {
		this.ckycNumber = ckycNumber;
	}

	public String getDateOfBirth() {
		return dateOfBirth;
	}

	public void setDateOfBirth(String dateOfBirth) {
		this.dateOfBirth = dateOfBirth;
	}

	@Override
	public String toString() {
		return "PIDData [dateTime=" + dateTime + ", idType=" + ckycNumber + ", idNumber=" + dateOfBirth + "]";
	}
	
	
}
