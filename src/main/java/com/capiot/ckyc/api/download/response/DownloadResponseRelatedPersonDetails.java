package com.capiot.ckyc.api.download.response;

import java.util.List;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

import org.springframework.stereotype.Component;

@Component
@XmlRootElement(name="RELATED_PERSON_DETAILS")
@XmlAccessorType(XmlAccessType.FIELD)
public class DownloadResponseRelatedPersonDetails {
	
	@XmlElement(name="RELATED_PERSON",type = DownloadResponseRelatedPerson.class)
	List<DownloadResponseRelatedPerson> relatedPersons;

	public DownloadResponseRelatedPersonDetails(List<DownloadResponseRelatedPerson> downloadResponseRelatedPerson) {
		super();
		this.relatedPersons = downloadResponseRelatedPerson;
	}

	public DownloadResponseRelatedPersonDetails() {
		super();
	}

	public List<DownloadResponseRelatedPerson> getDownloadResponseRelatedPerson() {
		return relatedPersons;
	}

	public void setDownloadResponseRelatedPerson(List<DownloadResponseRelatedPerson> downloadResponseRelatedPerson) {
		this.relatedPersons = downloadResponseRelatedPerson;
	}

}	
