package com.capiot.ckyc.api.download.response;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

import org.springframework.stereotype.Component;

@Component
@XmlRootElement(name="CKYC_INQ")
@XmlAccessorType(XmlAccessType.FIELD)
public class DownloadResponseCkycInq {
	
	@XmlElement(name="SESSION_KEY")
	String sessionKey;
	
	@XmlElement(name="PID", type=DownloadResponsePID.class)
	DownloadResponsePID pid;
	
	@XmlElement(name="ERROR")
	String error;

	public DownloadResponseCkycInq(String sessionKey, DownloadResponsePID pid, String error) {
		super();
		this.sessionKey = sessionKey;
		this.pid = pid;
		this.error = error;
	}

	public DownloadResponseCkycInq() {
		super();
	}

	public String getSessionKey() {
		return sessionKey;
	}

	public void setSessionKey(String sessionKey) {
		this.sessionKey = sessionKey;
	}

	public DownloadResponsePID getPid() {
		return pid;
	}

	public void setPid(DownloadResponsePID pid) {
		this.pid = pid;
	}

	public String getError() {
		return error;
	}

	public void setError(String error) {
		this.error = error;
	}
	
	

}
