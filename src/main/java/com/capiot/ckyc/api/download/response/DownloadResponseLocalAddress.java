package com.capiot.ckyc.api.download.response;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

import org.springframework.stereotype.Component;

@Component
@XmlRootElement(name="LOCAL_ADDRESS")
@XmlAccessorType(XmlAccessType.FIELD)	
public class DownloadResponseLocalAddress {
	
	@XmlElement(name="SEQUENCE_NO")
	String sequenceNumber;
	
	@XmlElement(name="BRANCH_CODE")
	String branchCode;
	
	@XmlElement(name="ADDR_LINE1")
	String addressLine1;
	
	@XmlElement(name="ADDR_LINE2")
	String addressLine2;
	
	@XmlElement(name="ADDR_LINE3")
	String addressLine3;
	
	@XmlElement(name="ADDR_CITY")
	String addressCity;
	
	@XmlElement(name="ADDR_DIST")
	String addressDistrict;
	
	@XmlElement(name="ADDR_PIN")
	String addressPin;
	
	@XmlElement(name="ADDR_STATE")
	String addressState;
	
	@XmlElement(name="ADDR_COUNTRY")
	String addressCountry;
	
	@XmlElement(name="RESI_STD_CODE")
	String residentialSTDCode;
	
	@XmlElement(name="RESI_TEL_NUM")
	String residentialTelephoneNumber;
	
	@XmlElement(name="OFF_STD_CODE")
	String officeSTDCode;
	
	@XmlElement(name="OFF_TEL_NUM")
	String officeTelephoneNumber;
	
	@XmlElement(name="MOB_CODE")
	String mobileCode;
	
	@XmlElement(name="MOB_NUM")
	String mobileNumber;
	
	@XmlElement(name="FAX_CODE")
	String faxCode;
	
	@XmlElement(name="FAX_NO")
	String faxNumber;
	
	@XmlElement(name="EMAIL")
	String email;
	
	@XmlElement(name="DEC_DATE")
	String declaredDate;
	
	@XmlElement(name="DEC_PLACE")
	String declatedPlace;

	public DownloadResponseLocalAddress() {
		super();
	}

	public DownloadResponseLocalAddress(String sequenceNumber, String branchCode, String addressLine1,
			String addressLine2, String addressLine3, String addressCity, String addressDistrict, String addressPin,
			String addressState, String addressCountry, String residentialSTDCode, String residentialTelephoneNumber,
			String officeSTDCode, String officeTelephoneNumber, String mobileCode, String mobileNumber, String faxCode,
			String faxNumber, String email, String declaredDate, String declatedPlace) {
		super();
		this.sequenceNumber = sequenceNumber;
		this.branchCode = branchCode;
		this.addressLine1 = addressLine1;
		this.addressLine2 = addressLine2;
		this.addressLine3 = addressLine3;
		this.addressCity = addressCity;
		this.addressDistrict = addressDistrict;
		this.addressPin = addressPin;
		this.addressState = addressState;
		this.addressCountry = addressCountry;
		this.residentialSTDCode = residentialSTDCode;
		this.residentialTelephoneNumber = residentialTelephoneNumber;
		this.officeSTDCode = officeSTDCode;
		this.officeTelephoneNumber = officeTelephoneNumber;
		this.mobileCode = mobileCode;
		this.mobileNumber = mobileNumber;
		this.faxCode = faxCode;
		this.faxNumber = faxNumber;
		this.email = email;
		this.declaredDate = declaredDate;
		this.declatedPlace = declatedPlace;
	}

	public String getSequenceNumber() {
		return sequenceNumber;
	}

	public void setSequenceNumber(String sequenceNumber) {
		this.sequenceNumber = sequenceNumber;
	}

	public String getBranchCode() {
		return branchCode;
	}

	public void setBranchCode(String branchCode) {
		this.branchCode = branchCode;
	}

	public String getAddressLine1() {
		return addressLine1;
	}

	public void setAddressLine1(String addressLine1) {
		this.addressLine1 = addressLine1;
	}

	public String getAddressLine2() {
		return addressLine2;
	}

	public void setAddressLine2(String addressLine2) {
		this.addressLine2 = addressLine2;
	}

	public String getAddressLine3() {
		return addressLine3;
	}

	public void setAddressLine3(String addressLine3) {
		this.addressLine3 = addressLine3;
	}

	public String getAddressCity() {
		return addressCity;
	}

	public void setAddressCity(String addressCity) {
		this.addressCity = addressCity;
	}

	public String getAddressDistrict() {
		return addressDistrict;
	}

	public void setAddressDistrict(String addressDistrict) {
		this.addressDistrict = addressDistrict;
	}

	public String getAddressPin() {
		return addressPin;
	}

	public void setAddressPin(String addressPin) {
		this.addressPin = addressPin;
	}

	public String getAddressState() {
		return addressState;
	}

	public void setAddressState(String addressState) {
		this.addressState = addressState;
	}

	public String getAddressCountry() {
		return addressCountry;
	}

	public void setAddressCountry(String addressCountry) {
		this.addressCountry = addressCountry;
	}

	public String getResidentialSTDCode() {
		return residentialSTDCode;
	}

	public void setResidentialSTDCode(String residentialSTDCode) {
		this.residentialSTDCode = residentialSTDCode;
	}

	public String getResidentialTelephoneNumber() {
		return residentialTelephoneNumber;
	}

	public void setResidentialTelephoneNumber(String residentialTelephoneNumber) {
		this.residentialTelephoneNumber = residentialTelephoneNumber;
	}

	public String getOfficeSTDCode() {
		return officeSTDCode;
	}

	public void setOfficeSTDCode(String officeSTDCode) {
		this.officeSTDCode = officeSTDCode;
	}

	public String getOfficeTelephoneNumber() {
		return officeTelephoneNumber;
	}

	public void setOfficeTelephoneNumber(String officeTelephoneNumber) {
		this.officeTelephoneNumber = officeTelephoneNumber;
	}

	public String getMobileCode() {
		return mobileCode;
	}

	public void setMobileCode(String mobileCode) {
		this.mobileCode = mobileCode;
	}

	public String getMobileNumber() {
		return mobileNumber;
	}

	public void setMobileNumber(String mobileNumber) {
		this.mobileNumber = mobileNumber;
	}

	public String getFaxCode() {
		return faxCode;
	}

	public void setFaxCode(String faxCode) {
		this.faxCode = faxCode;
	}

	public String getFaxNumber() {
		return faxNumber;
	}

	public void setFaxNumber(String faxNumber) {
		this.faxNumber = faxNumber;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getDeclaredDate() {
		return declaredDate;
	}

	public void setDeclaredDate(String declaredDate) {
		this.declaredDate = declaredDate;
	}

	public String getDeclatedPlace() {
		return declatedPlace;
	}

	public void setDeclatedPlace(String declatedPlace) {
		this.declatedPlace = declatedPlace;
	}
	
}
