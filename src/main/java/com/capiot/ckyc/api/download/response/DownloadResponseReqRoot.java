package com.capiot.ckyc.api.download.response;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

import org.springframework.stereotype.Component;

@Component
@XmlRootElement(name="CKYC_DOWNLOAD_RESPONSE")
@XmlAccessorType(XmlAccessType.FIELD)
public class DownloadResponseReqRoot {

	@XmlElement(name="HEADER", type=DownloadResponseHeader.class)
	DownloadResponseHeader downloadResponseHeader;

	@XmlElement(name="CKYC_INQ", type=DownloadResponseCkycInq.class)
	DownloadResponseCkycInq ckycInq;

	public DownloadResponseReqRoot(DownloadResponseHeader downloadResponseHeader, DownloadResponseCkycInq ckycInq) {
		super();
		this.downloadResponseHeader = downloadResponseHeader;
		this.ckycInq = ckycInq;
	}

	public DownloadResponseReqRoot() {
		super();
	}

	public DownloadResponseHeader getDownloadResponseHeader() {
		return downloadResponseHeader;
	}

	public void setDownloadResponseHeader(DownloadResponseHeader downloadResponseHeader) {
		this.downloadResponseHeader = downloadResponseHeader;
	}

	public DownloadResponseCkycInq getCkycInq() {
		return ckycInq;
	}

	public void setCkycInq(DownloadResponseCkycInq ckycInq) {
		this.ckycInq = ckycInq;
	}
	
}