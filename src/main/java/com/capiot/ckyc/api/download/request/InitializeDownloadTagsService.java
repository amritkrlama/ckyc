package com.capiot.ckyc.api.download.request;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.TimeZone;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Component;

import com.capiot.ckyc.api.support.RequestSecureService;

@Component
@Configuration
@PropertySource("classpath:ckyc.properties")
public class InitializeDownloadTagsService {
		
	@Autowired
	Environment environment;
	@Autowired
	RequestSecureService requestSecureService;
	
	public static final Logger logger = LoggerFactory.getLogger(InitializeDownloadTagsService.class);

	
	// To initialize and Marshal the PID Data part 
		public void initializePIDDataAndMarshal(String idType, String idNumber) {
			intializePidData(getCurrentDateTime(), idType, idNumber);
			
			DownloadRequestService.pidStream = new ByteArrayOutputStream();
			try {
				JAXBContext context = JAXBContext.newInstance(DownloadRequestPIDData.class);
				Marshaller marshaller = context.createMarshaller();
				marshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);
				marshaller.marshal(DownloadRequestService.pidData, DownloadRequestService.pidStream);
				DownloadRequestService.pidAsStringXml = DownloadRequestService.pidStream.toString("UTF-8");
				logger.info("PID Data: {}{}", System.lineSeparator(), DownloadRequestService.pidAsStringXml);
			} catch (JAXBException exception) {
				logger.debug("Exception while Marshaling");
				exception.printStackTrace();
			} catch (UnsupportedEncodingException e) {
				logger.debug("Error while Marshaling");
				e.printStackTrace();
			}finally {
				try {
					if(DownloadRequestService.pidStream!=null)
					DownloadRequestService.pidStream.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		
		}
		
		public void intializeHeader(String fiCode, String requestId, String version) {
			DownloadRequestService.header = new DownloadRequestHeader();
			DownloadRequestService.header.setFiCode(fiCode);
			DownloadRequestService.header.setRequestId(requestId);
			DownloadRequestService.header.setVersion(version);
		}

		public void intializePidData(String dateTime, String ckycNumber, String dateOfBirth) {
			DownloadRequestService.pidData = new DownloadRequestPIDData();
			DownloadRequestService.pidData.setDateTime(dateTime);
			DownloadRequestService.pidData.setCkycNumber(ckycNumber);
			DownloadRequestService.pidData.setDateOfBirth(dateOfBirth);
		}

		public void initializePID(DownloadRequestPIDData pidData) {
			DownloadRequestService.pid = new DownloadRequestPID();
			DownloadRequestService.pid.setPidData(pidData);
		}

		public void intializeCkycInq(String string, String pid) {
			DownloadRequestService.ckycInq = new DownloadRequestCkycInq();
			DownloadRequestService.ckycInq.setSessionKey(string);
			DownloadRequestService.ckycInq.setPid(pid);
		}

		public void initializeReqRoot(DownloadRequestHeader header, DownloadRequestCkycInq ckycInq) {
			DownloadRequestService.reqRoot = new DownloadRequestReqRoot();
			DownloadRequestService.reqRoot.setCkycInq(ckycInq);
			DownloadRequestService.reqRoot.setHeader(header);
		}

		
		
		/* Method to return current date time */
		public String getCurrentDateTime() {
			String indianTimeZone = environment.getProperty("timeZone");
			SimpleDateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy HH:mm:ss");
			dateFormat.setTimeZone(TimeZone.getTimeZone(indianTimeZone));
			Date now = new Date();
			String currentDateTime = dateFormat.format(now);
			return currentDateTime;
		}
}
