package com.capiot.ckyc.api.download.response;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

import org.springframework.stereotype.Component;

@Component
@XmlRootElement(name="PID_DATA")
@XmlAccessorType(XmlAccessType.FIELD)
public class DownloadResponsePIDData {
	
	@XmlElement(name="PERSONAL_DETAILS", type=DownloadResponsePersonalDetails.class)
	DownloadResponsePersonalDetails personalDetail;
	
	@XmlElement(name="IDENTITY_DETAILS", type=DownloadResponseIdentityDetails.class)
	DownloadResponseIdentityDetails identityDetail;
	
	@XmlElement(name="RELATED_PERSON_DETAILS", type=DownloadResponseRelatedPersonDetails.class)
	DownloadResponseRelatedPersonDetails relatedPersonDetail;
	
	@XmlElement(name="LOCAL_ADDRESS_DETAILS", type=DownloadResponseLocalAddressDetails.class)
	DownloadResponseLocalAddressDetails localAddressDetail;
	
	@XmlElement(name="IMAGE_DETAILS", type=DownloadResponseImageDetails.class)
	DownloadResponseImageDetails imageDetail;

	public DownloadResponsePIDData(DownloadResponsePersonalDetails personalDetail,
			DownloadResponseIdentityDetails identityDetail, DownloadResponseRelatedPersonDetails relatedPersonDetail,
			DownloadResponseLocalAddressDetails localAddressDetail, DownloadResponseImageDetails imageDetail) {
		super();
		this.personalDetail = personalDetail;
		this.identityDetail = identityDetail;
		this.relatedPersonDetail = relatedPersonDetail;
		this.localAddressDetail = localAddressDetail;
		this.imageDetail = imageDetail;
	}

	public DownloadResponsePIDData() {
		super();
	}

	public DownloadResponsePersonalDetails getPersonalDetail() {
		return personalDetail;
	}

	public void setPersonalDetail(DownloadResponsePersonalDetails personalDetail) {
		this.personalDetail = personalDetail;
	}

	public DownloadResponseIdentityDetails getIdentityDetail() {
		return identityDetail;
	}

	public void setIdentityDetail(DownloadResponseIdentityDetails identityDetail) {
		this.identityDetail = identityDetail;
	}

	public DownloadResponseRelatedPersonDetails getRelatedPersonDetail() {
		return relatedPersonDetail;
	}

	public void setRelatedPersonDetail(DownloadResponseRelatedPersonDetails relatedPersonDetail) {
		this.relatedPersonDetail = relatedPersonDetail;
	}

	public DownloadResponseLocalAddressDetails getLocalAddressDetail() {
		return localAddressDetail;
	}

	public void setLocalAddressDetail(DownloadResponseLocalAddressDetails localAddressDetail) {
		this.localAddressDetail = localAddressDetail;
	}

	public DownloadResponseImageDetails getImageDetail() {
		return imageDetail;
	}

	public void setImageDetail(DownloadResponseImageDetails imageDetail) {
		this.imageDetail = imageDetail;
	}
	
	
}
