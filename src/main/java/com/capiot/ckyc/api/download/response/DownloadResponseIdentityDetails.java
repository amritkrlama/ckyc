package com.capiot.ckyc.api.download.response;

import java.util.List;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

import org.springframework.stereotype.Component;

@Component
@XmlRootElement(name="IDENTITY_DETAILS")
@XmlAccessorType(XmlAccessType.FIELD)
public class DownloadResponseIdentityDetails {
	
	@XmlElement(name="IDENTITY", type=DownloadResponseIdentity.class)
	List<DownloadResponseIdentity> identities;

	public DownloadResponseIdentityDetails(List<DownloadResponseIdentity> identity) {
		super();
		this.identities = identity;
	}

	public DownloadResponseIdentityDetails() {
		super();
	}

	public List<DownloadResponseIdentity> getIdentity() {
		return identities;
	}

	public void setIdentity(List<DownloadResponseIdentity> identity) {
		this.identities = identity;
	}
	
}
