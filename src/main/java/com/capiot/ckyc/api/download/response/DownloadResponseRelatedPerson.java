package com.capiot.ckyc.api.download.response;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

import org.springframework.stereotype.Component;

@Component
@XmlRootElement(name="RELATED_PERSON")
@XmlAccessorType(XmlAccessType.FIELD)
public class DownloadResponseRelatedPerson {
	
	@XmlElement(name="SEQUENCE_NO")
	String sequenceNumber;
	
	@XmlElement(name="REL_TYPE")
	String relationType;
	
	@XmlElement(name="ADD_DEL_FLAG")
	String addDeleteFlag;
	
	@XmlElement(name="CKYC_NO")
	String ckycNumber;
	
	@XmlElement(name="PREFIX")
	String prefix;
	
	@XmlElement(name="FNAME")
	String firstName;
	
	@XmlElement(name="MNAME")
	String middleName;
	
	@XmlElement(name="LNAME")
	String lastName;
	
	@XmlElement(name="MAIDEN_PREFIX")
	String maidenPrefix;
	
	@XmlElement(name="MAIDEN_FNAME")
	String maidenFirstName;
	
	@XmlElement(name="MAIDEN_MNAME")
	String maidenMiddleName;
	
	@XmlElement(name="MAIDEN_LNAME")
	String maidenLastName;
	
	@XmlElement(name="FATHERSPOUSE_FLAG")
	String fatherSpouseFlag;
	
	@XmlElement(name="FATHER_PREFIX")
	String fatherPrefix;
	
	@XmlElement(name="FATHER_FNAME")
	String fatherFirstName;
	
	@XmlElement(name="FATHER_MNAME")
	String fatherMiddleName;
	
	@XmlElement(name="FATHER_LNAME")
	String fatherLastName;
	
	@XmlElement(name="MOTHER_PREFIX")
	String motherPrefix;
	
	@XmlElement(name="MOTHER_FNAME")
	String motherFirstName;
	
	@XmlElement(name="MOTHER_MNAME")
	String motherMiddleName;
	
	@XmlElement(name="MOTHER_LNAME")
	String motherLastName;
	
	@XmlElement(name="DOB")
	String dob;
	
	@XmlElement(name="GENDER")
	String gender;
	
	@XmlElement(name="MARITAL_STATUS")
	String maritalStatus;
	
	@XmlElement(name="NATIONALITY")
	String nationality;
	
	@XmlElement(name="RESI_STATUS")
	String residentialStatus;
	
	@XmlElement(name="OCCUPATION")
	String occupation;
	
	@XmlElement(name="PAN")
	String pan;
	
	@XmlElement(name="UID")
	String uid;
	
	@XmlElement(name="VOTER_ID")
	String voterID;
	
	@XmlElement(name="NREGA")
	String nrega;
	
	@XmlElement(name="PASSPORT")
	String passport;
	
	@XmlElement(name="PASSPORT_EXP")
	String passportExpiry;
	
	@XmlElement(name="DRIVING_LICENSE")
	String drivingLicense;
	
	@XmlElement(name="DRIVING_EXP")
	String drivingExpiry;
	
	@XmlElement(name="OTHERID_NAME")
	String otherIdName;
	
	@XmlElement(name="OTHERID_NO")
	String otherIdNumber;
	
	@XmlElement(name="SIMPLIFIED_CODE")
	String simplifiedCode;
	
	@XmlElement(name="SIMPLIFIED_NO")
	String simplifiedNumber;
	
	@XmlElement(name="DEC_DATE")
	String declaredDate;
	
	@XmlElement(name="DEC_PLACE")
	String declaredPlace;
	
	@XmlElement(name="KYC_DATE")
	String kycDate;
	
	@XmlElement(name="DOC_SUB")
	String documentSubmitted;
	
	@XmlElement(name="KYC_NAME")
	String kycEmployeeName;
	
	@XmlElement(name="KYC_DESIGNATION")
	String kycEmployeedesignation;
	
	@XmlElement(name="KYC_BRANCH")
	String kycEmployeeBranch;
	
	@XmlElement(name="KYC_EMPCODE")
	String kycEmployeeCode;
	
	@XmlElement(name="ORG_NAME")
	String organisationName;
	
	@XmlElement(name="ORG_CODE")
	String organisationCode;

	public DownloadResponseRelatedPerson() {
		super();
	}

	public String getSequenceNumber() {
		return sequenceNumber;
	}

	public void setSequenceNumber(String sequenceNumber) {
		this.sequenceNumber = sequenceNumber;
	}

	public String getRelationType() {
		return relationType;
	}

	public void setRelationType(String relationType) {
		this.relationType = relationType;
	}

	public String getAddDeleteFlag() {
		return addDeleteFlag;
	}

	public void setAddDeleteFlag(String addDeleteFlag) {
		this.addDeleteFlag = addDeleteFlag;
	}

	public String getCkycNumber() {
		return ckycNumber;
	}

	public void setCkycNumber(String ckycNumber) {
		this.ckycNumber = ckycNumber;
	}

	public String getPrefix() {
		return prefix;
	}

	public void setPrefix(String prefix) {
		this.prefix = prefix;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getMiddleName() {
		return middleName;
	}

	public void setMiddleName(String middleName) {
		this.middleName = middleName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String getMaidenPrefix() {
		return maidenPrefix;
	}

	public void setMaidenPrefix(String maidenPrefix) {
		this.maidenPrefix = maidenPrefix;
	}

	public String getMaidenFirstName() {
		return maidenFirstName;
	}

	public void setMaidenFirstName(String maidenFirstName) {
		this.maidenFirstName = maidenFirstName;
	}

	public String getMaidenMiddleName() {
		return maidenMiddleName;
	}

	public void setMaidenMiddleName(String maidenMiddleName) {
		this.maidenMiddleName = maidenMiddleName;
	}

	public String getMaidenLastName() {
		return maidenLastName;
	}

	public void setMaidenLastName(String maidenLastName) {
		this.maidenLastName = maidenLastName;
	}

	public String getFatherSpouseFlag() {
		return fatherSpouseFlag;
	}

	public void setFatherSpouseFlag(String fatherSpouseFlag) {
		this.fatherSpouseFlag = fatherSpouseFlag;
	}

	public String getFatherPrefix() {
		return fatherPrefix;
	}

	public void setFatherPrefix(String fatherPrefix) {
		this.fatherPrefix = fatherPrefix;
	}

	public String getFatherFirstName() {
		return fatherFirstName;
	}

	public void setFatherFirstName(String fatherFirstName) {
		this.fatherFirstName = fatherFirstName;
	}

	public String getFatherMiddleName() {
		return fatherMiddleName;
	}

	public void setFatherMiddleName(String fatherMiddleName) {
		this.fatherMiddleName = fatherMiddleName;
	}

	public String getFatherLastName() {
		return fatherLastName;
	}

	public void setFatherLastName(String fatherLastName) {
		this.fatherLastName = fatherLastName;
	}

	public String getMotherPrefix() {
		return motherPrefix;
	}

	public void setMotherPrefix(String motherPrefix) {
		this.motherPrefix = motherPrefix;
	}

	public String getMotherFirstName() {
		return motherFirstName;
	}

	public void setMotherFirstName(String motherFirstName) {
		this.motherFirstName = motherFirstName;
	}

	public String getMotherMiddleName() {
		return motherMiddleName;
	}

	public void setMotherMiddleName(String motherMiddleName) {
		this.motherMiddleName = motherMiddleName;
	}

	public String getMotherLastName() {
		return motherLastName;
	}

	public void setMotherLastName(String motherLastName) {
		this.motherLastName = motherLastName;
	}

	public String getDob() {
		return dob;
	}

	public void setDob(String dob) {
		this.dob = dob;
	}

	public String getGender() {
		return gender;
	}

	public void setGender(String gender) {
		this.gender = gender;
	}

	public String getMaritalStatus() {
		return maritalStatus;
	}

	public void setMaritalStatus(String maritalStatus) {
		this.maritalStatus = maritalStatus;
	}

	public String getNationality() {
		return nationality;
	}

	public void setNationality(String nationality) {
		this.nationality = nationality;
	}

	public String getResidentialStatus() {
		return residentialStatus;
	}

	public void setResidentialStatus(String residentialStatus) {
		this.residentialStatus = residentialStatus;
	}

	public String getOccupation() {
		return occupation;
	}

	public void setOccupation(String occupation) {
		this.occupation = occupation;
	}

	public String getPan() {
		return pan;
	}

	public void setPan(String pan) {
		this.pan = pan;
	}

	public String getUid() {
		return uid;
	}

	public void setUid(String uid) {
		this.uid = uid;
	}

	public String getVoterID() {
		return voterID;
	}

	public void setVoterID(String voterID) {
		this.voterID = voterID;
	}

	public String getNrega() {
		return nrega;
	}

	public void setNrega(String nrega) {
		this.nrega = nrega;
	}

	public String getPassport() {
		return passport;
	}

	public void setPassport(String passport) {
		this.passport = passport;
	}

	public String getPassportExpiry() {
		return passportExpiry;
	}

	public void setPassportExpiry(String passportExpiry) {
		this.passportExpiry = passportExpiry;
	}

	public String getDrivingLicense() {
		return drivingLicense;
	}

	public void setDrivingLicense(String drivingLicense) {
		this.drivingLicense = drivingLicense;
	}

	public String getDrivingExpiry() {
		return drivingExpiry;
	}

	public void setDrivingExpiry(String drivingExpiry) {
		this.drivingExpiry = drivingExpiry;
	}

	public String getOtherIdName() {
		return otherIdName;
	}

	public void setOtherIdName(String otherIdName) {
		this.otherIdName = otherIdName;
	}

	public String getOtherIdNumber() {
		return otherIdNumber;
	}

	public void setOtherIdNumber(String otherIdNumber) {
		this.otherIdNumber = otherIdNumber;
	}

	public String getSimplifiedCode() {
		return simplifiedCode;
	}

	public void setSimplifiedCode(String simplifiedCode) {
		this.simplifiedCode = simplifiedCode;
	}

	public String getSimplifiedNumber() {
		return simplifiedNumber;
	}

	public void setSimplifiedNumber(String simplifiedNumber) {
		this.simplifiedNumber = simplifiedNumber;
	}

	public String getDeclaredDate() {
		return declaredDate;
	}

	public void setDeclaredDate(String declaredDate) {
		this.declaredDate = declaredDate;
	}

	public String getDeclaredPlace() {
		return declaredPlace;
	}

	public void setDeclaredPlace(String declaredPlace) {
		this.declaredPlace = declaredPlace;
	}

	public String getKycDate() {
		return kycDate;
	}

	public void setKycDate(String kycDate) {
		this.kycDate = kycDate;
	}

	public String getDocumentSubmitted() {
		return documentSubmitted;
	}

	public void setDocumentSubmitted(String documentSubmitted) {
		this.documentSubmitted = documentSubmitted;
	}

	public String getKycEmployeeName() {
		return kycEmployeeName;
	}

	public void setKycEmployeeName(String kycEmployeeName) {
		this.kycEmployeeName = kycEmployeeName;
	}

	public String getKycEmployeedesignation() {
		return kycEmployeedesignation;
	}

	public void setKycEmployeedesignation(String kycEmployeedesignation) {
		this.kycEmployeedesignation = kycEmployeedesignation;
	}

	public String getKycEmployeeBranch() {
		return kycEmployeeBranch;
	}

	public void setKycEmployeeBranch(String kycEmployeeBranch) {
		this.kycEmployeeBranch = kycEmployeeBranch;
	}

	public String getKycEmployeeCode() {
		return kycEmployeeCode;
	}

	public void setKycEmployeeCode(String kycEmployeeCode) {
		this.kycEmployeeCode = kycEmployeeCode;
	}

	public String getOrganisationName() {
		return organisationName;
	}

	public void setOrganisationName(String organisationName) {
		this.organisationName = organisationName;
	}

	public String getOrganisationCode() {
		return organisationCode;
	}

	public void setOrganisationCode(String organisationCode) {
		this.organisationCode = organisationCode;
	}

}
