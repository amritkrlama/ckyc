package com.capiot.ckyc.api.download.response;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

import org.springframework.stereotype.Component;

@Component
@XmlRootElement(name="PERSONAL_DETAILS")
@XmlAccessorType(XmlAccessType.FIELD)
public class DownloadResponsePersonalDetails {
	
	@XmlElement(name="CONSTI_TYPE")
	String constitutionType;
	
	@XmlElement(name="ACC_TYPE")
	String accountType;
	
	@XmlElement(name="CKYC_NO")
	String ckycNumber;
	
	@XmlElement(name="PREFIX")
	String prefix;
	
	@XmlElement(name="FNAME")
	String firstName;
	
	@XmlElement(name="MNAME")
	String middleName;
	
	@XmlElement(name="LNAME")
	String lastName;
	
	@XmlElement(name="FULLNAME")
	String fullName;
	
	@XmlElement(name="MAIDEN_PREFIX")
	String maidenPrefix;
	
	@XmlElement(name="MAIDEN_FNAME")
	String maidenFirstName;
	
	@XmlElement(name="MAIDEN_MNAME")
	String maidenMiddleName;
	
	@XmlElement(name="MAIDEN_LNAME")
	String maidenLastName;
	
	@XmlElement(name="MAIDEN_FULLNAME")
	String maidenFullName;
	
	@XmlElement(name="FATHERSPOUSE_FLAG")
	String fatherSpouseFlag;
	
	@XmlElement(name="FATHER_PREFIX")
	String fatherPrefix;
	
	@XmlElement(name="FATHER_FNAME")
	String fatherFirstName;
	
	@XmlElement(name="FATHER_MNAME")
	String fatherMiddleName;
	
	@XmlElement(name="FATHER_LNAME")
	String fatherLastName;
	
	@XmlElement(name="FATHER_FULLNAME")
	String fatherFullName;
	
	@XmlElement(name="MOTHER_PREFIX")
	String motherPrefix;
	
	@XmlElement(name="MOTHER_FNAME")
	String motherFirstName;
	
	@XmlElement(name="MOTHER_MNAME")
	String motherMiddleName;
	
	@XmlElement(name="MOTHER_LNAME")
	String motherLastName;
	
	@XmlElement(name="MOTHER_FULLNAME")
	String motherFullName;
	
	@XmlElement(name="GENDER")
	String gender;
	
	@XmlElement(name="MARITAL_STATUS")
	String maritalStatus;
	
	@XmlElement(name="NATIONALITY")
	String nationality;
	
	@XmlElement(name="OCCUPATION")
	String occupation;
	
	@XmlElement(name="DOB")
	String dateOfBirth;
	
	@XmlElement(name="RESI_STATUS")
	String residentialStatus;
	
	@XmlElement(name="JURI_FLAG")
	String jurisdictionFlag;
	
	@XmlElement(name="JURI_RESI")
	String jurisdictionResidence;
	
	@XmlElement(name="TAX_NUM")
	String taxNumber;
	
	@XmlElement(name="BIRTH_COUNTRY")
	String birthCountry;
	
	@XmlElement(name="BIRTH_PLACE")
	String birthPlace;
	
	@XmlElement(name="PERM_TYPE")
	String permAddressType;
	
	@XmlElement(name="PERM_LINE1")
	String permLine1;
	
	@XmlElement(name="PERM_LINE2")
	String permLine2;
	
	@XmlElement(name="PERM_LINE3")
	String permLine3;
	
	@XmlElement(name="PERM_CITY")
	String permCity;
	
	@XmlElement(name="PERM_DIST")
	String permDist;
	
	@XmlElement(name="PERM_STATE")
	String permState;
	
	@XmlElement(name="PERM_COUNTRY")
	String country;
	
	@XmlElement(name="PERM_PIN")
	String pincode;
	
	@XmlElement(name="PERM_POA")
	String permProofOfAddress;
	
	@XmlElement(name="PERM_POAOTHERS")
	String permProofOfAddressOthers;
	
	@XmlElement(name="PERM_CORRES_SAMEFLAG")
	String permCorressSameFlag;
	
	@XmlElement(name="CORRES_LINE1")
	String corresLine1;
	
	@XmlElement(name="CORRES_LINE2")
	String corresLine2;
	
	@XmlElement(name="CORRES_LINE3")
	String corresline3;
	
	@XmlElement(name="CORRES_CITY")
	String corresCity;
	
	@XmlElement(name="CORRES_DIST")
	String corresDist;
	
	@XmlElement(name="CORRES_STATE")
	String corresState;
	
	@XmlElement(name="CORRES_COUNTRY")
	String corresCountry;
	
	@XmlElement(name="CORRES_PIN")
	String corresPin;
	
	@XmlElement(name="JURI_SAME_FLAG")
	String juriSameFlag;
	
	@XmlElement(name="JURI_LINE1")
	String juriLine1;
	
	@XmlElement(name="JURI_LINE2")
	String juriLine2;
	
	@XmlElement(name="JURI_LINE3")
	String juriLine3;
	
	@XmlElement(name="JURI_CITY")
	String juriCity;
	
	@XmlElement(name="JURI_STATE")
	String juriState;
	
	@XmlElement(name="JURI_COUNTRY")
	String juriCountry;
	
	@XmlElement(name="JURI_PIN")
	String juriPin;
	
	@XmlElement(name="RESI_STD_CODE")
	String resiSTDCode;
	
	@XmlElement(name="RESI_TEL_NUM")
	String resiTELNumber;
	
	@XmlElement(name="OFF_STD_CODE")
	String offSTDCode;
	
	@XmlElement(name="OFF_TEL_NUM")
	String offTELNumber;
	
	@XmlElement(name="MOB_CODE")
	String mobCode;
	
	@XmlElement(name="MOB_NUM")
	String mobNumber;
	
	@XmlElement(name="FAX_CODE")
	String faxCode;
	
	@XmlElement(name="FAX_NO")
	String faxNumber;
	
	@XmlElement(name="EMAIL")
	String email;
	
	@XmlElement(name="REMARKS")
	String remarks;
	
	@XmlElement(name="DEC_DATE")
	String decDate;
	
	@XmlElement(name="DEC_PLACE")
	String decPlace;
	
	@XmlElement(name="KYC_DATE")
	String kycDate;
	
	@XmlElement(name="DOC_SUB")
	String docSubmitted;
	
	@XmlElement(name="KYC_NAME")
	String kycName;
	
	@XmlElement(name="KYC_DESIGNATION")
	String kycDesignation;
	
	@XmlElement(name="KYC_BRANCH")
	String kycBranch;
	
	@XmlElement(name="KYC_EMPCODE")
	String kycEmpCode;
	
	@XmlElement(name="ORG_NAME")
	String orgName;
	
	@XmlElement(name="ORG_CODE")
	String orgCode;
	
	@XmlElement(name="NUM_IDENTITY")
	String numberOfIdentity;
	
	@XmlElement(name="NUM_RELATED")
	String numberOfRelatedPerson;
	
	@XmlElement(name="NUM_LOCALADDRESS")
	String numberOfLocalAddress;
	
	@XmlElement(name="NUM_IMAGES")
	String numberOfImages;
	
	@XmlElement(name="NAME_UPDATE_FLAG")
	String nameUpdateFlag;
	
	@XmlElement(name="PERSONAL_UPDATE_FLAG")
	String personalUpdateFlag;
	
	@XmlElement(name="ADDRESS_UPDATE_FLAG")
	String addressUpdateFLag;
	
	@XmlElement(name="CONTACT_UPDATE_FLAG")
	String contactUpdateFlag;
	
	@XmlElement(name="REMARKS_UPDATE_FLAG")
	String remarksUpdateFlag;
	
	@XmlElement(name="KYC_UPDATE_FLAG")
	String kycUpdateFlag;
	
	@XmlElement(name="IDENTITY_UPDATE_FLAG")
	String identityUpdateFlag;
	
	@XmlElement(name="RELPERSON_UPDATE_FLAG")
	String relatedPersonUpdateFlag;
	
	@XmlElement(name="IMAGE_UPDATE_FLAG")
	String imageUpdateFlag;

	public DownloadResponsePersonalDetails() {
		super();
	}

	public String getConstitutionType() {
		return constitutionType;
	}

	public void setConstitutionType(String constitutionType) {
		this.constitutionType = constitutionType;
	}

	public String getAccountType() {
		return accountType;
	}

	public void setAccountType(String accountType) {
		this.accountType = accountType;
	}

	public String getCkycNumber() {
		return ckycNumber;
	}

	public void setCkycNumber(String ckycNumber) {
		this.ckycNumber = ckycNumber;
	}

	public String getPrefix() {
		return prefix;
	}

	public void setPrefix(String prefix) {
		this.prefix = prefix;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getMiddleName() {
		return middleName;
	}

	public void setMiddleName(String middleName) {
		this.middleName = middleName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String getFullName() {
		return fullName;
	}

	public void setFullName(String fullName) {
		this.fullName = fullName;
	}

	public String getMaidenPrefix() {
		return maidenPrefix;
	}

	public void setMaidenPrefix(String maidenPrefix) {
		this.maidenPrefix = maidenPrefix;
	}

	public String getMaidenFirstName() {
		return maidenFirstName;
	}

	public void setMaidenFirstName(String maidenFirstName) {
		this.maidenFirstName = maidenFirstName;
	}

	public String getMaidenMiddleName() {
		return maidenMiddleName;
	}

	public void setMaidenMiddleName(String maidenMiddleName) {
		this.maidenMiddleName = maidenMiddleName;
	}

	public String getMaidenLastName() {
		return maidenLastName;
	}

	public void setMaidenLastName(String maidenLastName) {
		this.maidenLastName = maidenLastName;
	}

	public String getMaidenFullName() {
		return maidenFullName;
	}

	public void setMaidenFullName(String maidenFullName) {
		this.maidenFullName = maidenFullName;
	}

	public String getFatherSpouseFlag() {
		return fatherSpouseFlag;
	}

	public void setFatherSpouseFlag(String fatherSpouseFlag) {
		this.fatherSpouseFlag = fatherSpouseFlag;
	}

	public String getFatherPrefix() {
		return fatherPrefix;
	}

	public void setFatherPrefix(String fatherPrefix) {
		this.fatherPrefix = fatherPrefix;
	}

	public String getFatherFirstName() {
		return fatherFirstName;
	}

	public void setFatherFirstName(String fatherFirstName) {
		this.fatherFirstName = fatherFirstName;
	}

	public String getFatherMiddleName() {
		return fatherMiddleName;
	}

	public void setFatherMiddleName(String fatherMiddleName) {
		this.fatherMiddleName = fatherMiddleName;
	}

	public String getFatherLastName() {
		return fatherLastName;
	}

	public void setFatherLastName(String fatherLastName) {
		this.fatherLastName = fatherLastName;
	}

	public String getFatherFullName() {
		return fatherFullName;
	}

	public void setFatherFullName(String fatherFullName) {
		this.fatherFullName = fatherFullName;
	}

	public String getMotherPrefix() {
		return motherPrefix;
	}

	public void setMotherPrefix(String motherPrefix) {
		this.motherPrefix = motherPrefix;
	}

	public String getMotherFirstName() {
		return motherFirstName;
	}

	public void setMotherFirstName(String motherFirstName) {
		this.motherFirstName = motherFirstName;
	}

	public String getMotherMiddleName() {
		return motherMiddleName;
	}

	public void setMotherMiddleName(String motherMiddleName) {
		this.motherMiddleName = motherMiddleName;
	}

	public String getMotherLastName() {
		return motherLastName;
	}

	public void setMotherLastName(String motherLastName) {
		this.motherLastName = motherLastName;
	}

	public String getMotherFullName() {
		return motherFullName;
	}

	public void setMotherFullName(String motherFullName) {
		this.motherFullName = motherFullName;
	}

	public String getGender() {
		return gender;
	}

	public void setGender(String gender) {
		this.gender = gender;
	}

	public String getMaritalStatus() {
		return maritalStatus;
	}

	public void setMaritalStatus(String maritalStatus) {
		this.maritalStatus = maritalStatus;
	}

	public String getNationality() {
		return nationality;
	}

	public void setNationality(String nationality) {
		this.nationality = nationality;
	}

	public String getOccupation() {
		return occupation;
	}

	public void setOccupation(String occupation) {
		this.occupation = occupation;
	}

	public String getDateOfBirth() {
		return dateOfBirth;
	}

	public void setDateOfBirth(String dateOfBirth) {
		this.dateOfBirth = dateOfBirth;
	}

	public String getResidentialStatus() {
		return residentialStatus;
	}

	public void setResidentialStatus(String residentialStatus) {
		this.residentialStatus = residentialStatus;
	}

	public String getJurisdictionFlag() {
		return jurisdictionFlag;
	}

	public void setJurisdictionFlag(String jurisdictionFlag) {
		this.jurisdictionFlag = jurisdictionFlag;
	}

	public String getJurisdictionResidence() {
		return jurisdictionResidence;
	}

	public void setJurisdictionResidence(String jurisdictionResidence) {
		this.jurisdictionResidence = jurisdictionResidence;
	}

	public String getTaxNumber() {
		return taxNumber;
	}

	public void setTaxNumber(String taxNumber) {
		this.taxNumber = taxNumber;
	}

	public String getBirthCountry() {
		return birthCountry;
	}

	public void setBirthCountry(String birthCountry) {
		this.birthCountry = birthCountry;
	}

	public String getBirthPlace() {
		return birthPlace;
	}

	public void setBirthPlace(String birthPlace) {
		this.birthPlace = birthPlace;
	}

	public String getPermAddressType() {
		return permAddressType;
	}

	public void setPermAddressType(String permAddressType) {
		this.permAddressType = permAddressType;
	}

	public String getPermLine1() {
		return permLine1;
	}

	public void setPermLine1(String permLine1) {
		this.permLine1 = permLine1;
	}

	public String getPermLine2() {
		return permLine2;
	}

	public void setPermLine2(String permLine2) {
		this.permLine2 = permLine2;
	}

	public String getPermLine3() {
		return permLine3;
	}

	public void setPermLine3(String permLine3) {
		this.permLine3 = permLine3;
	}

	public String getPermCity() {
		return permCity;
	}

	public void setPermCity(String permCity) {
		this.permCity = permCity;
	}

	public String getPermDist() {
		return permDist;
	}

	public void setPermDist(String permDist) {
		this.permDist = permDist;
	}

	public String getPermState() {
		return permState;
	}

	public void setPermState(String permState) {
		this.permState = permState;
	}

	public String getCountry() {
		return country;
	}

	public void setCountry(String country) {
		this.country = country;
	}

	public String getPincode() {
		return pincode;
	}

	public void setPincode(String pincode) {
		this.pincode = pincode;
	}

	public String getPermProofOfAddress() {
		return permProofOfAddress;
	}

	public void setPermProofOfAddress(String permProofOfAddress) {
		this.permProofOfAddress = permProofOfAddress;
	}

	public String getPermProofOfAddressOthers() {
		return permProofOfAddressOthers;
	}

	public void setPermProofOfAddressOthers(String permProofOfAddressOthers) {
		this.permProofOfAddressOthers = permProofOfAddressOthers;
	}

	public String getPermCorressSameFlag() {
		return permCorressSameFlag;
	}

	public void setPermCorressSameFlag(String permCorressSameFlag) {
		this.permCorressSameFlag = permCorressSameFlag;
	}

	public String getCorresLine1() {
		return corresLine1;
	}

	public void setCorresLine1(String corresLine1) {
		this.corresLine1 = corresLine1;
	}

	public String getCorresLine2() {
		return corresLine2;
	}

	public void setCorresLine2(String corresLine2) {
		this.corresLine2 = corresLine2;
	}

	public String getCorresline3() {
		return corresline3;
	}

	public void setCorresline3(String corresline3) {
		this.corresline3 = corresline3;
	}

	public String getCorresCity() {
		return corresCity;
	}

	public void setCorresCity(String corresCity) {
		this.corresCity = corresCity;
	}

	public String getCorresDist() {
		return corresDist;
	}

	public void setCorresDist(String corresDist) {
		this.corresDist = corresDist;
	}

	public String getCorresState() {
		return corresState;
	}

	public void setCorresState(String corresState) {
		this.corresState = corresState;
	}

	public String getCorresCountry() {
		return corresCountry;
	}

	public void setCorresCountry(String corresCountry) {
		this.corresCountry = corresCountry;
	}

	public String getCorresPin() {
		return corresPin;
	}

	public void setCorresPin(String corresPin) {
		this.corresPin = corresPin;
	}

	public String getJuriSameFlag() {
		return juriSameFlag;
	}

	public void setJuriSameFlag(String juriSameFlag) {
		this.juriSameFlag = juriSameFlag;
	}

	public String getJuriLine1() {
		return juriLine1;
	}

	public void setJuriLine1(String juriLine1) {
		this.juriLine1 = juriLine1;
	}

	public String getJuriLine2() {
		return juriLine2;
	}

	public void setJuriLine2(String juriLine2) {
		this.juriLine2 = juriLine2;
	}

	public String getJuriLine3() {
		return juriLine3;
	}

	public void setJuriLine3(String juriLine3) {
		this.juriLine3 = juriLine3;
	}

	public String getJuriCity() {
		return juriCity;
	}

	public void setJuriCity(String juriCity) {
		this.juriCity = juriCity;
	}

	public String getJuriState() {
		return juriState;
	}

	public void setJuriState(String juriState) {
		this.juriState = juriState;
	}

	public String getJuriCountry() {
		return juriCountry;
	}

	public void setJuriCountry(String juriCountry) {
		this.juriCountry = juriCountry;
	}

	public String getJuriPin() {
		return juriPin;
	}

	public void setJuriPin(String juriPin) {
		this.juriPin = juriPin;
	}

	public String getResiSTDCode() {
		return resiSTDCode;
	}

	public void setResiSTDCode(String resiSTDCode) {
		this.resiSTDCode = resiSTDCode;
	}

	public String getResiTELNumber() {
		return resiTELNumber;
	}

	public void setResiTELNumber(String resiTELNumber) {
		this.resiTELNumber = resiTELNumber;
	}

	public String getOffSTDCode() {
		return offSTDCode;
	}

	public void setOffSTDCode(String offSTDCode) {
		this.offSTDCode = offSTDCode;
	}

	public String getOffTELNumber() {
		return offTELNumber;
	}

	public void setOffTELNumber(String offTELNumber) {
		this.offTELNumber = offTELNumber;
	}

	public String getMobCode() {
		return mobCode;
	}

	public void setMobCode(String mobCode) {
		this.mobCode = mobCode;
	}

	public String getMobNumber() {
		return mobNumber;
	}

	public void setMobNumber(String mobNumber) {
		this.mobNumber = mobNumber;
	}

	public String getFaxCode() {
		return faxCode;
	}

	public void setFaxCode(String faxCode) {
		this.faxCode = faxCode;
	}

	public String getFaxNumber() {
		return faxNumber;
	}

	public void setFaxNumber(String faxNumber) {
		this.faxNumber = faxNumber;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getRemarks() {
		return remarks;
	}

	public void setRemarks(String remarks) {
		this.remarks = remarks;
	}

	public String getDecDate() {
		return decDate;
	}

	public void setDecDate(String decDate) {
		this.decDate = decDate;
	}

	public String getDecPlace() {
		return decPlace;
	}

	public void setDecPlace(String decPlace) {
		this.decPlace = decPlace;
	}

	public String getKycDate() {
		return kycDate;
	}

	public void setKycDate(String kycDate) {
		this.kycDate = kycDate;
	}

	public String getDocSubmitted() {
		return docSubmitted;
	}

	public void setDocSubmitted(String docSubmitted) {
		this.docSubmitted = docSubmitted;
	}

	public String getKycName() {
		return kycName;
	}

	public void setKycName(String kycName) {
		this.kycName = kycName;
	}

	public String getKycDesignation() {
		return kycDesignation;
	}

	public void setKycDesignation(String kycDesignation) {
		this.kycDesignation = kycDesignation;
	}

	public String getKycBranch() {
		return kycBranch;
	}

	public void setKycBranch(String kycBranch) {
		this.kycBranch = kycBranch;
	}

	public String getKycEmpCode() {
		return kycEmpCode;
	}

	public void setKycEmpCode(String kycEmpCode) {
		this.kycEmpCode = kycEmpCode;
	}

	public String getOrgName() {
		return orgName;
	}

	public void setOrgName(String orgName) {
		this.orgName = orgName;
	}

	public String getOrgCode() {
		return orgCode;
	}

	public void setOrgCode(String orgCode) {
		this.orgCode = orgCode;
	}

	public String getNumberOfIdentity() {
		return numberOfIdentity;
	}

	public void setNumberOfIdentity(String numberOfIdentity) {
		this.numberOfIdentity = numberOfIdentity;
	}

	public String getNumberOfRelatedPerson() {
		return numberOfRelatedPerson;
	}

	public void setNumberOfRelatedPerson(String numberOfRelatedPerson) {
		this.numberOfRelatedPerson = numberOfRelatedPerson;
	}

	public String getNumberOfLocalAddress() {
		return numberOfLocalAddress;
	}

	public void setNumberOfLocalAddress(String numberOfLocalAddress) {
		this.numberOfLocalAddress = numberOfLocalAddress;
	}

	public String getNumberOfImages() {
		return numberOfImages;
	}

	public void setNumberOfImages(String numberOfImages) {
		this.numberOfImages = numberOfImages;
	}

	public String getNameUpdateFlag() {
		return nameUpdateFlag;
	}

	public void setNameUpdateFlag(String nameUpdateFlag) {
		this.nameUpdateFlag = nameUpdateFlag;
	}

	public String getPersonalUpdateFlag() {
		return personalUpdateFlag;
	}

	public void setPersonalUpdateFlag(String personalUpdateFlag) {
		this.personalUpdateFlag = personalUpdateFlag;
	}

	public String getAddressUpdateFLag() {
		return addressUpdateFLag;
	}

	public void setAddressUpdateFLag(String addressUpdateFLag) {
		this.addressUpdateFLag = addressUpdateFLag;
	}

	public String getContactUpdateFlag() {
		return contactUpdateFlag;
	}

	public void setContactUpdateFlag(String contactUpdateFlag) {
		this.contactUpdateFlag = contactUpdateFlag;
	}

	public String getRemarksUpdateFlag() {
		return remarksUpdateFlag;
	}

	public void setRemarksUpdateFlag(String remarksUpdateFlag) {
		this.remarksUpdateFlag = remarksUpdateFlag;
	}

	public String getKycUpdateFlag() {
		return kycUpdateFlag;
	}

	public void setKycUpdateFlag(String kycUpdateFlag) {
		this.kycUpdateFlag = kycUpdateFlag;
	}

	public String getIdentityUpdateFlag() {
		return identityUpdateFlag;
	}

	public void setIdentityUpdateFlag(String identityUpdateFlag) {
		this.identityUpdateFlag = identityUpdateFlag;
	}

	public String getRelatedPersonUpdateFlag() {
		return relatedPersonUpdateFlag;
	}

	public void setRelatedPersonUpdateFlag(String relatedPersonUpdateFlag) {
		this.relatedPersonUpdateFlag = relatedPersonUpdateFlag;
	}

	public String getImageUpdateFlag() {
		return imageUpdateFlag;
	}

	public void setImageUpdateFlag(String imageUpdateFlag) {
		this.imageUpdateFlag = imageUpdateFlag;
	}
	
	
	
}
