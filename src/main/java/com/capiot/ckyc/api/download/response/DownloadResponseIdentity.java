package com.capiot.ckyc.api.download.response;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

import org.springframework.stereotype.Component;

@Component
@XmlRootElement(name="IDENTITY")
@XmlAccessorType(XmlAccessType.FIELD)
public class DownloadResponseIdentity {
	
	@XmlElement(name="SEQUENCE_NO")
	String sequenceNumber;
	
	@XmlElement(name="IDENT_TYPE")
	String identType;
	
	@XmlElement(name="IDENT_NUM")
	String identNumber;
	
	@XmlElement(name="ID_EXPIRYDATE")
	String idExpiryDate;
	
	@XmlElement(name="IDPROOF_SUBMITTED")
	String idProofSubmitted;
	
	@XmlElement(name="IDVER_STATUS")
	String idVerStatus;

	public DownloadResponseIdentity() {
		super();
	}

	public String getSequenceNumber() {
		return sequenceNumber;
	}

	public void setSequenceNumber(String sequenceNumber) {
		this.sequenceNumber = sequenceNumber;
	}

	public String getIdentType() {
		return identType;
	}

	public void setIdentType(String identType) {
		this.identType = identType;
	}

	public String getIdentNumber() {
		return identNumber;
	}

	public void setIdentNumber(String identNumber) {
		this.identNumber = identNumber;
	}

	public String getIdExpiryDate() {
		return idExpiryDate;
	}

	public void setIdExpiryDate(String idExpiryDate) {
		this.idExpiryDate = idExpiryDate;
	}

	public String getIdProofSubmitted() {
		return idProofSubmitted;
	}

	public void setIdProofSubmitted(String idProofSubmitted) {
		this.idProofSubmitted = idProofSubmitted;
	}

	public String getIdVerStatus() {
		return idVerStatus;
	}

	public void setIdVerStatus(String idVerStatus) {
		this.idVerStatus = idVerStatus;
	}
	
	
	
}
