package com.capiot.ckyc.api.download.request;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

import org.springframework.stereotype.Component;

@Component
@XmlRootElement(name="PID")
@XmlAccessorType(XmlAccessType.FIELD)
public class DownloadRequestPID {
	
	@XmlElement(name="PID_DATA", type=DownloadRequestPIDData.class)
	DownloadRequestPIDData DownloadRequestPIDData;

	public DownloadRequestPID() {
		super();
	}

	public DownloadRequestPID(DownloadRequestPIDData DownloadRequestPIDData) {
		super();
		this.DownloadRequestPIDData = DownloadRequestPIDData;
	}

	public DownloadRequestPIDData getPidData() {
		return DownloadRequestPIDData;
	}

	public void setPidData(DownloadRequestPIDData DownloadRequestPIDData) {
		this.DownloadRequestPIDData = DownloadRequestPIDData;
	}

	@Override
	public String toString() {
		return "PID [DownloadRequestPIDData=" + DownloadRequestPIDData + "]";
	}
	
	
	
}
