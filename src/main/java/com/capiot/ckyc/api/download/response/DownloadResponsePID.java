package com.capiot.ckyc.api.download.response;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

import org.springframework.stereotype.Component;

@Component
@XmlRootElement(name="PID")
@XmlAccessorType(XmlAccessType.FIELD)
public class DownloadResponsePID {
	
	@XmlElement(name="PID_DATA", type=DownloadResponsePIDData.class)
	DownloadResponsePIDData pidData;

	public DownloadResponsePID(DownloadResponsePIDData pidData) {
		super();
		this.pidData = pidData;
	}

	public DownloadResponsePID() {
		super();
	}

	public DownloadResponsePIDData getPidData() {
		return pidData;
	}

	public void setPidData(DownloadResponsePIDData pidData) {
		this.pidData = pidData;
	}
	
	
}
