package com.capiot.ckyc.api.download.response;

import java.util.List;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

import org.springframework.stereotype.Component;

@Component
@XmlRootElement(name="LOCAL_ADDRESS_DETAILS")
@XmlAccessorType(XmlAccessType.FIELD)	
public class DownloadResponseLocalAddressDetails {
	
	@XmlElement(name="LOCAL_ADDRESS", type=DownloadResponseLocalAddress.class)
	List<DownloadResponseLocalAddress> localAddresses;

	public DownloadResponseLocalAddressDetails() {
		super();
	}

	public DownloadResponseLocalAddressDetails(List<DownloadResponseLocalAddress> localAddresses) {
		super();
		this.localAddresses = localAddresses;
	}

	public List<DownloadResponseLocalAddress> getLocalAddresses() {
		return localAddresses;
	}

	public void setLocalAddresses(List<DownloadResponseLocalAddress> localAddresses) {
		this.localAddresses = localAddresses;
	}
	
	
}
