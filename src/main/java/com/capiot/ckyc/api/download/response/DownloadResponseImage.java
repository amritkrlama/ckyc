package com.capiot.ckyc.api.download.response;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

import org.springframework.stereotype.Component;

@Component
@XmlRootElement(name="IMAGE")
@XmlAccessorType(XmlAccessType.FIELD)
public class DownloadResponseImage {
	
	@XmlElement(name="SEQUENCE_NO")
	String sequenceNumber;
	
	@XmlElement(name="IMAGE_TYPE")
	String imageType;
	
	@XmlElement(name="IMAGE_CODE")
	String imageCode;
	
	@XmlElement(name="GLOBAL_FLAG")
	String globalOrLocalFlag;
	
	@XmlElement(name="BRANCH_CODE")
	String branchCode;
	
	@XmlElement(name="IMAGE_DATA")
	String imageData;

	public DownloadResponseImage() {
		super();
	}

	public DownloadResponseImage(String sequenceNumber, String imageType, String imageCode, String globalOrLocalFlag,
			String branchCode, String imageData) {
		super();
		this.sequenceNumber = sequenceNumber;
		this.imageType = imageType;
		this.imageCode = imageCode;
		this.globalOrLocalFlag = globalOrLocalFlag;
		this.branchCode = branchCode;
		this.imageData = imageData;
	}

	public String getSequenceNumber() {
		return sequenceNumber;
	}

	public void setSequenceNumber(String sequenceNumber) {
		this.sequenceNumber = sequenceNumber;
	}

	public String getImageType() {
		return imageType;
	}

	public void setImageType(String imageType) {
		this.imageType = imageType;
	}

	public String getImageCode() {
		return imageCode;
	}

	public void setImageCode(String imageCode) {
		this.imageCode = imageCode;
	}

	public String getGlobalOrLocalFlag() {
		return globalOrLocalFlag;
	}

	public void setGlobalOrLocalFlag(String globalOrLocalFlag) {
		this.globalOrLocalFlag = globalOrLocalFlag;
	}

	public String getBranchCode() {
		return branchCode;
	}

	public void setBranchCode(String branchCode) {
		this.branchCode = branchCode;
	}

	public String getImageData() {
		return imageData;
	}

	public void setImageData(String imageData) {
		this.imageData = imageData;
	}
	
	
}	
