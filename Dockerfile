FROM openjdk:8-jre-slim-stretch
ADD CKYC-0.0.1-SNAPSHOT.jar CKYC-0.0.1-SNAPSHOT.jar
EXPOSE 8090
COPY Downloads /dch/ckyc/Downloads
COPY input /dch/ckyc/input
COPY response /dch/ckyc/response
ENTRYPOINT ["java", "-jar", "CKYC-0.0.1-SNAPSHOT.jar"]
